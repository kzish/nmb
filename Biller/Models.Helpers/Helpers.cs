﻿using Biller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Helpers
{

    public static string getBillerCategory(int id)
    {
        var db = new dbContext();
        var res = db.EnumBillerCategory.Find(id);
        db.Dispose();
        return res.Category;
    }
}
