﻿using System;
using System.Collections.Generic;

namespace Biller.Models
{
    public partial class MApiClients
    {
        public string Id { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }

        public virtual AspNetUsers IdNavigation { get; set; }
    }
}
