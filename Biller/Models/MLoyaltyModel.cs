﻿using System;
using System.Collections.Generic;

namespace Biller.Models
{
    public partial class MLoyaltyModel
    {
        public int Id { get; set; }
        public string LoyaltyModelName { get; set; }
        public string LoyaltyPointsCriteria { get; set; }
        public decimal? LoyaltyPointsMinValue { get; set; }
        public decimal? LoyaltyPointsMaxValue { get; set; }
        public decimal? PointsPerAmount { get; set; }
        public decimal? BonusPoints { get; set; }
    }
}
