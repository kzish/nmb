﻿using System;
using System.Collections.Generic;

namespace Biller.Models
{
    public partial class MRedeemRule
    {
        public int Id { get; set; }
        public string RedeemRuleName { get; set; }
        public string RedeemRuleCriteria { get; set; }
        public decimal? RedeemRuleMinPoints { get; set; }
        public decimal? AmountPerPoint { get; set; }
    }
}
