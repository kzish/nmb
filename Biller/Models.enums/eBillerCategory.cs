﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models.enums
{
    public enum eBillerCategory
    {
        UNIVERSITIES,
        SCHOOLS,
        HOSPITALS,
        HOTELS,
        SUPERMARKETS,
        PHARMACEUTICALS,
        AUTOMOTIVE,
        TRANSPORT,
        FUEL,
        INSURANCE,
        CHURCHES,
        FASTFOODS
    }
}
