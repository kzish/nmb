﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models.enums
{
    public enum eCurrencyCode
    {
        ZWL,
        USD,
        RAND,
        PULA
    }
}
