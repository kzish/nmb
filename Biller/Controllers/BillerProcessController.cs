﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Biller.Models;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using Biller.Models.Biller;
using Biller.Models.enums;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

/// <summary>
/// end point used by client device/mobile application
/// this is for online billers and offline billers, 
/// the client/mobile app will access all services through this one end point
/// and requires authentication from the Auth microservice
/// </summary>
namespace OnlineBillers.Controllers
{
    [Route("scpayment/v1")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BillerProcessController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        /// <summary>
        /// fetch all products from this biller, billercode
        /// </summary>
        /// <param name="billerCode"></param>
        /// <returns></returns>
        [HttpGet("GetBillerProducts")]
        public JsonResult GetBillerProducts(string billerCode)
        {

            try
            {
                if (string.IsNullOrEmpty(billerCode))
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code parameter is missing"
                    });
                }

                var biller = db.MBillerInfo.Where(i => i.BillerCode == billerCode).FirstOrDefault();


                if (biller == null)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code is invalid"
                    });
                }

                //this biller supports his own products
                //fetch the products from the biller endpoint
                if (biller.HasProducts == true)
                {
                    var client = new HttpClient();

                    var response = client.GetAsync(biller.RequestEndPoint + "/GetBillerProducts")
                        .Result.Content.ReadAsStringAsync().Result;
                    dynamic json = JObject.Parse(response);
                    string products_array = json.msg.ToString();
                    var products = JsonConvert.DeserializeObject<List<MBillerProduct>>(products_array);
                    //normalize these products
                    int index = 0;
                    foreach (var p in products)
                    {
                        products[index].Id = index + 1;
                        products[index].BillerCode = billerCode;
                        index++;
                    }
                    return Json(new
                    {
                        res = "00",
                        data = products
                    });
                }
                else
                {
                    //this billers products are stored in our database
                    //fetch the products from the db
                    var products = db.MBillerProduct.Where(i => i.BillerCode == billerCode).ToList();
                    return Json(new
                    {
                        res = "00",
                        data = products
                    });
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    msg = ex.Message
                });
            }


        }


        /// <summary>
        /// client/app stores cart offline and posts the entire cart at once
        /// client/app must store the entire cart on client/app side before posting the cart
        /// this method handles the cart creation and payment
        /// this method handles all related processes to make the payment 
        /// where the biller microservice requires a tracking id, the payment ref will match the tracking id
        /// where the biller microservice does not accept a tracking id, the payment ref will be different from the tracking id, eg fresh in a box
        /// if multiple items exist in the biller cart then the payment ref is appended by the item subscript index starting at zero eg xxxxx.1
        /// </summary>
        /// <param name="BillerCart"></param>
        /// <returns></returns>
        [HttpPost("ProcessPayment")]
        public async Task<JsonResult> PostBillerCart([FromBody] mBillerCartRequest BillerCart)
        {
            try
            {
                if (string.IsNullOrEmpty(BillerCart.biller_code))
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code is missing"
                    });
                }

                if (BillerCart.items.Count == 0)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "There are no items in the cart"
                    });
                }


                //fetch only the billers this client has selected to allow
                var client_id = HttpContext.User.Claims.Where(i => i.Type == "client_id").FirstOrDefault()?.Value;
                var client_billers = db.MClientBillers.Where(i => i.ClientId == client_id).ToList();

                //
                var biller = db.MBillerInfo
                    .Where(i => i.BillerCode == BillerCart.biller_code)
                    .Where(i => client_billers.Any(x => x.BillerId == i.Id))//only my billers
                    .FirstOrDefault();


                if (biller == null)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code is invalid"
                    });
                }
                //
                var asp_net_user = db.AspNetUsers
                    .Where(i => i.Id == biller.Id)
                    .Include(i => i.MBankAccount)
                    .FirstOrDefault();


                dynamic result = null;
                if (biller.EnumBillerType == 1)//this is online biller
                {
                    //online billers
                    //post cart to the biller's microservice
                    //biller microservice must handle the cart/all internal processed and the payment
                    var client = new HttpClient();
                    var response = client.PostAsJsonAsync(biller.RequestEndPoint + "/PostBillerCart", BillerCart)
                        .Result.Content.ReadAsStringAsync().Result;
                    client.Dispose();
                    result = JObject.Parse(response);
                }
                else
                {
                    //offline billers with zss enabled
                    if (!string.IsNullOrEmpty(biller.Enabler))
                    {
                        //go thru zss
                        var client = new HttpClient();
                        var response = await client.PostAsJsonAsync(biller.RequestEndPoint + "/ZSS/PayforOrder", BillerCart).Result.Content.ReadAsStringAsync();
                        dynamic json = JsonConvert.DeserializeObject(response);
                        if (json.res == "ok")
                        {
                            var obj = Json(new
                            {
                                res = "ok",
                                msg = json.msg,
                                tracking_id = json.tracking_id,
                                payment_ref = json.payment_ref,
                            }).Value;
                            result = JObject.Parse(JsonConvert.SerializeObject(obj));
                        }
                        else
                        {
                            var obj = Json(new
                            {
                                res = "err",
                                msg = json.msg,
                                tracking_id = BillerCart.payment_ref,
                                payment_ref = BillerCart.payment_ref,
                            }).Value;
                            result = JObject.Parse(JsonConvert.SerializeObject(obj));
                        }
                    }
                    else
                    {   //normal offline billers, no microservice
                        var obj = Json(new
                        {
                            res = "ok",
                            msg = "success",
                            tracking_id = BillerCart.payment_ref,
                            payment_ref = BillerCart.payment_ref,
                        }).Value;
                        result = JObject.Parse(JsonConvert.SerializeObject(obj));
                    }
                }
                //get the active account
                var billers_bank_account = asp_net_user.MBankAccount.First(i => i.Active == true);
                var currency_code = db.EnumCurrencyCode.First(i => i.Id == billers_bank_account.EnumCurrencyCode).CurrencyCode;
                var bank_code = db.EnumBanks.First(i => i.Id == billers_bank_account.EnumBank).BankCode;
                var transaction_amount = BillerCart.items.Sum(i => i.quantity * i.unit_price);

                //save this transaction to db
                var transaction = new MTransAction();
                transaction.Date = DateTime.Now;
                transaction.FromAccountNumber = null;
                transaction.ToAccountNumber = billers_bank_account.AccountNumber;
                transaction.FromCurrencyCode = null;
                transaction.ToCurrencyCode = null;
                transaction.MessageSubType = null;
                transaction.TransactionCurrency = currency_code;
                transaction.TransactionAmount = transaction_amount.ToString("0.00");
                transaction.FromBankCode = BillerCart.bank_code;//to identify the bank where the trasaction came from
                transaction.ToBankCode = bank_code;
                transaction.CostRate = null;
                transaction.ConversionRate = null;
                transaction.UniqueReferenceIdentifier = null;
                transaction.TransactionBranch = null;
                transaction.TransactionPostingDate = DateTime.Now.ToString();
                transaction.SourceSystemName = null;
                transaction.TransactionType = null;
                transaction.LcyEquivalent = null;
                transaction.Countrycode = null;
                transaction.TrackingId = result.tracking_id;
                transaction.BillerCode = BillerCart.biller_code;
                transaction.PayerName = BillerCart.payer_name;
                transaction.PayerMobile = BillerCart.payer_mobile;
                transaction.PayerDetails1 = BillerCart.payer_details1;
                transaction.PayerDetails2 = BillerCart.payer_details2;
                transaction.PayerDetails3 = BillerCart.payer_details3;
                transaction.PaymentRef = BillerCart.payment_ref;
                transaction.ApiResponseData = result.msg.ToString();//save the response incase user wants to retrive this response again
                if (result.res == "ok")
                    transaction.TransactionStatus = "SUCCESS";
                else if (result.res == "err")
                    transaction.TransactionStatus = "FAIL";

                db.MTransAction.Add(transaction);
                db.SaveChanges();
                foreach (var item in BillerCart.items)
                {
                    var order = new MBillerOrders();
                    order.Date = transaction.Date ?? DateTime.Now;
                    order.ProductCode = item.product_code;
                    order.Quantity = item.quantity;
                    order.UnitPrice = item.unit_price;
                    order.BillerCode = BillerCart.biller_code;
                    order.TransactionId = transaction.Id;
                    db.MBillerOrders.Add(order);
                }
               

                //retrun the result to the api
                if (result.res == "ok")
                {
                    //save the api usage only for a successful transaction
                    var usage = new MApiUsageStats();
                    usage.Date = DateTime.Now;
                    usage.TrackingId = result.tracking_id;
                    usage.ClientName = client_id;
                    usage.TransactionAmount = transaction_amount;
                    usage.PaymentRef = BillerCart.payment_ref;
                    db.MApiUsageStats.Add(usage);
                    db.SaveChanges();

                    return Json(new
                    {
                        res = "00",
                        data = result.msg,
                        tracking_id = transaction.PaymentRef//return the payment ref to the caller
                    });
                }
                else
                { //error occurred
                    return Json(new
                    {
                        res = "02",
                        data = result.msg
                    });
                }


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    msg = "Error occurred",
                    debug = ex
                });
            }


        }

        /// <summary>
        /// allow the mobile app or client to confirm the account details before making the actual payment
        /// availble for certain billers eg zesa, zinara, cimas etc
        /// the biller cart must be exaclty the same as the billercart for processing payment
        /// </summary>
        /// <param name="BillerCart"></param>
        /// <returns></returns>
        [HttpPost("ConfirmDetails")]
        public async Task<JsonResult> ConfirmDetails([FromBody] mBillerCartRequest BillerCart)
        {
            try
            {
                if (string.IsNullOrEmpty(BillerCart.biller_code))
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code is missing"
                    });
                }

                if (BillerCart.items.Count == 0)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "There are no items in the cart"
                    });
                }

                //fetch only the billers this client has selected to allow
                var client_id = HttpContext.User.Claims.Where(i => i.Type == "client_id").FirstOrDefault()?.Value;
                var client_billers = db.MClientBillers.Where(i => i.ClientId == client_id).ToList();

                //
                var biller = db.MBillerInfo
                    .Where(i => i.BillerCode == BillerCart.biller_code)
                    .Where(i => client_billers.Any(x => x.BillerId == i.Id))//only my billers
                    .FirstOrDefault();


                if (biller == null)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller code is invalid"
                    });
                }

                if (!biller.AllowConfirm)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Biller does not support allow confirm"
                    });
                }

                dynamic result = null;
                if (!string.IsNullOrEmpty(biller.Enabler))//zss enabler
                {
                    //go thru zss
                    var client = new HttpClient();
                    var response = await client.PostAsJsonAsync(biller.RequestEndPoint + "/ZSS/CheckAccount", BillerCart).Result.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(response);
                    if (json.res == "ok")
                    {
                        var obj = Json(new
                        {
                            res = "ok",
                            msg = json.msg
                        }).Value;
                        result = JObject.Parse(JsonConvert.SerializeObject(obj));
                    }
                    else
                    {
                        var obj = Json(new
                        {
                            res = "err",
                            msg = json.msg
                        }).Value;
                        result = JObject.Parse(JsonConvert.SerializeObject(obj));
                    }
                    return Json(new
                    {
                        res = "00",
                        data = result.msg,
                    });
                }
                else if (!string.IsNullOrEmpty(biller.RequestEndPoint))
                {
                    //go the microservice
                    var client = new HttpClient();
                    var response = await client.PostAsJsonAsync(biller.RequestEndPoint + "/CheckAccount", BillerCart).Result.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(response);
                    if (json.res == "ok")
                    {
                        var obj = Json(new
                        {
                            res = "ok",
                            msg = json.msg
                        }).Value;
                        result = JObject.Parse(JsonConvert.SerializeObject(obj));
                    }
                    else
                    {
                        var obj = Json(new
                        {
                            res = "err",
                            msg = json.msg
                        }).Value;
                        result = JObject.Parse(JsonConvert.SerializeObject(obj));
                    }
                    return Json(new
                    {
                        res = "00",
                        data = result.msg,
                    });
                }
                return Json(new
                {
                    res = "00",
                    data = string.Empty,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    msg = "Error occurred",
                    debug = ex
                });
            }


        }



        /// <summary>
        /// client/app may post responses to questions/enquiries
        /// </summary>
        /// <param name="responses"></param>
        /// <returns></returns>
        [HttpPost("PostBillerEnquiry")]
        private JsonResult PostBillerEnquiry([FromBody]List<BillerEnquiryResponse> responses)
        {
            try
            {
                //create the service enquiry and save to db
                var _mServiceEnquiry = new MServiceEnquiry();
                _mServiceEnquiry.PayerName = responses[0].payer_name;
                _mServiceEnquiry.PhoneNumber = responses[0].phone_number;
                _mServiceEnquiry.Billercode = responses[0].biller_code;
                db.MServiceEnquiry.Add(_mServiceEnquiry);
                db.SaveChanges();
                var service_enquiry_id = _mServiceEnquiry.Id;

                //save the responses
                foreach (var res in responses)
                {
                    var ans = new MEnquiryAnswer();
                    ans.Questionid = res.question_id;
                    ans.Question = db.MEnquiryQuestions.Where(i => i.Id == res.question_id).FirstOrDefault()?.Question;
                    ans.Answer = res.question_response;
                    ans.ServiceEnquiryId = (int)service_enquiry_id;
                    db.MEnquiryAnswer.Add(ans);
                }
                db.SaveChanges();
                //clean up

                return Json(new
                {
                    res = "00",
                    msg = "Saved responses",

                });
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    msg = ex.Message

                });
            }

        }


        /// <summary>
        /// get the status of a transaction
        /// </summary>
        /// <param name="payment_ref"></param>
        /// <returns></returns>
        [HttpGet("GetTransactionStatus")]
        public JsonResult GetTransactionStatus(string payment_ref)
        {
            try
            {
                if (string.IsNullOrEmpty(payment_ref))
                {
                    return Json(new
                    {
                        res = "02",
                        msg = "Payment_ref is missing."

                    }, new JsonSerializerSettings()
                    {
                        Formatting = Formatting.Indented,

                    });
                }
                var transaction = db.MTransAction.Where(i => i.PaymentRef == payment_ref).FirstOrDefault();

                if (transaction == null)
                {
                    return Json(new
                    {
                        res = "02",
                        msg = $"Payment_ref {payment_ref} does not exist."

                    }, new JsonSerializerSettings()
                    {
                        Formatting = Formatting.Indented,

                    });
                }

                dynamic result = new JObject();
                result.transaction_status = transaction.TransactionStatus;
                result.payer_mobile = transaction.PayerMobile;
                result.txn_date = transaction.Date;
                result.amount = transaction.TransactionAmount;
                result.biller_code = transaction.BillerCode;
                result.payer_account_number = $"{transaction.PayerDetails1} | {transaction.PayerDetails2} | {transaction.PayerDetails3}";
                return Json(new
                {
                    res = "00",
                    data = result

                }, new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented,

                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    msg = "Error fetching the record"
                }, new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented,

                });
            }

        }

    }
}

