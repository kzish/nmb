﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Biller.Models;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;
using Biller.Models.Biller;
using Microsoft.EntityFrameworkCore;

namespace OnlineBillers.Controllers
{
    [Route("scpayment/v1")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BillerAccountsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        /// <summary>
        /// fetches all the billers
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetBillers")]
        public JsonResult GetBillers(string billerCategory)
        {
            try
            {

                //fetch only the billers this client has selected to allow
                var client_id = HttpContext.User.Claims.Where(i => i.Type == "client_id").FirstOrDefault()?.Value;
                var client_billers = db.MClientBillers.Where(i => i.ClientId == client_id).ToList();

                List<AspNetUsers> asp_net_users = null;

                if (string.IsNullOrEmpty(billerCategory))
                {
                    asp_net_users = db.AspNetUsers
                        .Where(i => i.AspNetUserRoles.Any(r => r.Role.Name == "biller"))
                        .Include(i => i.MBillerInfo)
                        .Include(i => i.MBankAccount)
                        .Where(i => i.MBillerInfo.BillerStatus == true)
                        .Where(i=>client_billers.Any(x=>x.BillerId==i.Id))//only my billers
                        .ToList();
                }
                else
                {
                    int biller_category = db.EnumBillerCategory.Where(i => i.Category == billerCategory).FirstOrDefault().Id;
                    asp_net_users = db.AspNetUsers
                          .Where(i => i.AspNetUserRoles.Any(r => r.Role.Name == "biller"))
                          .Include(i => i.MBillerInfo)
                          .Include(i => i.MBankAccount)
                          .Where(i => i.MBillerInfo.BillerStatus == true)
                          .Where(i => i.MBillerInfo.EnumBillerCategory == biller_category)//this category
                          .Where(i => client_billers.Any(x => x.BillerId == i.Id))//only my billers
                          .ToList();
                }

                var billers_list = new List<mBiller>();//use clean model
                foreach (var b in asp_net_users)
                {
                    var bank_account = b.MBankAccount.Where(i => i.Active == true).FirstOrDefault();
                    if (bank_account == null)
                    {
                        continue;
                    }
                    var bank_code = db.EnumBanks.Where(i => i.Id == bank_account.EnumBank).FirstOrDefault()?.BankCode;
                    var nb = new mBiller()
                    {
                        biller_code = b.MBillerInfo.BillerCode,
                        biller_name = b.MBillerInfo.BillerName,
                        biller_img_url = "https://rubieminnovations.xyz/img/brand/rubien%20innovation%20overall%20transparent%20bg.png",
                        biller_category = Helpers.getBillerCategory(b.MBillerInfo.EnumBillerCategory.Value),
                        account_name = bank_account?.AccountName,
                        account_number = bank_account?.AccountNumber,
                        bank_code = bank_code,
                        allow_confirm = b.MBillerInfo.AllowConfirm

                    };
                    billers_list.Add(nb);
                }

                //return success
                return Json(new
                {
                    responce_code = "00",
                    data = billers_list
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "02",
                    data = ex.Message
                });
            }

        }

        [HttpGet("GetBillerTypes")]
        public JsonResult GetBillerTypes()
        {
            var types = db.EnumBillerCategory.OrderBy(i=>i.Category).ToList();
            var types_ = new List<string>();
            foreach (var t in types)
            {
                types_.Add(t.Category);
            }
            return Json(types_);
        }


    }
}
