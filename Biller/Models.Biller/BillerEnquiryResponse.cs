﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models.Biller
{
    /// <summary>
    /// this is the model the mobile app will respond
    /// to the billers questions
    /// this model does not persist to the database but is a helper class, to accept the mobile app's
    /// response
    /// </summary>
    public class BillerEnquiryResponse
    {
        public int question_id { get; set; }
        public string question_response { get; set; }
        public string payer_name { get; set; }
        public string phone_number { get; set; }
        public string biller_code { get; set; }
    }
}
