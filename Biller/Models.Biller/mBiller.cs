﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models.Biller
{
    /// <summary>
    /// this model is diplayed to the api
    /// models the biller to be diplayed to the api
    /// </summary>
    public class mBiller
    {
        public string biller_code { get; set; }
        public string biller_name { get; set; }
        public string biller_category { get; set; }
        public string biller_img_url { get; set; } 
        public string bank_code { get; set; }
        public string account_number { get; set; }
        public string account_name { get; set; }
        public bool allow_confirm { get; set; }
    }
}
