﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using IdentityServer4;
using IdentityServer4.AccessTokenValidation;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.Cookies;
using Consul;
using Microsoft.Extensions.Configuration;
using TestAPI.globals;
using System.Text;

namespace TestAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //consul
            services.Configure<ConsulConfig>(Configuration);
            services.Configure<ConsulConfig>(async config => {
                using (var client = new ConsulClient(clientConfig => clientConfig.Address = new Uri("http://localhost:8500/")))
                {
                    var getPair = await client.KV.Get("serviceUrl");
                    if (getPair.Response != null)
                    {
                        var serviceUrl = Encoding.UTF8.GetString(getPair.Response.Value, 0, getPair.Response.Value.Length);
                        config.ServiceUrl = serviceUrl;
                    }
                }
            });
            //authentication
            services.AddAuthentication("Bearer")
           .AddIdentityServerAuthentication(x =>
            {
                x.Authority = "http://localhost:14726";
                x.ApiName = "api";
                x.RequireHttpsMetadata = false;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //for authentication
            app.UseAuthentication();


            app.UseMvc();
            //self register in simba_config
            var app_name = env.ApplicationName;
            app.Run(async (context) =>
            {
                var path = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedUrl(context.Request);
                path = path.Replace("/favicon.ico", "");
                Globals.write_config(app_name, $"{path}");
                await context.Response.WriteAsync(app_name);
            });
        }
    }
}
