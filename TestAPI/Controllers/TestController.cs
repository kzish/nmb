﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using ZSS;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Data.Odbc;

/// <summary>
/// controller to test the microservices api
/// </summary>
namespace TestAPI.Controllers
{
    [Route("test")]
   // [Route("")]
    public class TestController : Controller
    {

        
        [HttpGet("index")]
        //[HttpGet("")]
        public JsonResult Index()
        {
            try
            {
                Globals.log_data_to_file(source, "Service is started at " + DateTime.Now);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = 5000; //number in milisecinds  
                timer.Enabled = true;
                timer.Start();
                return Json(DateTime.Now);
            }catch(Exception ex)
            {
                return Json(ex);
            }
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                if (busy) return;
                busy = true;
                dynamic jsonconfig = JsonConvert.DeserializeObject(System.IO.File.ReadAllText(config_file));
                //read the config param
                string base_url = jsonconfig.url;
                Globals.log_data_to_file(source+ ".OnElapsedTime", "Service is recall at " + DateTime.Now);
                ///fetch all the pending transactions
                var client = new HttpClient();
                var json = client.GetAsync($"{base_url}").Result.Content.ReadAsStringAsync().Result;
                dynamic json_ = JsonConvert.DeserializeObject(json);
                if (json_.res == "ok")
                {
                    List<Biller.Models.MTransAction> transactions = JsonConvert.DeserializeObject<List<Biller.Models.MTransAction>>(json_.msg.ToString());
                    decimal totalTransactionAmount = 0m;
                    foreach (var t in transactions)
                    {
                        totalTransactionAmount += decimal.Parse(t.TransactionAmount);
                        ///post this to the progress db
                        PostReceipts(t.ToAccountNumber,decimal.Parse(t.TransactionAmount),t.Date.ToString());
                    }

                    AddReceiptsControlFile(rec_date:DateTime.Now.ToString(),cash: totalTransactionAmount.ToString("0.00"));
                }
                else
                {
                    Globals.log_data_to_file(source + ".OnElapsedTime", json_.msg);
                }
                ///
            }catch(Exception ex)
            {
                Globals.log_data_to_file(source+ ".OnElapsedTime", ex);

            }
            finally
            {
                busy = false;
            }

        }

        protected static bool PostReceipts(string Accno, decimal amount, string effdate)
        {
            //The connection string assumes there is a DSN named sports for a Progress database.
            //Alternatively, a DSN-less connection string could be used in its place. See article# 000022406 for more details.

            string connectionString = "DSN=receipting;" +
                                       "UID=Sysprogress;" +
                                       "PWD=Sysprogress";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    String query = "INSERT INTO pub.munrct (acc,amount,code,eff-date,mcno,opcode,paytype,rec-date,rec-status,vat-amt) VALUES (@acc,@amount,@code,@eff-date,@mcno,@opcode,@paytype,@rec-date,@rec-status,@vat-amt)";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@acc", Accno);
                        command.Parameters.AddWithValue("@amount", amount);
                        command.Parameters.AddWithValue("@code", "zz");
                        command.Parameters.AddWithValue("@eff-date", effdate);
                        command.Parameters.AddWithValue("@mcno", "99");
                        command.Parameters.AddWithValue("@opcode", "99");
                        command.Parameters.AddWithValue("@paytype", "p");
                        command.Parameters.AddWithValue("@rec-date", effdate);
                        command.Parameters.AddWithValue("@rec-status", "u");
                        command.Parameters.AddWithValue("@vat-amt", "0");

                        connection.Open();
                        int result = command.ExecuteNonQuery();

                        // Check Error
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }


                catch (Exception e)
                {

                }
            }
            return true;
        }

        protected static void AddReceiptsControlFile(string rec_date, string cash)
        {
            //The connection string assumes there is a DSN named sports for a Progress database.
            //Alternatively, a DSN-less connection string could be used in its place. See article# 000022406 for more details.

            dynamic jsonconfig = JsonConvert.DeserializeObject(System.IO.File.ReadAllText(config_file));


            string connectionString = "DSN=receipting;" +
                                       "UID=Sysprogress;" +
                                       "PWD=Sysprogress";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                String query = "INSERT INTO pub.munrctctr (mcno,rec-date,valid,cash,rec-status,eff-date,eff-datetime) VALUES (@mcno ,@rec-date,@valid,@cash,@rec-status,@eff-date,@eff-datetime)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    //Default values
                    //cash = 99999.00;
                    //valid = 0;

                    command.Parameters.AddWithValue("@mcno", jsonconfig.mcno);
                    command.Parameters.AddWithValue("@rec-date", rec_date);
                    command.Parameters.AddWithValue("@valid", "true");
                    command.Parameters.AddWithValue("@cash", cash);//amount in cents
                    command.Parameters.AddWithValue("@rec-status", jsonconfig.rec_status);
                    command.Parameters.AddWithValue("@eff-date", rec_date);
                    command.Parameters.AddWithValue("@eff-datetime", rec_date);
                    command.Parameters.AddWithValue("@vat-amt", jsonconfig.vat_amt);
                    command.Parameters.AddWithValue("@paytype", jsonconfig.paytype);
                    command.Parameters.AddWithValue("@opcode", jsonconfig.opcode);
                    command.Parameters.AddWithValue("@code", jsonconfig.code);

                    connection.Open();
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                    command.Dispose();
                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error inserting data into Database!");
                }
            }
        }


        private Timer timer = new Timer();
        private static string log_file = @"C:\rubiem\simba\simba_logs.txt";
        private string source = "sc_progress_city_service";
        private static string config_file = @"c:\rubiem\simba\SCProgressCityService.config";
        private static bool busy=false;



    }
}
