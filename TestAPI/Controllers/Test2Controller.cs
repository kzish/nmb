﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceReference1;

namespace TestAPI.Controllers
{
    [Route("test2")]
    [Route("")]
    public class Test2Controller : Controller
    {
        [Route("test")]
        [Route("")]
        public JsonResult test()
        {
            var client = get_client();
            GetCustomerName request = new GetCustomerName();
            request.customerNo = "scd0045";
            request.result = "";
            var response=client.GetCustomerNameAsync(request).Result;
            return new JsonResult(response);
        }




        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the scb client for the request
        /// </summary>
        /// <returns></returns>
        public AppIntegrationMgt_PortClient get_client()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            //configure end point
            var endPoint = new EndpointAddress("http://196.44.191.67:7047/AppIntegration/WS/Speedlink%20Cargo%20Pvt%20Ltd/Codeunit/AppIntegrationMgt");
            //declare client
            AppIntegrationMgt_PortClient sc_client = new AppIntegrationMgt_PortClient(myBinding, endPoint);
            sc_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("Webuser", "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=");
            return sc_client;
        }





    }
}
