﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAPI.Models
{

    //error log 
    //all exceptions are logged in this class
    public class mErrorLog
    {
        public string _id { get; private set; } = Guid.NewGuid().ToString();//id
        public DateTime date { get; private set; } = DateTime.Now;//time the error occured
        public string error_message { get; set; }//the actual error message
        public string full_error_message { get; set; }//the actual error message
        public string activity_id { get; set; }//the reference to the activity performed
    }
}
