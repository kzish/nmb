﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAPI.Models
{
    /// <summary>
    /// the activity log to be logged for every event that occures in the system
    /// </summary>
    public class mActivityLog
    {
        public string _id { get; private set; } = Guid.NewGuid().ToString();//id
        public DateTime date { get; private set; } = DateTime.Now;//timestamp of event
        public string username { get; set; }//the username of the action/logged user
        public string user_device { get; set; }//device user accessed from
        public string user_ip_address { get; set; }//ip location of the user activity
        public string user_activity { get; set; }//description of the action that the user did
        public string data { get; set; }//log response
    }
}
