﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using APIClientConsole.Models;

public class Globals
{

    //config file path
    public static string simba_config = @"c:\rubiem\simba\simba_config.json";//this is keyvalue pairs json file with configuration
    public static string log_file = "C:\\rubiem\\simba\\simba_logs.txt";
    public static string db = "server=localhost;database=stanchart_simba_biller_console;user id=simba;password=Rubiem#99";

    public static string FormatMoney(double money)
    {

        if (money >= 1000000000000)
            return (money / 1000000000000).ToString("0.00B");//trillion

        if (money >= 1000000000)
            return (money / 1000000000).ToString("0.00B");//billion

        if (money >= 1000000)
            return (money / 1000000).ToString("0.00M");//million

        if (money >= 1000)
            return (money / 1000).ToString("0.00K");//thousand

        return money.ToString("0.00");//default

    }

    public static string FormatMoneyNODecimal(double money)//no decimal
    {

        if (money >= 1000000000000)
            return (money / 1000000000000).ToString("0B");//trillion

        if (money >= 1000000000)
            return (money / 1000000000).ToString("0B");//billion

        if (money >= 1000000)
            return (money / 1000000).ToString("0M");//million

        if (money >= 1000)
            return (money / 1000).ToString("0K");//thousand

        return money.ToString("0");//default

    }

    public static void log_data_to_file(string source, object data)
    {
        try
        {
            dynamic obj = new JObject();
            obj.source = source;
            obj.msg = data.ToString();
            var logdata = JsonConvert.SerializeObject(obj);
            System.IO.File.AppendAllText(log_file, logdata+ Environment.NewLine);
        }
        catch (Exception ex)
        {
            System.IO.File.AppendAllText(log_file, ex.Message+Environment.NewLine);
        }
    }

    public static int count_un_read_notifications(string billercode)
    {
        try
        {
            var db = new dbContext();
            var notifications_unread = db.MNotification.Where(i => i.BillerCode == billercode && i.IsRead == false).Count();
            db.Dispose();
            return notifications_unread;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }


    //use the application name as is verbatim for the microservices names
    /// <summary>
    /// return the key from the config file
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public static dynamic read_config(string key)
    {
        var file = System.IO.File.ReadAllText(simba_config);
        dynamic obj = JsonConvert.DeserializeObject(file);
        return obj.key;
    }
    /// <summary>
    ///write the key to the file
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public static void write_config(string key, string value)
    {

        if (!File.Exists(simba_config))
        {
            var path = Path.GetDirectoryName(simba_config);
            Directory.CreateDirectory(path);
            File.Create(simba_config).Close();
        };
        var file = File.ReadAllText(simba_config);
        dynamic obj = JsonConvert.DeserializeObject(file);
        if (obj == null)
        {
            obj = new JObject();
        }
        obj[key] = value;
        File.WriteAllText(simba_config, obj.ToString());
    }
}
