﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using APIClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIClientConsole.Controllers
{
    [Authorize(Roles = "api_client")]
    [Route("Home")]
    [Route("")]
    public class HomeController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }



        [Route("Dashboard")]
        [Route("")]
        public async Task<IActionResult> Dashboard(string date_from, string date_to)
        {
            ViewBag.title = "Dashboard";

            try
            {
                //get the user
                var user = db.AspNetUsers
                    .Where(i => i.Email == User.Identity.Name)
                    .Include(i => i.MApiClients)
                    .FirstOrDefault();

                string chart1data = string.Empty;
                string chart2data = string.Empty;

                //chart1 data
                var query = from T in db.MTransAction
                             join U in db.MApiUsageStats
                             on T.TrackingId equals U.TrackingId
                             where U.ClientName == user.MApiClients.ClientId
                             select new
                             {
                                 date = T.Date,
                                 biller_code = T.BillerCode,
                                 client_name = U.ClientName,
                                 transaction_amount = U.TransactionAmount,
                             };

                //filter by date
                if (!string.IsNullOrEmpty(date_from))
                {
                    query = query.Where(i => i.date >= DateTime.Parse(date_from));
                }
                if (!string.IsNullOrEmpty(date_to))
                {
                    query = query.Where(i => i.date <= DateTime.Parse(date_to));
                }


                //
                var query1_ = query.GroupBy(
                    i => i.biller_code,
                    (key, g) => new
                    {
                        biller_code = key,
                        transaction_amount = g.Sum(i => i.transaction_amount)
                    });
                foreach (var item in query.ToList())
                {
                    chart1data += $"['{item.biller_code}',{item.transaction_amount}],";
                }

                //
                var query2_ = query.GroupBy(
                    i => i.biller_code,
                    (key, g) => new
                    {
                        biller_code = key,
                        transaction_count = g.Count(i => !string.IsNullOrEmpty(i.biller_code))
                    });
                foreach (var item in query2_.ToList())
                {
                    chart2data += $"{{name: '{item.biller_code}', y: {item.transaction_count} }},";
                }

                ViewBag.chart1data = chart1data;
                ViewBag.chart2data = chart2data;

                //total transactions
                var total_transactions = Globals.FormatMoneyNODecimal(query2_.Sum(i => i.transaction_count));
                ViewBag.total_transactions = total_transactions;
                //total amount transacted
                var total_transactions_amount = Globals.FormatMoney((double)query1_.Sum(i => i.transaction_amount));
                ViewBag.total_transactions_amount = total_transactions_amount;

                ViewBag.date_from = date_from;
                ViewBag.date_to = date_to;

            }
            catch (Exception ex)
            {
                TempData["msg"] = "Error fetching data from server";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }

            return View();
        }


        [Route("Key")]
        public IActionResult Key()
        {
            ViewBag.title = "Key";
            var this_client = db.AspNetUsers
                .Where(i => i.Email == User.Identity.Name)
                .Include(i => i.MApiClients)
                .FirstOrDefault();
            ViewBag.client = this_client;
            return View();
        }


        [Route("Transactions")]
        public IActionResult Tranactions()
        {
            ViewBag.title = "Transactions";
            return View();
        }



    }
}
