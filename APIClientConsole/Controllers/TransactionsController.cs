﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using APIClientConsole.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace APIClientConsole.Controllers
{
    [Authorize(Roles = "api_client")]
    [Route("Transactions")]
    public class TransactionsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


        [Route("ListTransactions")]
        public IActionResult ListTransactions(int transaction_id, string date_from, string date_to)
        {
            ViewBag.title = "Transactions";
            //now using datatables , paging and pagenumber now obsolete
            var user = db.AspNetUsers
                .Where(i => i.Email == User.Identity.Name)
                .Include(i => i.MApiClients)
                .FirstOrDefault();

            //transactions based on usage stats
            var transactions = db.MApiUsageStats.AsQueryable();

            if (!string.IsNullOrEmpty(date_from))
            {
                var datefrom = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_from = datefrom.ToString("yyyy-MM-dd");
                transactions = transactions.Where(i => i.Date >= datefrom);
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var dateto = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_to = dateto.ToString("yyyy-MM-dd");
                transactions = transactions.Where(i => i.Date <= dateto);
            }
            transactions = transactions
                .Where(i => i.ClientName == user.MApiClients.ClientId)
                .OrderByDescending(i => i.Date);
            ViewBag.transactions = transactions.ToList();
            return View();
        }

        [Route("ListOrders")]
        public IActionResult ListOrders(string tracking_id)
        {

            ViewBag.title = "Orders";

            //now using datatables , paging and pagenumber now obsolete
            var user = db.AspNetUsers
                .Where(i => i.Email == User.Identity.Name)
                .Include(i => i.MApiClients)
                .FirstOrDefault();

            var transaction = db.MTransAction.Where(i => i.TrackingId == tracking_id).First();

            var orders = db.MBillerOrders.Where(i => i.TransactionId == transaction.Id)
                .OrderByDescending(i => i.Date);
            ViewBag.orders = orders.ToList();
            return View();
        }

    }
}
