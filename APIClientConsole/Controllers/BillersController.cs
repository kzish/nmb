﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using APIClientConsole.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APIClientConsole.Controllers
{
    [Route("Billers")]
    [Authorize(Roles = "api_client")]
    public class BillersController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet("ShowBillers/{category_id}")]
        public IActionResult ShowBillers(int category_id)
        {
            ///get the current logged in client
            var this_client = db.AspNetUsers
                .Where(i => i.Email == User.Identity.Name)
                .Include(i => i.MApiClients)
                .FirstOrDefault();

            //get category
            var category = db.EnumBillerCategory.Find(category_id).Category;
            ViewBag.title = $"Billers - {category}";

            //billers in this category
            var all_billers = db.MBillerInfo.Where(i => i.EnumBillerCategory == category_id).ToList();
            var billers_selected_by_this_client = new List<String>();
            
            foreach(var b in db.MClientBillers.Where(i => i.ClientId == this_client.MApiClients.ClientId).ToList())
            {
                billers_selected_by_this_client.Add(b.BillerId);
            }
            
            ViewBag.all_billers = all_billers;
            ViewBag.billers_selected_by_this_client = billers_selected_by_this_client;
            ViewBag.categoryId = category_id;

            return View();
        }

        [HttpPost("SaveSelection")]
        public JsonResult SaveSelection(List<string> selectedBillers,int categoryId)
        {
            try
            {
                ViewBag.title = "SaveSelection";

                ///get the current logged in client
                var this_client = db.AspNetUsers
                    .Where(i=>i.Email==User.Identity.Name)
                    .Include(i=>i.MApiClients)
                    .FirstOrDefault();

                //filter the billers who are in this category
                //and are linked to this apiclient
                var query = from BI in db.MBillerInfo.Where(i=>i.EnumBillerCategory==categoryId)
                            join CB in db.MClientBillers.Where(i=>i.ClientId==this_client.MApiClients.ClientId) on BI.Id equals CB.BillerId into GJ
                            from X in GJ.DefaultIfEmpty()
                            select new MClientBillers()
                            {
                                ClientId = X.ClientId,
                                BillerId = X.BillerId
                            };

                var range_to_remove = new List<MClientBillers>();
                foreach (var r in query.ToList())
                {
                    var CB = db.MClientBillers.Where(i => i.BillerId == r.BillerId && i.ClientId == r.ClientId).FirstOrDefault();
                    if(CB!=null)
                    {
                        range_to_remove.Add(CB);
                    }
                }
                //clear the above objects, which are the current selection in this category
                db.MClientBillers.RemoveRange(range_to_remove);

                //now add the current selection
                foreach(var biller_id in selectedBillers)
                {
                    var client_biller = new MClientBillers();
                    client_biller.BillerId = biller_id;
                    client_biller.ClientId = this_client.MApiClients.ClientId;
                    db.MClientBillers.Add(client_biller);
                }
                db.SaveChanges();

                return Json(new
                {
                    res = "ok",
                    msg = "Saved"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }
    }
}
