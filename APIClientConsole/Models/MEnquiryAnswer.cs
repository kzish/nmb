﻿using System;
using System.Collections.Generic;

namespace APIClientConsole.Models
{
    public partial class MEnquiryAnswer
    {
        public int Id { get; set; }
        public int? Questionid { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int? ServiceEnquiryId { get; set; }
    }
}
