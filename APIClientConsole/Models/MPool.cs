﻿using System;
using System.Collections.Generic;

namespace APIClientConsole.Models
{
    public partial class MPool
    {
        public int Id { get; set; }
        public string Poolname { get; set; }
        public string Pooldiscription { get; set; }
        public DateTime? Poolstartdate { get; set; }
        public DateTime? Poolenddate { get; set; }
        public bool? Active { get; set; }
        public decimal? Poolcontributionamount { get; set; }
        public int? EnumContributioncycle { get; set; }
        public string Terms { get; set; }
    }
}
