﻿using System;
using System.Collections.Generic;

namespace APIClientConsole.Models
{
    public partial class MErrorLog
    {
        public int Id { get; set; }
        public string ErrorMessage { get; set; }
        public string FullErrorMessage { get; set; }
        public int? ActivityId { get; set; }
    }
}
