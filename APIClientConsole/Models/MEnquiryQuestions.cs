﻿using System;
using System.Collections.Generic;

namespace APIClientConsole.Models
{
    public partial class MEnquiryQuestions
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Billercode { get; set; }
        public bool? Status { get; set; }
    }
}
