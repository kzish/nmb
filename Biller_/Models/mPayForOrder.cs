﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models
{
    public class mPayForOrder
    {
        public string BillerCode { get; set; }
        public mPayerDetails PayerDetails { get; set; }
        public string TrackingId { get; set; }
        public decimal AmountPaid { get; set; }
        public mPayerSCAccountDetails PayerSCAccountDetails { get; set; }
        public string ClientIndentifier { get; set; }
        public string OrderID { get; set; } = "";
        

}
    public class ZSSBillPaymentsDTO
    {
        public PayerDetails payerdetails { get; set; }
        public string billAccount { get; set; }
        public long billAmount { get; set; }
        public string provider { get; set; }
        public string product { get; set; }
       // public BillData[] billDatas { get; set; }
        public string participantRef { get; set; }
        public string RequestType { get; set; }
    }
    public class PayerDetails
    {
    public string payerAccountReferenceField { get; set; }
    
    public string payerCardReferenceField { get; set; }

    public string payerMsisdnField { get; set; }

    public string payerNameField { get; set; }

    public string payerReferenceField { get; set; }
}
    public class mPayForWithAdditionalData
    {
        public string BillerCode { get; set; }
        public mPayerDetails PayerDetails { get; set; }
        public string TrackingId { get; set; }
        public mPayerSCAccountDetails PayerSCAccountDetails { get; set; }
        public  string ClientIndentifier { get; set; }

    }
    public class mPayForWithoutOrder
    {
        public string BillerCode { get; set; }
        public mPayerDetails PayerDetails { get; set; }
        public string TrackingId { get; set; }
        public mPayerSCAccountDetails PayerSCAccountDetails { get; set; }
        public string ClientIndentifier { get; set; }

    }
}
