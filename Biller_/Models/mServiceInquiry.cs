﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models
{
    public class mServiceInquiry
    {
        public string InquiryNumber { get; set; }
        public string payerName { get; set;}
        public string phoneNumber { get; set; }
        public string billercode { get; set; }
        public   List<mAnswer> answers { get; set; } = new List<mAnswer>();
      


    }

    public class mInquiryQuestions
    {

        public int questionid { get; set; }
        public string question { get; set; }

        public string billercode { get; set; }
        public bool status { get; set; }
    }
     public class mInquiryAnswer
    {
        public string payerName { get; set; }
        public string phoneNumber { get; set; }
        public int questionid { get; set; }
        public string billercode { get; set; }
        public string  answer { get; set; }
        public string InquiryNumber { get; set; }
        public bool Actioned { get; set; }
    }
    public class mAnswer
    {
        public int questionid { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        
    }

}
