﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models
{
    public class mBiller
    {
        public string BillerCode { get; set; }
        public mBillerInfo BillerInfo { get; set; }
        public mBankAccount BankAccounts { get; set; }
        public mAddress Address { get; set; }

    }

    public class mAddress
    {

        public static readonly mBiller billerdata = new mBiller();
        public string BillerCode { get; private set; } 
        public string Street { get; set; }
        
        public string City { get; set; }
        
    }

    public class mBillerInfo
    {
        public static readonly mBiller billerdata = new mBiller();
        [Required]
        public string BillerCode { get; set; } 
        [Required]
        public string BillerName { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public string ContactPhone { get; set; }
        [Required]
        public string ContactEmail { get; set; }
        [Required]
        public eBillerType BillerType { get; set; }
        [Required]
        public eBillerCategory BillerCategory { get; set; }
        [Required]
        public bool hasProducts { get; set; }
        [Required]
        public bool hasAccounts { get; set; }
        [Required]
        public bool hasCart { get; set; }
        [Required]
        public ClientIndentifier ClientIndentifier { get; set; }
        [Required]
        public bool hasAdditionalData { get; set; }
        public string RequestEndPoint { get; set; }

        public string CredentialsUsername { get; set; }
        public string CredentialsPassword { get; set; }
        public string ConnectionType { get; set; }

        public int PaymentCycle { get; set; }
     
        [Required]
        public bool BillerStatus { get; set; }
         public string Enabler { get; set; }
    }

    public enum ClientIndentifier
    {
       token,
       billerCustomerAccount
    }
    public enum eBillerCategory
    {

        UNIVERSITIES,
        SCHOOLS,
        HOSPITALS,
        HOTELS,
        SUPERMARKETS,
        PHARMACEUTICALS,
        AUTOMOTIVE,
        TRANSPORT,
        FUEL,
        INSURANCE,
        CHURCHES,
        FASTFOODS
    }
    public enum eBillerType
    {
        ONLINE,
        OFFLINE
    }
}
