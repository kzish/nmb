﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models
{
    public class mBillerCart
    {
      int id { get; set; }
  

       public decimal Total { get; set; }

  public List <mCartProduct> mCartProduct { get; set; }

    }

    public class mBillerCartRequest
    {
        public string billercode { get; set; }
        public List<mCartProduct> items { get; set; } = new List<mCartProduct>();
       
     

    }
    public class mBillerCartReq
    {

        public string token { get; set; }
      
        public mCartProduct mBillerProduct { get; set; }

    }
    public class mCartProduct
    {
        public string token { get; set; }
        public  mBillerProduct mBillerProduct { get; set; }
        public decimal quantity { get; set; }
}
}
