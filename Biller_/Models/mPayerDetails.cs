﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biller.Models
{
    public class mPayerDetails
    {
        string payerDeatailsID { get; set; }
        public string payerName { get; set; }
        public string payerPhoneNumber { get; set; }
        public string payerReference { get; set; }
        public string payerDiliveryAddress { get; set; }
    }
    public class mCheckOut
    {
       public string billercode { get; set; }
        public string payerName { get; set; }
        public string payerPhoneNumber { get; set; }
        public string payerReference { get; set; }
        public string payerDiliveryAddress { get; set; }
        public string payerCity { get; set; }
    }

}
