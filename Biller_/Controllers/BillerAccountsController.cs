﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Biller.Models;
using Biller.Database;


namespace OnlineBillers.Controllers
{
    [Route("scpayment/v1")]
    public class BillerAccountsController : Controller
    {
        
        [HttpPost("AddBiller")]
        public JsonResult AddBiller(mBiller Biller)
        {
            try
            {
               
            using (var db = new db())
              {

                   db.Insert(Biller.BillerInfo);
                    db.Insert(Biller.BankAccounts);
                    db.Insert(Biller.Address);
                }


                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Biller",

                });
            }

            catch(Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }

        [HttpPut("UpdateBiller")]
        public  JsonResult UpdateBiller(string BillerCode)
        {
            return null;
        }

        [HttpGet("GetBillers")]
        public JsonResult GetBillers()
        {
            try
            {
                var db = new db();
                var BillerData = (from trans in db.mBiller

                                  select trans).ToList();


                //return success
                return Json(new
                {
                    res = "ok",
                    data = Json(BillerData)
                });
            }
            catch( Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    data = ex.Message
                });
            }
            
        }
    }
}
