﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Biller.Models;
using Biller.Database;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Text;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineBillers.Controllers
{
    [Route("scpayment/v1")]
    public class BillerProcessController : Controller
    {


        [HttpGet("getBillerProductOnline")]
        public JsonResult GetBillerProduct(string billerCode)
        {

            var baseurl = "";
            bool supportproduct = false;
            mBillerInfo billerData = new mBillerInfo();
            try
            {
                if (string.IsNullOrEmpty(billerCode))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code parameter is missing"
                    });
                }

                var db = new db();
                billerData = (from biller in db.mBiller
                              where biller.BillerCode == billerCode
                              select biller).FirstOrDefault();



                if (billerData == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code is invalid"
                    });
                }


                baseurl = billerData.RequestEndPoint.ToString();
                if (baseurl == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller url not configured"
                    });
                }
                supportproduct = billerData.hasProducts;
                if (supportproduct == true)
                {
                    var client = new HttpClient();

                    var response = client.GetAsync(baseurl + "/" + billerCode + "/GetBillerProducts").Result;

                    return Json(new
                    {
                        responsecode = "ok",
                        responsemessage = JObject.Parse(response.Content.ReadAsStringAsync().Result)
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller does not support the get product feature"

                    });
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("postBillerProductOffline")]
        public JsonResult postBillerProductOffline([FromBody]mProductOffline product)
        {
            try
            {

                using (var db = new db())
                {

                    db.Insert(product);
                
                }


                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Product",

                });
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }

        }
        [HttpPost("postBillerInquiryQuestions")]
        public JsonResult postBillerInquiryQuestions([FromBody]mInquiryQuestions questions)
        {
            try
            {

                using (var db = new db())
                {

                    db.Insert(questions);

                }


                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Inquiry Questions",

                });
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }

        }

        [HttpPost("postBillerInquiry")]
        public JsonResult postBillerInquiry([FromBody]mServiceInquiry inquiry)
        {
            try
            { 
                mInquiryAnswer answer = new mInquiryAnswer();
                var db = new db();
                foreach (dynamic item in inquiry.answers)
                {
                    answer.InquiryNumber = inquiry.InquiryNumber;
                    answer.payerName = inquiry.payerName;
                    answer.phoneNumber = inquiry.phoneNumber;
                    answer.questionid = item.questionid;
                    answer.answer = item.answer;
                    answer.billercode = inquiry.billercode;
                    db.Insert(answer);

                }
               


                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Inquiry Questions",

                });
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }

        }
        [HttpGet("GetBillerInquiries")]
        public JsonResult GetBillerInquiries(string billercode)
        {
            try
            {
                List<mServiceInquiry> serviceInquirieslist = new List<mServiceInquiry>();
                mServiceInquiry serviceInquiry = new mServiceInquiry();
                var db = new db();
                var BillerInquiries = (from Inquiry in db.inquiry
                                       where Inquiry.billercode == billercode && Inquiry.Actioned == false
                                       select Inquiry).ToList();

                var result = BillerInquiries.Select(o => o.InquiryNumber).Distinct().ToList();
                foreach(dynamic inquiry in result)
                {
                    serviceInquiry.InquiryNumber = inquiry.InquiryNumber;
                    serviceInquiry.payerName = inquiry.payerName;
                    serviceInquiry.phoneNumber = inquiry.phoneNumber;
                    var results = BillerInquiries.Select(o => o.InquiryNumber== inquiry.InquiryNumber).ToList();
                    foreach (dynamic inq in results)
                    {
                        serviceInquiry.answers.Add(inq.answers);
                            
                    }
                    serviceInquirieslist.Add(serviceInquiry);
                }
                if (BillerInquiries != null)
                {
                    return Json(new
                    {
                        res = "ok",
                        data = Json(serviceInquirieslist)
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "ok",
                        mesg = "No pending Inquiries"
                    });
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    data = ex.Message
                });
            }

        }
        [HttpGet("getBillerInquiryQuestions")]
        public JsonResult GetBillerInquiryQuestions( string billercode)
        {
            try
            {
                var db = new db();
                var BillerQuestions= (from Questions in db.questions
                                       where Questions.billercode == billercode && Questions.status == true
                                  select Questions).ToList();


               if(BillerQuestions != null)
                {
                    return Json(new
                    {
                        res = "ok",
                        data = Json(BillerQuestions)
                    });
                }
               else
                {
                    return Json(new
                    {
                        res = "ok",
                       mesg = "No Biller Inquiry Questions"
                    });
                }
               
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    data = ex.Message
                });
            }

        }
        [HttpGet("GetBillerInquiriesHistory")]
        public JsonResult GetBillerInquiriesHistory(string billercode)
        {
            try
            {
                var db = new db();
                var BillerInquiries = (from Inquiry in db.inquiry
                                       where Inquiry.billercode == billercode 
                                       select Inquiry).ToList();


                if (BillerInquiries != null)
                {
                    return Json(new
                    {
                        res = "ok",
                        data = Json(BillerInquiries)
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "ok",
                        mesg = "No Inquiries For biller code " + billercode
                    });
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    data = ex.Message
                });
            }

        }
        [HttpPost("UpdateBillerProductOffline")]
        public JsonResult UpdateBillerProductOffline([FromBody]mProductOffline product)
        {
            try
            {
        mProductOffline productoffline = new mProductOffline();
                var db = new db();

                productoffline = (from products in db.product
                                      where products.billercode == product.billercode
                                      select products).FirstOrDefault();
                    if(productoffline!=null)
                    {
                        db.Update(product);
                    return Json(new
                    {
                        res = "ok",
                        msg = "Successfully Updated Product",

                    });
                }
                    else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "You have supplied a product with an Invalid Product Code"

                    });
                }

            }
                   

                


              

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }

        }
        [HttpPost("UpdateBillerInquiry")]
        public JsonResult UpdateBillerInquiry(string InquiryNumber , bool status=false)
        {
            try
            {
                List<mInquiryAnswer> answer = new List<mInquiryAnswer>();
                mInquiryAnswer answers = new mInquiryAnswer();
                var db = new db();

               answer = (from Inquiries in db.inquiry
                                  where Inquiries.InquiryNumber == InquiryNumber
                                  select Inquiries).ToList();
                if (answer != null)
                {
                    
                    foreach (dynamic items in answer)
                    {
                        answers = items;
                        answers.Actioned = status;
                        db.Update(answers);
                    }
                    
                    return Json(new
                    {
                        res = "ok",
                        msg = "Successfully Updated Your Inquiry",

                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "You have supplied an Invalid Inquiry Number"

                    });
                }

            }







            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }

        }

        [HttpGet("getBillerProductOffline")]
        public JsonResult GetBillerProductOffline(string billerCode)
        {

            
            bool supportproduct = false;
            mBillerInfo billerData = new mBillerInfo();
            List <mProductOffline> productoffline = new List<mProductOffline>();
            try
            {
                if (string.IsNullOrEmpty(billerCode))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code parameter is missing"
                    });
                }

                var db = new db();
                billerData = (from biller in db.mBiller
                              where biller.BillerCode == billerCode
                              select biller).FirstOrDefault();



                if (billerData == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code is invalid"
                    });
                }


                
                supportproduct = billerData.hasProducts;
                if (supportproduct != true)
                {
                
                    productoffline = (from products in db.product
                                  where products.billercode == billerCode && products.disabled == false
                                  select products).ToList();
                
                    
                }
                if (productoffline == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller does not support the get product feature"

                    });
                }
                else
                {
                    return Json(new
                    {
                        responsecode = "ok",
                        responsemessage = productoffline
                    });
                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("postBillerCart")]
        public JsonResult postBillerCart([FromBody] mBillerCartRequest BillerCart)
        {
            var baseurl = "";
            bool supportcart = false;
            mBillerInfo billerData = new mBillerInfo();
            try
            {

                if (string.IsNullOrEmpty(BillerCart.billercode))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code is missing"
                    });
                }

                if (string.IsNullOrEmpty(BillerCart.items[0].token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Token  is missing"
                    });
                }

                var db = new db();
                billerData = (from biller in db.mBiller
                              where biller.BillerCode == BillerCart.billercode
                              select biller).FirstOrDefault();


                if (billerData == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code is invalid"
                    });
                }

                baseurl = billerData.RequestEndPoint.ToString();
                if (baseurl == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller url not configured"
                    });
                }

                supportcart = billerData.hasCart;
                if (supportcart == true)
                {
                    var client = new HttpClient();

                    var response = client.PostAsJsonAsync("http://localhost:2009/Cart/AddToCart", BillerCart.items).Result;

                    return Json(new
                    {

                        responsemessage = JObject.Parse(response.Content.ReadAsStringAsync().Result)
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller does not support cart"

                    });
                }

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("BillerPayment")]
        public JsonResult postBillerPayment([FromBody] mPayForOrder PayForOrder)
        {
            var baseurl = "";
          
            var Enabler = "";
            mBillerInfo billerData = new mBillerInfo();
            try
            {
                if (string.IsNullOrEmpty(PayForOrder.BillerCode))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code parameter is missing"
                    });
                }

                var db = new db();
                billerData = (from biller in db.mBiller
                              where biller.BillerCode == PayForOrder.BillerCode
                              select biller).FirstOrDefault();

                if (billerData == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Biller code is invalid"
                    });
                }


                baseurl = billerData.RequestEndPoint.ToString();
                if (baseurl == null)
                 
                {
                return Json(new
                {
                    res = "err",
                    msg = "Biller url not configured"
                });
            }
            Enabler = billerData.Enabler;
            if (Enabler == "ZSS")
            {
                ZSSBillPaymentsDTO ZSSBillPaymentsDTO = new ZSSBillPaymentsDTO();
                ZSSBillPaymentsDTO.payerdetails.payerAccountReferenceField = "";
                ZSSBillPaymentsDTO.payerdetails.payerMsisdnField = PayForOrder.PayerDetails.payerPhoneNumber;
                ZSSBillPaymentsDTO.payerdetails.payerNameField = PayForOrder.PayerDetails.payerName;
                ZSSBillPaymentsDTO.billAccount = PayForOrder.ClientIndentifier;
                ZSSBillPaymentsDTO.billAmount = long.Parse(PayForOrder.AmountPaid.ToString());
                ZSSBillPaymentsDTO.provider = PayForOrder.BillerCode;
                ZSSBillPaymentsDTO.product = "Default";

                var client = new HttpClient();

                var response = client.PostAsJsonAsync(baseurl + "/ZSS/GetBillerProducts", ZSSBillPaymentsDTO).Result;

                return Json(new
                {
                    responsecode = "ok",
                    responsemessage = JObject.Parse(response.Content.ReadAsStringAsync().Result)
                });




            }
            
            mReceipts Receipts = new mReceipts();
            Receipts.AmountPaid = PayForOrder.AmountPaid;
            Receipts.BillerCode = PayForOrder.BillerCode;
            Receipts.OrderID = PayForOrder.OrderID;
            Receipts.PayerName = PayForOrder.PayerDetails.payerName;
            Receipts.PayerPhoneNumber = PayForOrder.PayerDetails.payerPhoneNumber;
            Receipts.Posted = false;
            Receipts.TrackingId = PayForOrder.TrackingId;
            Receipts.TransactionDate = DateTime.Now.ToString();
            Receipts.TransactionNumber = "";
            Receipts.PayerAccount = PayForOrder.ClientIndentifier;
            Receipts.PaidToBiller = false;

          
                db.Insert(Receipts);
           





            return Json(new
            {
                res = "ok",
                msg = "Payment successfully made"

            });
        }
    
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpGet("getBillerCart")]
            public JsonResult GetBillerCart(string billerCode,  string mBillerCartID)
            {
                try
                {


                    return Json(new
                    {
                        res = "ok",
                        msg = ""

                    });
                }

                catch (Exception ex)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = ex.Message

                    });
                }


            }

            [HttpGet("PostBillerCheckOut")]
            public JsonResult PostBillerCheckout( )
            {
                try
                {
                string token; string delivery_address; string delivery_name; string delivery_city;

                    return Json(new
                    {
                        res = "oka",
                        msg = "Successfully Checked out"

                    });
                }

                catch (Exception ex)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = ex.Message

                    });
                }


            }
            [HttpGet("postBillerOrder")]
            public JsonResult PostBillerOrder([FromBody]  string billerCode, [FromBody] string mBillerCartID)
            {
                try
                {


                    return Json(new
                    {
                        res = "ok",
                        msg = "Biller Order Succesfully added"

                    });
                }

                catch (Exception ex)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = ex.Message

                    });
                }


            }
           
        }
    }

