﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Biller.Models;

namespace Biller.Database
{
    public class db : LinqToDB.Data.DataConnection
    {
        public db() : base(Database.sql_server_db.configuration) { }
        public ITable<mBillerInfo> mBiller => GetTable<mBillerInfo>();
        public ITable<mBankAccount> mBankAccount => GetTable<mBankAccount>();
        public ITable<mInquiryQuestions> questions => GetTable<mInquiryQuestions>();
        public ITable<mInquiryAnswer> inquiry=> GetTable<mInquiryAnswer>();
        public ITable<mProductOffline> product => GetTable<mProductOffline>();
        public ITable<mAddress> mAddress => GetTable<mAddress>();
        public ITable<mReceipts> mReceipts=> GetTable<mReceipts>();
    }



}
