﻿using DeviceDetectorNET;
using DeviceDetectorNET.Parser;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StanChartAccounts;
using System;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Transactions.Models;

/// <summary>
/// handle client account information
/// </summary>
namespace SCCoreBanking.Controllers
{

    /// <summary>
    /// handle client account information
    /// </summary>
    [Route("Account")]
    public class AccountController : Controller
    {

        const string captureSystem = "SCRubiemInnovation";
        const string countryCode = "ZW";

        private IHttpContextAccessor _accessor;

        public AccountController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            DeviceDetector.SetVersionTruncation(VersionTruncation.VERSION_TRUNCATION_NONE);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the header for the request, each request must have a header
        /// </summary>
        /// <param name="trackingId">unique guid</param>
        /// <param name="eventType">action performed eg. postTransaction/getTransActionStatus/getTransActionPostingDetails/reserveTransaction/postChequeTransaction</param>
        /// <returns></returns>
        private SCBMLHeaderType get_header(string trackingId, string eventType)
        {
            //
            #region messageSenderType
            //
            messageSenderType messageSenderType = new messageSenderType();
            messageSenderType.messageSender = null;
            messageSenderType.senderDomain = null;
            messageSenderType.countryCode = countryCode;
            //
            #endregion messageSenderType

            #region originationDetailsType
            //
            originationDetailsType originationDetailsType = new originationDetailsType();
            originationDetailsType.messageSender = messageSenderType;
            originationDetailsType.messageTimestamp = DateTime.Now;
            originationDetailsType.initiatedTimestamp = DateTime.Now;
            originationDetailsType.trackingId = trackingId;
            originationDetailsType.correlationID = null;
            originationDetailsType.conversationID = null;
            originationDetailsType.customSearchID = null;
            originationDetailsType.batchID = null;
            originationDetailsType.serviceBusID = null;
            originationDetailsType.validFrom = DateTime.Now;
            originationDetailsType.validTo = DateTime.Now.AddDays(1);
            originationDetailsType.timeToLive = DateTime.Now.AddDays(1);
            originationDetailsType.priority = PriorityTypeEnum.Item1;
            originationDetailsType.checksum = null;
            originationDetailsType.compressionAlgorithm = null;
            originationDetailsType.encoding = null;
            originationDetailsType.possibleDuplicate = true;
            //
            #endregion originationDetailsType

            #region messageDetailsType
            messageDetailsType messageDetailsType = new messageDetailsType();
            messageDetailsType.messageVersion = decimal.Parse("6.0");//todo: check version
            messageDetailsType.messageType = new messageType
            {
                typeName = "",
                subType = new subType()
                {
                    subTypeName = "",
                    subTypeScheme = ""
                }
            };
            //
            #endregion messageDetailsType

            #region SCBMLHeaderType
            SCBMLHeaderType SCBMLHeaderType = new SCBMLHeaderType();
            SCBMLHeaderType.messageDetails = messageDetailsType;
            SCBMLHeaderType.originationDetails = originationDetailsType;
            SCBMLHeaderType.captureSystem = captureSystem;
            SCBMLHeaderType.process = new ProcessType()
            {
                processName = "",
                eventType = "",
                lifecycleState = "",
                workflowState = "",
                action = ""
            };
            SCBMLHeaderType.metadata = null;
            SCBMLHeaderType.messageHistory = null;
            SCBMLHeaderType.exceptions = null;
            //
            #endregion

            //create a header for the request
            #region header
            SCBMLHeaderType header = SCBMLHeaderType;
            header.messageDetails = messageDetailsType;
            header.originationDetails = originationDetailsType;
            header.captureSystem = captureSystem;
            header.process = new ProcessType
            {
                processName = "",
                eventType = eventType,
                lifecycleState = "",
                workflowState = "",
                action = ""
            };
            header.metadata = null;
            header.messageHistory = null;
            header.exceptions = null;
            #endregion header

            return header;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the scb client for the request
        /// </summary>
        /// <returns></returns>
        public Account_PortTypeClient get_client()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            //configure end point
            var endPoint = new EndpointAddress("https://gateway-hk.npedmi.standardchartered.com:5054/ws/scbCoreBankingAccount.v6.ws.providerV2:Account/scbCoreBankingAccount_v6_ws_providerV2_Account_Port");
            //declare client
            Account_PortTypeClient sc_client = new Account_PortTypeClient(myBinding, endPoint);
            sc_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("zimra2016", "Standard12", "zone2.scb.net");
            return sc_client;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// uploads data to the given url, used to communicate with the other microservices, returns string response
        /// </summary>
        /// <param name="uri">the url to which data must be posted</param>
        /// <param name="obj">the object to be posted</param>
        private void upload_data(string uri, object obj)
        {
            Task.Run(() =>
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.Headers["Content-Type"] = "application/json";
                    var post_object = JsonConvert.SerializeObject(obj);
                    string res = wc.UploadString(uri, post_object);
                }
                catch (Exception ex)
                {
                    globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.upload_data", ex);
                }
            });
        }


        /// <summary>
        /// returns the account details associated with an account
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="accountCurrencyCode"></param>
        /// <returns></returns>
        [HttpGet("getAccountDetails")]
        public JsonResult getAccountDetails(string accountNumber, string accountCurrencyCode)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountNumber param missing"
                });
            }

            if (String.IsNullOrEmpty(accountCurrencyCode))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountCurrencyCode param missing"
                });
            }


            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.getAccountDetails()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            try
            {

                //create client
                var sc_client = get_client();
                //create request
                var getAccountDetailsReq = new getAccountDetailsReq();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();

                //create request payload
                var getAccountDetailsReqPayload = new getAccountDetailsReqPayload();
                getAccountDetailsReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getAccountDetailsReqPayload.payloadVersion = "1.0";
                getAccountDetailsReqPayload.getAccountDetailsReq = new getAccountDetails1()
                {
                    accountNumber = accountNumber,
                    accountCurrencyCode = accountCurrencyCode,
                    accountProductCode = "",
                    addressUseCode = "",
                    companyID = "",
                };

                //assign header and payload to the request
                getAccountDetailsReq.header = get_header(trackingId, "getAccountDetails");
                getAccountDetailsReq.getAccountDetailsReqPayload = getAccountDetailsReqPayload;

                //execute request
                var response = sc_client.getAccountDetailsAsync(getAccountDetailsReq).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.getAccountDetails().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);



                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.getAccountDetailsResponse.getAccountDetailsResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.getAccountDetails", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        [HttpGet("getStatus")]
        public JsonResult getStatus(string accountNumber, string currencyCode)
        {

            if (String.IsNullOrEmpty(accountNumber))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountNumber param missing"
                });
            }

            if (String.IsNullOrEmpty(currencyCode))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "currencyCode param missing"
                });
            }


            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.getStatus()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            try
            {

                //create client
                var sc_client = get_client();
                //create request
                var getStatusReq = new getStatusReq();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();

                //transactionInfo
                var transactionInfo = new transactionInfo();
                transactionInfo.transactionRisk = new transactionRisk();
                transactionInfo.transactionRisk.creditDebitIndicator = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = ""
                };
                transactionInfo.transactionRisk.transactionType = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = ""
                };
                transactionInfo.transactionRisk.transactionAmount = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = ""
                };
                transactionInfo.transactionRisk.channelID = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = ""
                };
                //accountInfo
                var accountInfo = new accountInfo();
                accountInfo.accountNumber = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = accountNumber
                };
                accountInfo.currencyCode = new docTypeRef_com_dataTypeString()
                {
                    forModify = false,
                    forModifySpecified = false,
                    Value = currencyCode
                };

                //create request payload
                getStatusReqPayload getStatusReqPayload = new getStatusReqPayload();
                getStatusReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getStatusReqPayload.payloadVersion = "1.0";
                getStatusReqPayload.getAccountStatusInfo = new account();
                getStatusReqPayload.getAccountStatusInfo.transactionInfo = transactionInfo;
                getStatusReqPayload.getAccountStatusInfo.accountInfo = accountInfo;

                //assign header and payload to the request
                getStatusReq.header = get_header(trackingId, "getStatus");
                getStatusReq.getStatusReqPayload = getStatusReqPayload;

                //execute request
                var response = sc_client.getStatusAsync(getStatusReq).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.getStatus().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);



                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.getStatusRes.getStatusResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.getStatus", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="accountCurrencyCode"></param>
        /// <returns></returns>
        [HttpGet("getAccountBalances")]
        public JsonResult getAccountBalances(string accountNumber, string accountCurrencyCode)
        {

            if (String.IsNullOrEmpty(accountNumber))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountNumber param missing"
                });
            }

            if (String.IsNullOrEmpty(accountCurrencyCode))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountCurrencyCode param missing"
                });
            }

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.getAccountBalances()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            try
            {

                //create client
                var sc_client = get_client();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();
                //create request
                var getAccountBalancesReq = new getAccountBalancesReq();


                //create request payload
                getAccountBalancesReqPayload getAccountBalancesReqPayload = new getAccountBalancesReqPayload();
                getAccountBalancesReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getAccountBalancesReqPayload.payloadVersion = "1.0";
                getAccountBalancesReqPayload.getAccountBalancesReq = new getAccountBalances1()
                {
                    accountNumber = accountNumber,
                    accountCurrencyCode = accountCurrencyCode,
                    multiCurrencyFlag = "",
                    accountProductCode = ""
                };

                //assign header and payload to the request
                getAccountBalancesReq.header = get_header(trackingId, "getAccountBalances");
                getAccountBalancesReq.getAccountBalancesReqPayload = getAccountBalancesReqPayload;

                //execute request
                var response = sc_client.getAccountBalancesAsync(getAccountBalancesReq).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.getAccountBalances().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);



                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.getAccountBalancesResponse.getAccountBalancesResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.getAccountBalances", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        [HttpGet("notifyAccountBalanceChange")]
        public JsonResult notifyAccountBalanceChange(string accountNumber)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountNumber param missing"
                });
            }

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.notifyAccountBalanceChange()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            try
            {

                //create client
                var sc_client = get_client();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();
                //create request
                var notifyAccountBalanceChangeReq = new notifyAccountBalanceChangeReq();


                //create request payload
                notifyAccountBalanceChangeReqPayload notifyAccountBalanceChangeReqPayload = new notifyAccountBalanceChangeReqPayload();
                notifyAccountBalanceChangeReqPayload.payloadFormat = PayloadFormatEnum.XML;
                notifyAccountBalanceChangeReqPayload.payloadVersion = "1.0";
                notifyAccountBalanceChangeReqPayload.notifyAccountBalanceChangeReq = new notifyAccountBalanceChange1()
                {
                    accountNumber = accountNumber
                };


                //assign header and payload to the request
                notifyAccountBalanceChangeReq.header = get_header(trackingId, "notifyAccountBalanceChange");
                notifyAccountBalanceChangeReq.notifyAccountBalanceChangeReqPayload = notifyAccountBalanceChangeReqPayload;

                //execute request
                var response = sc_client.notifyAccountBalanceChangeAsync(notifyAccountBalanceChangeReq).Result;

                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.notifyAccountBalanceChange().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);


                if (response.notifyAccountBalanceChangeResponse == null)
                {
                    //create response
                    return Json(new
                    {
                        res = "err",
                        data = "null response"
                    });
                }
                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.notifyAccountBalanceChangeResponse.notifyAccountBalanceChangeResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.notifyAccountBalanceChange", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionCurrencyCode"></param>
        /// <returns></returns>
        [HttpGet("getForexConversionRate")]
        public JsonResult getForexConversionRate(string transactionCurrencyCode)
        {

            if (String.IsNullOrEmpty(transactionCurrencyCode))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "transactionCurrencyCode param missing"
                });
            }

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.getForexConversionRate()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);

            try
            {

                //create client
                var sc_client = get_client();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();
                //create request
                var getForexConversionRateReq = new getForexConversionRateReq();


                //create request payload
                getForexConversionRateReqPayload getForexConversionRateReqPayload = new getForexConversionRateReqPayload();
                getForexConversionRateReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getForexConversionRateReqPayload.payloadVersion = "1.0";
                getForexConversionRateReqPayload.getForexConversionRateReq = new getForexConversionRate1()
                {
                    transactionCurrencyCode = transactionCurrencyCode,
                    transactionAmount = "1",
                    channelID = "",
                    rateTypeCode = "",
                };


                //assign header and payload to the request
                getForexConversionRateReq.header = get_header(trackingId, "getForexConversionRate");
                getForexConversionRateReq.getForexConversionRateReqPayload = getForexConversionRateReqPayload;

                //execute request
                var response = sc_client.getForexConversionRateAsync(getForexConversionRateReq).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.getForexConversionRate().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);


                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.getForexConversionRateResponse.getForexConversionRateResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.getForexConversionRate", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="accountCurrencyCode"></param>
        /// <param name="transactionFromDate"></param>
        /// <param name="transactionToDate"></param>
        /// <param name="displayPage"></param>
        /// <param name="startingItemNo"></param>
        /// <param name="limitSearch"></param>
        /// <returns></returns>
        [HttpGet("getTransactionDetails")]
        public JsonResult getTransactionDetails(string accountNumber
            , string accountCurrencyCode
            , string transactionFromDate
            , string transactionToDate
            , string displayPage
            , string startingItemNo
            , string limitSearch)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountNumber param missing"
                });
            }

            if (String.IsNullOrEmpty(accountCurrencyCode))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "accountCurrencyCode param missing"
                });
            }

            if (String.IsNullOrEmpty(transactionFromDate))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "transactionFromDate param missing"
                });
            }


            if (String.IsNullOrEmpty(transactionToDate))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "transactionToDate param missing"
                });
            }

            if (String.IsNullOrEmpty(displayPage))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "displayPage param missing"
                });
            }


            if (String.IsNullOrEmpty(startingItemNo))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "startingItemNo param missing"
                });
            }


            if (String.IsNullOrEmpty(limitSearch))
            {
                //create response
                return Json(new
                {
                    res = "err",
                    data = "limitSearch param missing"
                });
            }


            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.AccountController.getTransactionDetails()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            try
            {

                //create client
                var sc_client = get_client();
                //create request
                var getTransactionDetailsReq = new getTransactionDetailsReq();
                //create tracking id
                var trackingId = Guid.NewGuid().ToString();

                //create request payload
                var getTransactionDetailsReqPayload = new getTransactionDetailsReqPayload();
                getTransactionDetailsReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getTransactionDetailsReqPayload.payloadVersion = "1.0";
                getTransactionDetailsReqPayload.getTransactionDetailsReq = new getTransactionDetails1()
                {
                    accountNumber = accountNumber,
                    accountCurrencyCode = accountCurrencyCode,
                    transactionFromDate = transactionFromDate,
                    transactionToDate = transactionToDate,
                    displayPage = displayPage,
                    startingItemNo = startingItemNo,
                    limitSearch = limitSearch

                };

                //assign header and payload to the request
                getTransactionDetailsReq.header = get_header(trackingId, "getTransactionDetails");
                getTransactionDetailsReq.getTransactionDetailsReqPayload = getTransactionDetailsReqPayload;

                //execute request
                var response = sc_client.getTransactionDetailsAsync(getTransactionDetailsReq).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.AccountController.getTransactionDetails().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);



                //create response
                return Json(new
                {
                    res = "ok",
                    data = response.getTransactionDetailsResponse.getTransactionDetailsResPayload
                });

            }
            catch (Exception ex)
            {

                //log the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.AccountController.getTransactionDetails", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void modifyaccountStatement() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAllInOneDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getOverdraftLimit() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void modifyAccountDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void closeAccount() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getLinkedmortgageDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAccountNomineeDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAccountLinkDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getSettlementAccountDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAccountRiskDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getCommunicationAddressDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAccountInterestDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void modifyLien() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getAccountCardDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void createAccount() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void notifyOverdraftLimitChange() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void getPassbookTransactionDetails() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void setupOverdraftLimit() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void createLien() { }
        [ApiExplorerSettings(IgnoreApi = true)]
        public void setupsweepAccount() { }










    }
}
