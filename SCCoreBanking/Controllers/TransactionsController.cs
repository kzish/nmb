﻿using DeviceDetectorNET;
using DeviceDetectorNET.Parser;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Newtonsoft.Json;
using StanChart;
using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Transactions.Database;
using Transactions.Models;



/// <summary>
/// handle all tranactions with the SC core banking system
/// all requests to this controller are authorized
/// </summary>
namespace SCCoreBanking.Controllers
{

    [Route("Transactions")]
    public class TransactionsController : Controller
    {

        const string captureSystem = "SCRubiemInnovation";
        const string countryCode = "ZW";


        private IHttpContextAccessor _accessor;

        public TransactionsController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            DeviceDetector.SetVersionTruncation(VersionTruncation.VERSION_TRUNCATION_NONE);
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the header for the request, each request must have a header
        /// </summary>
        /// <param name="trackingId">unique guid</param>
        /// <param name="eventType">action performed eg. postTransaction/getTransActionStatus/getTransActionPostingDetails/reserveTransaction/postChequeTransaction</param>
        /// <returns></returns>
        private SCBMLHeaderType get_header(string trackingId, string eventType)
        {
            //
            #region messageSenderType
            //
            messageSenderType messageSenderType = new messageSenderType();
            messageSenderType.messageSender = null;
            messageSenderType.senderDomain = null;
            messageSenderType.countryCode = countryCode;
            //
            #endregion messageSenderType

            #region originationDetailsType
            //
            originationDetailsType originationDetailsType = new originationDetailsType();
            originationDetailsType.messageSender = messageSenderType;
            originationDetailsType.messageTimestamp = DateTime.Now;
            originationDetailsType.initiatedTimestamp = DateTime.Now;
            originationDetailsType.trackingId = trackingId;
            originationDetailsType.correlationID = null;
            originationDetailsType.conversationID = null;
            originationDetailsType.customSearchID = null;
            originationDetailsType.batchID = null;
            originationDetailsType.serviceBusID = null;
            originationDetailsType.validFrom = DateTime.Now;
            originationDetailsType.validTo = DateTime.Now.AddDays(1);
            originationDetailsType.timeToLive = DateTime.Now.AddDays(1);
            originationDetailsType.priority = PriorityTypeEnum.Item1;
            originationDetailsType.checksum = null;
            originationDetailsType.compressionAlgorithm = null;
            originationDetailsType.encoding = null;
            originationDetailsType.possibleDuplicate = true;
            //
            #endregion originationDetailsType

            #region messageDetailsType
            messageDetailsType messageDetailsType = new messageDetailsType();
            messageDetailsType.messageVersion = decimal.Parse("6.0");//todo: check version
            messageDetailsType.messageType = new messageType
            {
                typeName = "",
                subType = new subType()
                {
                    subTypeName = "",
                    subTypeScheme = ""
                }
            };
            //
            #endregion messageDetailsType

            #region SCBMLHeaderType
            SCBMLHeaderType SCBMLHeaderType = new SCBMLHeaderType();
            SCBMLHeaderType.messageDetails = messageDetailsType;
            SCBMLHeaderType.originationDetails = originationDetailsType;
            SCBMLHeaderType.captureSystem = captureSystem;
            SCBMLHeaderType.process = new ProcessType()
            {
                processName = "",
                eventType = "",
                lifecycleState = "",
                workflowState = "",
                action = ""
            };
            SCBMLHeaderType.metadata = null;
            SCBMLHeaderType.messageHistory = null;
            SCBMLHeaderType.exceptions = null;
            //
            #endregion

            //create a header for the request
            #region header
            SCBMLHeaderType header = SCBMLHeaderType;
            header.messageDetails = messageDetailsType;
            header.originationDetails = originationDetailsType;
            header.captureSystem = captureSystem;
            header.process = new ProcessType
            {
                processName = "",
                eventType = eventType,
                lifecycleState = "",
                workflowState = "",
                action = ""
            };
            header.metadata = null;
            header.messageHistory = null;
            header.exceptions = null;
            #endregion header

            return header;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the scb client for the request
        /// </summary>
        /// <returns></returns>
        public Transaction_PortTypeClient get_client()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            //configure end point
            var endPoint = new EndpointAddress("https://gateway-hk.npedmi.standardchartered.com:5054/ws/scbCoreBankingTransaction.v6.ws.provider.Transaction/scbCoreBankingTransaction_v6_ws_provider_Transaction_Port");
            //declare client
            Transaction_PortTypeClient sc_client = new Transaction_PortTypeClient(myBinding, endPoint);
            sc_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("zimra2016", "Standard12", "zone2.scb.net");
            return sc_client;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// uploads data to the given url, used to communicate with the other microservices, returns string response
        /// </summary>
        /// <param name="uri">the url to which data must be posted</param>
        /// <param name="obj">the object to be posted</param>
        private void upload_data(string uri, object obj)
        {
            Task.Run(() =>
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.Headers["Content-Type"] = "application/json";
                    var post_object = JsonConvert.SerializeObject(obj);
                    string res = wc.UploadString(uri, post_object);
                }
                catch (Exception ex)
                {
                    globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsController.upload_data", ex);
                }
            });
        }

        /// <summary>
        /// post a tranaction to Stanchart2 core banking system
        /// </summary>
        /// <param name="mTransaction">mTransaction mTransaction param</param>
        /// <returns></returns>
        [HttpPost("postTransaction")]
        public JsonResult postTransaction([FromBody]mTransAction mTransaction)
        {

            if (string.IsNullOrEmpty(mTransaction.fromAccountNumber))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromAccountNumber param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toAccountNumber))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toAccountNumber param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.fromCurrencyCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromCurrencyCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toCurrencyCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toCurrencyCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.fromBankCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromBankCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toBankCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toBankCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.transactionCurrency))
            {
                return Json(new
                {
                    res = "err",
                    msg = "transactionCurrency param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.transactionAmount))
            {
                return Json(new
                {
                    res = "err",
                    msg = "transactionAmount param is missing"
                });
            }

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.postTransaction()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);

            try
            {

                //get the header
                var header = get_header(mTransaction.trackingId, "postTransaction");

                //transaction

                transaction postTransactionReq = new transaction();
                postTransactionReq.transactionInfo = mTransaction.get_transaction_info();


                //construct the payload
                postTransactionReqPayload postTransactionReqPayload = new postTransactionReqPayload();
                postTransactionReqPayload.payloadFormat = PayloadFormatEnum.XML;
                postTransactionReqPayload.payloadVersion = "1.0";
                postTransactionReqPayload.postTransactionReq = postTransactionReq;

                //construct the request
                postTransactionReq postTransactionRequest = new postTransactionReq();
                postTransactionRequest.header = header;

                postTransactionRequest.postTransactionReqPayload = postTransactionReqPayload;

                var sc_client = get_client();
                //fetch the transaction status and wait for result
                var response = sc_client.postTransactionAsync(postTransactionRequest).Result;



                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.postTransaction().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);

                return Json(new
                {
                    res = "ok",
                    msg = "postTransaction() = success",
                    transaction_response = Json(response.postTransactionResponse.postTransactionResPayload)
                });

            }
            catch (Exception ex)
            {
                //log  the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsControllerglobals.postTransaction", ex);

                //generate response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message,
                });

            }




        }

        /// <summary>
        /// check the transaction status
        /// </summary>
        /// <param name="trackingId">string trackingId</param>
        /// <returns>json string mTranaction type</returns>
        [HttpGet("getTransActionStatus")]
        public JsonResult getTransActionStatus(string trackingId)
        {
            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.getTransActionStatus()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);

            //fetch the transaction from the db
            var db = new db();
            var mTransaction = (from trans in db.mTransAction where trans.trackingId == trackingId select trans).FirstOrDefault();

            if (mTransaction == null)
            {
                //generate response
                return Json(new
                {
                    res = "err",
                    msg = "Invalid tracking id."
                });
            }


            try
            {

                //get the header
                var header = get_header(mTransaction.trackingId, "getTransActionStatus");

                //transaction
                transaction getTransactionStatusReq = new transaction();
                getTransactionStatusReq.transactionInfo = mTransaction.get_transaction_info();


                //construct the payload
                getTransactionStatusReqPayload getTransactionStatusReqPayload = new getTransactionStatusReqPayload();
                getTransactionStatusReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getTransactionStatusReqPayload.payloadVersion = "1.0";
                getTransactionStatusReqPayload.getTransactionStatusReq = getTransactionStatusReq;

                //construct the request
                getTransactionStatusReq getTransactionStatusRequest = new getTransactionStatusReq();
                getTransactionStatusRequest.header = header;
                getTransactionStatusRequest.getTransactionStatusReqPayload = getTransactionStatusReqPayload;


                var sc_client = get_client();
                //fetch the transaction status and wait for result
                var response = sc_client.getTransactionStatusAsync(getTransactionStatusRequest).Result;


                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.getTransActionStatus().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);


                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "getTransActionStatus() = success",
                    transaction_response = Json(response.getTransactionStatusResponse.getTransactionStatusResPayload)
                });

            }
            catch (Exception ex)
            {
                //log  the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsController.getTransActionStatus", ex);

                //generate response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// get the transaction posting details
        /// </summary>
        /// <param name="trackingId">string trackingId</param>
        /// <returns>json string mTransaction type</returns>
        [HttpGet("getTransActionPostingDetails")]
        public JsonResult getTransActionPostingDetails(string trackingId)
        {

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.getTransActionPostingDetails()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);


            //fetch the transaction from the db
            var db = new db();
            var mTransaction = (from trans in db.mTransAction where trans.trackingId == trackingId select trans).FirstOrDefault();

            if (mTransaction == null)
            {
                //generate response
                return Json(new
                {
                    res = "err",
                    msg = "Invalid Tracking id."
                });
            }

            try
            {


                //get the header
                var header = get_header(mTransaction.trackingId, "getTransActionPostingDetails");

                //tranction
                transaction getTransactionPostingDetailsReq = new transaction();
                getTransactionPostingDetailsReq.transactionInfo = mTransaction.get_transaction_info();

                //construct the payload
                getTransactionPostingDetailsReqPayload getTransactionPostingDetailsReqPayload = new getTransactionPostingDetailsReqPayload();
                getTransactionPostingDetailsReqPayload.payloadFormat = PayloadFormatEnum.XML;
                getTransactionPostingDetailsReqPayload.payloadVersion = "1.0";
                getTransactionPostingDetailsReqPayload.getTransactionPostingDetailsReq = getTransactionPostingDetailsReq;

                //construct the request
                getTransactionPostingDetailsReq getTransactionPostingDetailsRequest = new getTransactionPostingDetailsReq();
                getTransactionPostingDetailsRequest.header = header;
                getTransactionPostingDetailsRequest.getTransactionPostingDetailsReqPayload = getTransactionPostingDetailsReqPayload;

                var sc_client = get_client();
                //fetch the transaction status and wait for result
                var response = sc_client.getTransactionPostingDetailsAsync(getTransactionPostingDetailsRequest).Result;

                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.getTransActionPostingDetails().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "getTransActionPostingDetails() = success",
                    transaction_response = Json(response.getTransactionPostingDetailsResponse.getTransactionPostingDetailsResPayload)
                });
            }
            catch (Exception ex)
            {
                //log  the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsController.getTransActionPostingDetails", ex);

                //generate response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }

        }


        /// <summary>
        /// reverse the transaction
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        [HttpPost("reverseTransaction")]
        public JsonResult reverseTransaction(string trackingId)
        {

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.reverseTransaction()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);

            //fetch the transaction from the db
            var db = new db();
            var mTransaction = (from trans in db.mTransAction where trans.trackingId == trackingId select trans).FirstOrDefault();

            if (mTransaction == null)
            {
                //generate response
                return Json(new
                {
                    res = "err",
                    msg = "Invalid tracking id."
                });
            }


            try
            {

                //get the header
                var header = get_header(mTransaction.trackingId, "reverseTransaction");

                //transaction
                transaction reverseTransactionReq = new transaction();
                reverseTransactionReq.transactionInfo = mTransaction.get_transaction_info();


                //construct the payload
                reverseTransactionReqPayload reverseTransactionReqPayload = new reverseTransactionReqPayload();
                reverseTransactionReqPayload.payloadFormat = PayloadFormatEnum.XML;
                reverseTransactionReqPayload.payloadVersion = "1.0";
                reverseTransactionReqPayload.reverseTransactionReq = reverseTransactionReq;

                //construct the request
                reverseTransactionReq reverseTransactionRequest = new reverseTransactionReq();
                reverseTransactionRequest.header = header;
                reverseTransactionRequest.reverseTransactionReqPayload = reverseTransactionReqPayload;

                var sc_client = get_client();
                //fetch the transaction status and wait for result
                var response = sc_client.reverseTransactionAsync(reverseTransactionRequest).Result;

                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.reverseTransaction().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);

                //log the actual transaction
                upload_data(globals.transaction_logger_url, mTransaction);

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "reverseTransaction() = success",
                    transaction_response = Json(response.reverseTransactionResponse.reverseTransactionResPayload)
                });

            }
            catch (Exception ex)
            {
                //log  the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsController.reverseTransaction", ex);

                //generate response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// post check transaction
        /// </summary>
        /// <param name="mTransaction">mTransAction mTransaction type</param>
        /// <returns>json string mTransAction type</returns>
        [HttpPost("postChequeTransaction")]
        public JsonResult postChequeTransaction(mTransAction mTransaction)
        {

            if (string.IsNullOrEmpty(mTransaction.fromAccountNumber))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromAccountNumber param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toAccountNumber))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toAccountNumber param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.fromCurrencyCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromCurrencyCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toCurrencyCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toCurrencyCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.fromBankCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "fromBankCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.toBankCode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "toBankCode param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.transactionCurrency))
            {
                return Json(new
                {
                    res = "err",
                    msg = "transactionCurrency param is missing"
                });
            }

            if (string.IsNullOrEmpty(mTransaction.transactionAmount))
            {
                return Json(new
                {
                    res = "err",
                    msg = "transactionAmount param is missing"
                });
            }

            //log the activity
            mActivityLog activity = new mActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.postChequeTransaction()";
            activity.username = user_agent;
            activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            upload_data(globals.activity_logger_url, activity);

            try
            {

                //header
                var header = get_header(mTransaction.trackingId, "postChequeTransaction");

                //transaction object
                transaction postChequeTransacitonReq = new transaction();
                postChequeTransacitonReq.transactionInfo = mTransaction.get_transaction_info();

                //construct the payload
                postChequeTransactionReqPayload postChequeTransactionReqPayload = new postChequeTransactionReqPayload();
                postChequeTransactionReqPayload.payloadFormat = PayloadFormatEnum.XML;
                postChequeTransactionReqPayload.payloadVersion = "1.0";
                postChequeTransactionReqPayload.postChequeTransacitonReq = postChequeTransacitonReq;

                //construct the request
                postChequeTransactionReq postChequeTransactionRequest = new postChequeTransactionReq();
                postChequeTransactionRequest.header = header;
                postChequeTransactionRequest.postChequeTransactionReqPayload = postChequeTransactionReqPayload;


                var sc_client = get_client();
                //fetch the transaction status and wait for result
                var response = sc_client.postChequeTransactionAsync(postChequeTransactionRequest).Result;



                //log the response
                activity = new mActivityLog();
                user_agent = Request.Headers["User-Agent"].ToString();
                activity.user_activity = "SCCoreBanking.Controllers.TransactionsController.postChequetransaction().Response";
                activity.username = user_agent;
                activity.user_activity = JsonConvert.SerializeObject(response);
                activity.user_ip_address = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                upload_data(globals.activity_logger_url, activity);

                //log the actual transaction
                upload_data(globals.transaction_logger_url, mTransaction);

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "postChequeTransaction() = success",
                    transaction_response = Json(response.postChequeTransactionResponse.postChequeTransactionResPayload)
                });


            }
            catch (Exception ex)
            {
                //log  the error
                var error_log = new mErrorLog();
                error_log.activity_id = activity._id;
                error_log.error_message = ex.Message;
                error_log.full_error_message = ex.ToString();
                upload_data(globals.error_logger_url, error_log);

                globals.log_data_to_file("SCCoreBanking.Controllers.TransactionsControllerglobals.postChequetransaction", ex);

                //generate response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });


            }
        }


    }
}
