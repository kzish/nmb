﻿using System;
using System.Xml.Serialization;

namespace Transactions.Models
{
    /// <summary>
    /// class to model the transaction
    /// </summary>
    /// 
    public class mTransAction
    {

        public string _id { get; set; } = Guid.NewGuid().ToString();//id
        public DateTime date { get; set; } = DateTime.Now;//transaction's date
        public string fromAccountNumber { get; set; }
        public string toAccountNumber { get; set; }
        public string fromCurrencyCode { get; set; }
        public string toCurrencyCode { get; set; }
        public string messageSubType { get; set; }
        public string transactionCurrency { get; set; }
        public string transactionAmount { get; set; }
        public string fromBankCode { get; set; }
        public string toBankCode { get; set; }
        public string costRate { get; set; }
        public string conversionRate { get; set; }
        public string uniqueReferenceIdentifier { get; set; }
        public string transactionBranch { get; set; }
        public string transactionPostingDate { get; set; }
        public string sourceSystemName { get; set; }
        public string transactionType { get; set; }
        public string lcyEquivalent { get; set; }
        public string countrycode { get; set; }
        public string trackingId { get; set; } = Guid.NewGuid().ToString();//tracking id 
        public string billerCode { get; set; }
        public ApiCallType ApiCallType { get; set; }//wat type of transaction, ie getTransActionStatus.postTransaction.getTransActionPostingDetails.reserveTransaction.postChequeTransaction 


        public StanChart.transactionInfo get_transaction_info()
        {
            var transactionInfo = new StanChart.transactionInfo();
            transactionInfo.fromAccountNumber = this.fromAccountNumber;
            transactionInfo.toAccountNumber = this.toAccountNumber;
            transactionInfo.fromCurrencyCode = this.fromCurrencyCode;
            transactionInfo.toCurrencyCode = this.toCurrencyCode;
            transactionInfo.messageSubType = this.messageSubType;
            transactionInfo.transactionCurrency = this.transactionCurrency;
            transactionInfo.transactionAmount = this.transactionAmount;
            transactionInfo.fromBankCode = this.fromBankCode;
            transactionInfo.toBankCode = this.toBankCode;
            transactionInfo.costRate = this.costRate;
            transactionInfo.conversionRate = this.conversionRate;
            transactionInfo.uniqueReferenceIdentifier = this.uniqueReferenceIdentifier;
            transactionInfo.transactionBranch = this.transactionBranch;
            transactionInfo.transactionPostingDate = this.transactionPostingDate;
            transactionInfo.sourceSystemName = this.sourceSystemName;
            transactionInfo.transactionType = this.transactionType;
            transactionInfo.lcyEquivalent = this.lcyEquivalent;
            transactionInfo.countrycode = this.countrycode;
            return transactionInfo;

        }
    }

    public enum ApiCallType
    {
        postTransaction,
        getTransActionStatus,
        getTransActionPostingDetails,
        reserveTransaction,
        postChequeTransaction
    }

   

}
