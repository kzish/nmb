﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using LinqToDB.Configuration;

namespace Transactions.Database
{
    public class mysql_db
    {

        private static string db_server = "localhost";
        private static string db_name = "stanchart_simba_logs";
        private static string db_user_name = "root";
        private static string db_user_password = "";
        public static string configuration = "MySqlserver";


        public class ConnectionStringSettings : IConnectionStringSettings
        {
            public string ConnectionString { get; set; }
            public string Name { get; set; }
            public string ProviderName { get; set; }
            public bool IsGlobal => false;
        }




        public class MySettings : ILinqToDBSettings
        {
            public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();

            public string DefaultConfiguration => configuration;
            public string DefaultDataProvider => configuration;

            public IEnumerable<IConnectionStringSettings> ConnectionStrings
            {
                get
                {
                    yield return
                        new ConnectionStringSettings
                        {
                            Name = configuration,
                            ProviderName = configuration,
                            ConnectionString = @"Server="+ db_server + ";Database=" + db_name + ";Uid=" + db_user_name + ";Pwd=" + db_user_password + ";"
                        };
                }
            }
        }

    }
}
