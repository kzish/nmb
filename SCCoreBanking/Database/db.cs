﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transactions.Models;

namespace Transactions.Database
{
    public class db : LinqToDB.Data.DataConnection
    {
        public db() : base(Database.sql_server_db.configuration) { }

        public ITable<mActivityLog> mActivityLog => GetTable<mActivityLog>();
        public ITable<mErrorLog> mErrorLog => GetTable<mErrorLog>();
        public ITable<mTransAction> mTransAction => GetTable<mTransAction>();
    }



}
