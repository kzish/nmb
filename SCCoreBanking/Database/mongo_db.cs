﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace Transactions.Database
{
    public class mongo_db
    {

        /**** mongo db settings ******/
        public static MongoClient mongo_client;
        public static IMongoDatabase mongo_database;
        public static string mongo_connection_string = "mongodb://localhost";
        public static string mongo_database_name = "stanchart_simba_logs";



        //get the mongo db instance
        public static IMongoDatabase get_db()
        {
            if (mongo_client == null) mongo_client = new MongoClient(mongo_connection_string);
            if (mongo_database == null) mongo_database = mongo_client.GetDatabase(mongo_database_name);
            return mongo_database;

        }


    }
}
