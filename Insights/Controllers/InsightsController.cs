﻿using Insights.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

/// <summary>
/// retrieves insights for the mobile app AI engine
/// </summary>
namespace Insights.Controllers
{
    [Route("Insights")]
    public class InsightsController : Controller
    {
        private dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        /// <summary>
        /// get all the transactions which have not been posted to the city council
        /// </summary>
        /// <param name="billercode"></param>
        /// <returns></returns>
        [HttpGet("GetNewTransactions")]
        public JsonResult GetNewTransactions(string billercode)
        {
            try
            {
                //fetch the transactions
                var transactions = db.MTransAction.Where(i => i.BillerCode == billercode && !i.PostedToBiller).ToList();
                //update the transactions
                foreach (var t in transactions)
                {
                    t.PostedToBiller = true;
                }
                db.SaveChanges();
                return Json(new
                {
                    res = "ok",
                    msg = transactions
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }







    }
}
