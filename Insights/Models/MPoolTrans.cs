﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MPoolTrans
    {
        public int Id { get; set; }
        public string Pooltransnumber { get; set; }
        public string Poolname { get; set; }
        public DateTime? PoolTransDate { get; set; }
        public string Trackingid { get; set; }
        public decimal? PoolTransAmount { get; set; }
        public string PoolTransCustomer { get; set; }
        public int? ApprovalRef { get; set; }
    }
}
