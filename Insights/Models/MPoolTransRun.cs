﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MPoolTransRun
    {
        public int Id { get; set; }
        public string Pooltransnumber { get; set; }
        public string Poolname { get; set; }
        public DateTime? PoolTransDate { get; set; }
        public decimal? PoolTransAmount { get; set; }
        public DateTime? NextPoolRunDate { get; set; }
        public int? BenefitedPosition { get; set; }
        public int? NextBenefitingPosition { get; set; }
        public string PoolTransCustomer { get; set; }
    }
}
