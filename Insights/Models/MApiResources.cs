﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MApiResources
    {
        public int Id { get; set; }
        public string ResourceName { get; set; }
    }
}
