﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MReferenceNumber
    {
        public int Reference { get; set; }
    }
}
