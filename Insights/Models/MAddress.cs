﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MAddress
    {
        public int Id { get; set; }
        public string BillerCode { get; set; }
        public string AptNum { get; set; }
        public string Street { get; set; }
        public string Extention { get; set; }
        public string City { get; set; }
    }
}
