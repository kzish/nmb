﻿using System;
using System.Collections.Generic;

namespace Insights.Models
{
    public partial class MBillerProduct
    {
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Category { get; set; }
        public string BillerCode { get; set; }
        public bool? Available { get; set; }
    }
}
