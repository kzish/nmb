﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auth.Models
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource("api","api")
            };
        }

        public static IEnumerable<Client> GetApiClients()
        {
            return new List<Client>
            {
                new Client
                {
                        ClientId="client",
                        AllowedGrantTypes=GrantTypes.ClientCredentials,
                        ClientSecrets={
                                new Secret("secret".Sha256())
                        },
                        AllowedScopes={"api" },
                }
            };
        }


    }
}
