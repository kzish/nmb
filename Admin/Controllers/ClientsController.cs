﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Admin.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Admin.Controllers
{
    [Route("Clients")]
    [Authorize(Roles = "admin")]
    public class ClientsController : Controller
    {
        dbContext db = new dbContext();
        private string source = "Admin.Controllers.ClientsController.";
        UserManager<IdentityUser> userManager;
        RoleManager<IdentityRole> roleManager;
        public ClientsController(UserManager<IdentityUser>userManager, RoleManager<IdentityRole> roleManager) {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [HttpGet("DeleteClient/{id}")]
        public async Task<IActionResult> DeleteClient(string id)
        {
            ViewBag.title = "Delete Client";
            //delete user which also deletes client
            try
            {
                var client_user = await userManager.FindByIdAsync(id);
                await userManager.DeleteAsync(client_user);
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("Clients", "Home");
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeleteClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction("Clients", "Home");
            }
        }


        [HttpGet("CreateClient")]
        public IActionResult CreateClient()
        {
            ViewBag.title = "Create Client";
            return View();
        }

        [HttpPost("CreateClient")]
        public async Task<IActionResult> CreateClient(string Email, string Password, string ClientId, string Secret)
        {
            ViewBag.title = "Create Client";
            try
            {
                //create a user and create  client, then link the client to the user
                //assign client to client roles
                var id_user = new IdentityUser()
                {
                    Email = Email,
                    PasswordHash = Password,
                    UserName = Email
                };
                var created = await userManager.CreateAsync(id_user, Password);
                if(created.Succeeded)
                {
                    var newApiClient = new MApiClients();
                    newApiClient.Id = id_user.Id;//link to the user
                    newApiClient.ClientId = ClientId;
                    newApiClient.Secret = Secret;
                    db.MApiClients.Add(newApiClient);
                    //create role if not exists
                    var client_role = await roleManager.RoleExistsAsync("api_client");
                    if (!client_role)
                    {
                        await roleManager.CreateAsync(new IdentityRole("api_client"));
                    }
                    //add client to role
                    var is_in_role = await userManager.IsInRoleAsync(id_user,"api_client");
                    if (!is_in_role)
                    {
                        await userManager.AddToRoleAsync(id_user, "api_client");
                    }
                    TempData["type"] = "success";
                    TempData["msg"] = "Saved";
                }
                else
                {
                    TempData["type"] = "error";
                    var errors = string.Empty;
                    foreach(var e in created.Errors.ToList())
                    {
                        errors += e.Description + " ";
                    }
                    TempData["msg"] =errors;
                }
                
                db.SaveChanges();
                
                return RedirectToAction("Clients", "Home");
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "CreateClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction($"CreateClient", "Clients");
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}
