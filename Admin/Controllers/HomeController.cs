﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin.Controllers
{
    [Route("Home")]
    [Route("")]
    [Authorize(Roles ="admin")]
    public class HomeController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        RoleManager<IdentityRole> roleManager;
        public HomeController(RoleManager<IdentityRole> roleManager)
        {
            this.roleManager = roleManager;
        }



        [HttpGet("Dashboard")]
        [Route("")]

        public IActionResult Dashboard()
        {
            ViewBag.title = "Dashboard";
            return View();
        }

        [HttpGet("Clients")]
        public IActionResult Clients()
        {
            ViewBag.title = "Clients";

            //fetch the api users
            var clients = db.AspNetUsers
                .Where(i => i.AspNetUserRoles.Any(r => r.Role.Name == "api_client"))
                 .Include(c => c.MApiClients)
                 .ToList();
            ViewBag.clients = clients;
            return View();
        }

        [HttpGet("Resources")]
        public IActionResult Resources()
        {
            ViewBag.title = "Resources";
            var resources = db.MApiResources.ToList();
            ViewBag.resources = resources;
            return View();
        }


        [HttpGet("Billers")]
        public IActionResult Billers()
        {
            ViewBag.title = "Billers";
            var billers = db.AspNetUsers
                .Where(i => i.AspNetUserRoles.Any(r => r.Role.Name == "biller"))
                .Include(i=>i.MBillerInfo)
                .OrderBy(i=>i.MBillerInfo.BillerName)
                .ToList();
            ViewBag.billers = billers;
            return View();
        }


        [HttpGet("AllRoles")]
        public IActionResult AllRoles()
        {
            ViewBag.title = "All Roles";
            var roles = roleManager.Roles.ToList();
            ViewBag.roles = roles;
            return View();
        }


    }
}
