﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Admin.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

/// <summary>
/// handles the billers
/// </summary>
namespace Admin.Controllers
{
    [Route("Billers")]
    [Authorize(Roles = "admin")]
    public class BillersController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        UserManager<IdentityUser> userManager;
        RoleManager<IdentityRole> roleManager;
        public BillersController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }


        [HttpGet("EditBiller/{id}")]
        public IActionResult EditBiller(string id)
        {
            ViewBag.title = "EditBiller";
            var biller = db.AspNetUsers.Where(i => i.Id == id).Include(i => i.MBillerInfo).FirstOrDefault();
            ViewBag.biller = biller ?? new AspNetUsers();
            ViewBag.categories = db.EnumBillerCategory.ToList();
            ViewBag.types = db.EnumBillerType.ToList();
            return View();
        }



        [HttpGet("DeleteBiller/{id}")]
        public async Task<IActionResult> DeleteBiller(string id)
        {
            ViewBag.title = "Delete Biller";
            try
            {
                db.RemoveRange(db.MBankAccount.Where(i => i.BillerId == id).ToList());
                var biller = db.MBillerInfo.Find(id);
                if (biller != null)
                {
                    db.Remove(biller);
                }
                db.SaveChanges();
                var id_user=await userManager.FindByIdAsync(id);
                await userManager.DeleteAsync(id_user);
                TempData["type"] = "Removed biller";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("Billers", "Home");
        }


        [HttpPost("EditBiller")]
        public IActionResult EditBiller(string Id, MBillerInfo billerinfo)
        {
            ViewBag.title = "Edit Biller";
            try
            {
                db.Entry(billerinfo).State = EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("Billers", "Home");
        }


        [HttpGet("CreateBiller")]
        public IActionResult CreateBiller()
        {
            ViewBag.title = "Create Biller";
            ViewBag.categories = db.EnumBillerCategory.ToList();
            ViewBag.types = db.EnumBillerType.ToList();
            return View();
        }

        [HttpPost("CreateBiller")]
        public async Task<IActionResult> CreateBiller(string Email, string Password, MBillerInfo billerinfo)
        {
            ViewBag.title = "Create Biller";
            //create a user then assign user to biller info
            var id_user = new IdentityUser()
            {
                Email = Email,
                UserName = Email,
                PasswordHash = Password
            };
            var create_user = await userManager.CreateAsync(id_user,Password);
            if (create_user.Succeeded)
            {
                var role_exist = await roleManager.RoleExistsAsync("biller");
                if (!role_exist)
                {
                    await roleManager.CreateAsync(new IdentityRole("biller"));
                }
                await userManager.AddToRoleAsync(id_user, "biller");
                billerinfo.Id = id_user.Id;//link to the user
                db.MBillerInfo.Add(billerinfo);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Created Biller";
            }
            else
            {
                var errors = String.Empty;
                TempData["type"] = "error";
                foreach (var e in create_user.Errors)
                {
                    errors += e.Description + ", ";
                }
                TempData["msg"] = errors;
            }
            return RedirectToAction("Billers","Home");
        }


        [HttpGet("BillerAccounts/{id}")]
        public IActionResult BillerAccounts(string id)
        {
            ViewBag.title = "Biller Accounts";
            var biller = db.AspNetUsers.Where(i => i.Id == id)
                .Include(i => i.MBankAccount)
                .Include(i=>i.MBillerInfo)
                .FirstOrDefault();

            ViewBag.biller = biller;
            ViewBag.banks = db.EnumBanks.ToList();
            ViewBag.currency = db.EnumCurrencyCode.ToList();

            return View();
        }

        [HttpPost("CreateBillerAccount")]
        public IActionResult CreateBillerAccount(string Id, MBankAccount account)
        {
            ViewBag.title = "Create Biller";
            try
            {
                account.BillerId = Id;
                db.MBankAccount.Add(account);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"BillerAccounts", "Billers", new { Id });
        }

        [HttpGet("DeleteAccount/{id}")]
        public IActionResult DeleteAccount(int id)
        {
            ViewBag.title = "Delete Account";
            var Id = string.Empty;
            try
            {
                var account = db.MBankAccount.Find(id);
                Id = account.BillerId;
                db.Remove(account);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"BillerAccounts", "Billers", new { Id });
        }
         
        [HttpGet("EnableBiller/{id}")]
        public IActionResult EnableBiller(string id)
        {
            ViewBag.title = "Enable Biller";
            var Id = string.Empty;
            try
            {
                var biller = db.AspNetUsers.Where(i => i.Id == id).Include(i => i.MBillerInfo).FirstOrDefault();
                Id = biller.Id;
                biller.MBillerInfo.BillerStatus = true;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"Billers", "Home");
        }

        [HttpGet("DisableBiller/{id}")]
        public IActionResult DisableBiller(string id)
        {
            ViewBag.title = "Disable Biller";
            var Id = string.Empty;
            try
            {
                var biller = db.AspNetUsers.Where(i => i.Id == id).Include(i => i.MBillerInfo).FirstOrDefault();
                Id = biller.Id;
                biller.MBillerInfo.BillerStatus = false;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"Billers", "Home");
        }

        //make the selected account the default account
        [HttpGet("SetDefaultAccount/{id}")]
        public IActionResult SetDefaultAccount(int id)
        {
            ViewBag.title = "Set Default Account";
            var biller_id = string.Empty;
            try
            {
                var account = db.MBankAccount.Find(id);
                biller_id = account.BillerId;
                var accounts = db.MBankAccount.Where(i => i.BillerId == biller_id).ToList();
                //disable all other accounts to leave this one active
                foreach (var a in accounts)
                {
                    a.Active = false;
                }
                account.Active = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction($"BillerAccounts", "Billers", new { Id = biller_id });
        }




    }
}
