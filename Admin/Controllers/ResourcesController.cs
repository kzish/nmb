﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Admin.Models;
using Microsoft.AspNetCore.Authorization;

namespace Admin.Controllers
{
    [Route("Resources")]
    [Authorize(Roles = "admin")]
    public class ResourcesController : Controller
    {
        dbContext db = new dbContext();
        private string source = "Admin.Controllers.ResourcesController.";


        [HttpGet("DeleteResource/{id}")]
        public IActionResult DeleteResource(int id) {
            ViewBag.title = "Delete Resource";
            try
            {
                var resource = db.MApiResources.Find(id);
                db.Remove(resource);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("Resources","Home");
            }
            catch (Exception ex) {
                Globals.log_data_to_file(source + "DeleteClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction("Resources", "Home");
            }
        }

     
        [HttpGet("CreateResource")]
        public IActionResult CreateResource()
        {
            ViewBag.title = "Create Reource";
            return View();
        }

        [HttpPost("CreateResource")]
        public IActionResult CreateResource(MApiResources resource)
        {
            ViewBag.title = "Create Resource";
            try
            {

                db.MApiResources.Add(resource);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("Resources", "Home");
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "CreateClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction($"CreateResource", "Resources");
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}
