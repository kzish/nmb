﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Helpers
{
    public static string getBillerCategory(int? id)
    {
        if (id == null) return "";
        var db = new dbContext();
        var res = db.EnumBillerCategory.Find(id);
        db.Dispose();
        return res.Category;
    }
    public static string getBillerType(int? id)
    {
        if (id == null) return "";
        var db = new dbContext();
        var res = db.EnumBillerType.Find(id);
        db.Dispose();
        return res.Type;
    }
}
