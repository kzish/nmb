﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class EnumCurreccyCode
    {
        public EnumCurreccyCode()
        {
            MBankAccount = new HashSet<MBankAccount>();
        }

        public int Id { get; set; }
        public string CurrencyCode { get; set; }

        public virtual ICollection<MBankAccount> MBankAccount { get; set; }
    }
}
