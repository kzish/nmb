﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class EnumBanks
    {
        public EnumBanks()
        {
            MBankAccount = new HashSet<MBankAccount>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string BankCode { get; set; }

        public virtual ICollection<MBankAccount> MBankAccount { get; set; }
    }
}
