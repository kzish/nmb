﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class MApiUsageStats
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime Date { get; set; }
        public string TrackingId { get; set; }
    }
}
