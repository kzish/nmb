﻿using System;
using System.Collections.Generic;

namespace Admin.Models
{
    public partial class MClientBillers
    {
        public int Id { get; set; }
        public string BillerId { get; set; }
        public string ClientId { get; set; }
    }
}
