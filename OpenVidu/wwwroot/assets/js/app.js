var OV;
var session;
 

function joinSession() {
    console.log("bout to join");
	var mySessionId = document.getElementById("sessionId").value;
    console.log("1");
	OV = new OpenVidu();
	session = OV.initSession();
    console.log("2");
	session.on("streamCreated", function (event) {
		session.subscribe(event.stream, "subscriber");
	});
    console.log("3");
	getToken(mySessionId).then(token => {
        console.log("7");
        console.log("sessionId:");
        console.log(mySessionId);
        //token = token.replace("localhost", "192.168.100.5");
        console.log("token:");
        console.log(token);
		session.connect(token)
            .then(() => {
                console.log("6");
				document.getElementById("session-header").innerText = mySessionId;
				document.getElementById("join").style.display = "none";
				document.getElementById("session").style.display = "block";

				var publisher = OV.initPublisher("publisher");
                session.publish(publisher);
                console.log("5");
			})
            .catch(error => {
                console.log("4");
                console.log(error.code + " " + error.message);
				console.log("There was an error connecting to the session:", error.code, error.message);
			});
	});

}

function leaveSession() {
	session.disconnect();
	document.getElementById("join").style.display = "block";
	document.getElementById("session").style.display = "none";
}

window.onbeforeunload = function () {
    console.log("session:");
    console.log(session);
    alert("bye");
    if (session) session.disconnect();
};


/**
 * --------------------------
 * SERVER-SIDE RESPONSIBILITY
 * --------------------------
 * These methods retrieve the mandatory user token from OpenVidu Server.
 * This behavior MUST BE IN YOUR SERVER-SIDE IN PRODUCTION (by using
 * the API REST, openvidu-java-client or openvidu-node-client):
 *   1) Initialize a session in OpenVidu Server	(POST /api/sessions)
 *   2) Generate a token in OpenVidu Server		(POST /api/tokens)
 *   3) The token must be consumed in Session.connect() method
 */

var OPENVIDU_SERVER_URL = "https://192.168.5.22:4443";
var OPENVIDU_SERVER_SECRET = "MY_SECRET";

function getToken(mySessionId) {
	return createSession(mySessionId).then(sessionId => createToken(sessionId));
}

function createSession(sessionId) { // See https://openvidu.io/docs/reference-docs/REST-API/#post-apisessions
    return new Promise((resolve, reject) => {
        console.log("OPENVIDU_SERVER_URL: " + OPENVIDU_SERVER_URL);
        console.log("sessionId: " + sessionId);
        $.ajax({
            type: "POST",
            url: OPENVIDU_SERVER_URL + "/api/sessions",
            data: JSON.stringify({ customSessionId: sessionId }),
            headers: {
                "Authorization": "Basic " + btoa("OPENVIDUAPP:" + OPENVIDU_SERVER_SECRET),
                "Content-Type": "application/json"
            },
            success: response => {
                console.log("response:");
                console.log(response);
                resolve(response.id);
            },
            error: (error) => {
                console.log("error:");
                console.log(error);
				if (error.status === 409) {
					resolve(sessionId);
                } else {
                    console.log('No connection to OpenVidu Server. This may be a certificate error at ' + OPENVIDU_SERVER_URL);
					console.warn('No connection to OpenVidu Server. This may be a certificate error at ' + OPENVIDU_SERVER_URL);
					if (window.confirm('No connection to OpenVidu Server. This may be a certificate error at \"' + OPENVIDU_SERVER_URL + '\"\n\nClick OK to navigate and accept it. ' +
                        'If no certificate warning is shown, then check that your OpenVidu Server is up and running at "' + OPENVIDU_SERVER_URL + '"'))
                    {
                        console.log("assign certificate");
                        location.assign(OPENVIDU_SERVER_URL + '/accept-certificate');
                        console.log("accepted certificate");
					}
				}
			}
		});
	});
}

function createToken(sessionId) { // See https://openvidu.io/docs/reference-docs/REST-API/#post-apitokens
	return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: OPENVIDU_SERVER_URL + "/api/tokens",
            data: JSON.stringify({ session: sessionId }),
            headers: {
                "Authorization": "Basic " + btoa("OPENVIDUAPP:" + OPENVIDU_SERVER_SECRET),
                "Content-Type": "application/json"
            },
            success: response => {
                console.log("created token:");
                console.log(response);
                resolve(response.token);
            },
			error: error => reject(error)
		});
	});
}


