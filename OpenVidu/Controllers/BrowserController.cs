﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace OpenVidu.Controllers
{
    /// <summary>
    /// displays the browser to the client
    /// </summary>
    [Route("RubiemVideoChat")]
    public class BrowserController : Controller
    {
        //todo: get the permanant certificate url
        private string accept_certificate_url = $"{Globals.open_vidu_server}/Accept-certificate";


        /// <summary>
        /// Browser, open and display the video session in the browser
        /// </summary>
        /// <returns></returns>
        [HttpGet("Browser")]
        public IActionResult Browser()
        {
            return View();
        }

        /// <summary>
        /// force the client browser to accept the certificate
        /// </summary>
        /// <returns></returns>
        [HttpGet("AcceptCertificate")]
        public IActionResult AcceptCertificate()
        {
            
            Response.Headers["Refresh"] = "5;url=homepage.aspx";
            return RedirectPermanent(accept_certificate_url);
        }

    }
}
