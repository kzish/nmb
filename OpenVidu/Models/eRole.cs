﻿namespace OpenVidu.Models
{
    /// <summary>
    /// the role of the participant
    /// </summary>
    public enum eRole
    {
        PUBLISHER,//default
        SUBSCRIBER,
        MODERATOR
    }


}
