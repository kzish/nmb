﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeloneTomi.Models
{
    //request made by the user
    public class mBillerCartRequest
    {
        [Required]
        public string biller_code { get; set; }
        [Required]
        public string payment_ref { get; set; }
        [Required]
        public string payer_name { get; set; }
        public string payer_mobile { get; set; }
        public string payer_details1 { get; set; }
        public string payer_details2 { get; set; }
        public string payer_details3 { get; set; }
        [Required]
        public string bank_code { get; set; }
        [Required]
        public List<mCartProduct> items { get; set; } = new List<mCartProduct>();

    }

    /// <summary>
    /// what the user is buying
    /// product and its quantity
    /// </summary>
    public class mCartProduct
    {
        [Required]

        public string product_code { get; set; }
        [Required]

        public decimal quantity { get; set; }//2decimal places
        [Required]

        public decimal unit_price { get; set; }//2decimal places
    }


}
