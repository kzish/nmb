﻿using System;
using System.Collections.Generic;

namespace TeloneTomi.Models
{
    public partial class Logs
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Data { get; set; }
        public DateTime? Date { get; set; }
        public int? LogType { get; set; }
    }
}
