﻿using System;
using System.Collections.Generic;

namespace TeloneTomi.Models
{
    public partial class MBillerInfo
    {
        public string Id { get; set; }
        public string BillerName { get; set; }
        public string BillerCode { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public int? EnumBillerType { get; set; }
        public int? EnumBillerCategory { get; set; }
        public int? EnumClientIndentifier { get; set; }
        public bool? HasProducts { get; set; }
        public bool? HasAccounts { get; set; }
        public bool? HasAdditionalData { get; set; }
        public string RequestEndPoint { get; set; }
        public string CredentialsUsername { get; set; }
        public string CredentialsPassword { get; set; }
        public string ConnectionType { get; set; }
        public int? PaymentCycle { get; set; }
        public bool? BillerStatus { get; set; }
        public string Enabler { get; set; }
        public bool? HasCart { get; set; }
        public bool AllowConfirm { get; set; }

        public virtual EnumBillerCategory EnumBillerCategoryNavigation { get; set; }
        public virtual EnumBillerType EnumBillerTypeNavigation { get; set; }
        public virtual AspNetUsers IdNavigation { get; set; }
    }
}
