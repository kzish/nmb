﻿using System;
using System.Collections.Generic;

namespace TeloneTomi.Models
{
    public partial class MNotification
    {
        public int Id { get; set; }
        public string BillerCode { get; set; }
        public string Message { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsRead { get; set; }
    }
}
