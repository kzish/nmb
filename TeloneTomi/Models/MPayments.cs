﻿using System;
using System.Collections.Generic;

namespace TeloneTomi.Models
{
    public partial class MPayments
    {
        public int Id { get; set; }
        public string BillerCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public string TrackingId { get; set; }
        public string BatchNo { get; set; }
    }
}
