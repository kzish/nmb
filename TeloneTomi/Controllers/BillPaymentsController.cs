﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TeloneTomi.Models;


/// <summary>
/// handles the billpayments and land line payments
/// </summary>
namespace TeloneTomi.Controllers
{
    [Route("TelOneBillPayments")]
    public class BillPaymentsController : Controller
    {

        [HttpGet("GetBillerProducts")]
        public JsonResult getBroadbandProducts()
        {
            try
            {

                var biller_products = new List<MBillerProduct>();
                var product = new MBillerProduct();
                //
                product.ProductCode = "bill";
                product.Name = "phone bill";
                product.Category = "phone";
                product.Description = "pay telone phone bill";
                product.Price = (decimal)0.00;
                product.ImageUrl = null;
                product.Available = true;
                biller_products.Add(product);

                return Json(new
                {
                    res = "ok",
                    msg = biller_products

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// handles the payments, and the cart
        /// </summary>
        [HttpPost("PostBillerCart")]
        public async Task<JsonResult> PostBillerCart([FromBody] mBillerCartRequest cartRequest)
        {
            try
            {
                
                dynamic req = new JObject();
                req.BillAmount = cartRequest.items.First().unit_price;
                req.PaymentMethodData =null;
                req.MerchantReference = Guid.NewGuid().ToString();//cartRequest.payment_ref;
                req.Identifier = cartRequest.payer_details1;
                req.PaymentMethod = "Cash";
                req.Currency = "ZWL";
                req.Reason = "Bill Payment";
                req.AccountSid = Globals.AccountSid;
                req.APIKey = Globals.APIKey;


                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var jsonContent = JsonConvert.SerializeObject(req);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                var response = await client.PostAsync($"{Globals.baseUrl}/payBill", content).Result.Content.ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject(response);
                if (json.return_code == "0")
                {
                    return Json(new
                    {
                        res = "ok",
                        msg = $"{json.return_message}",
                        tracking_id = cartRequest.payment_ref,
                        payment_ref = cartRequest.payment_ref
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Payment failed",
                        debug=new JsonResult(response)
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message,
                });
            }
        }


        [HttpPost("CheckAccount")]
        public async Task<IActionResult> CheckAccount([FromBody] mBillerCartRequest cartRequest)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                dynamic json = new JObject();
                json.AccountSid = Globals.AccountSid;
                json.APIKey = Globals.APIKey;
                json.CustomerAccount = cartRequest.payer_details2;

                var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(Globals.baseUrl + "/verifyCustomerAccount", content).Result.Content.ReadAsStringAsync();
                dynamic jResponse = JsonConvert.DeserializeObject(response);
                var account_details = $"AccountNumber:{jResponse.AccountNumber},AccountName:{jResponse.AccountName},AccountAddress:{jResponse.AccountAddress}";
                return Json(new
                {
                    res = "ok",
                    msg = account_details
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

    }
}
