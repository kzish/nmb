﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TeloneTomi.Models;


/// <summary>
/// handles the broadband services/accounts/payments
/// </summary>
namespace TeloneTomi.Controllers
{
    [Route("TeloneBroadband")]
    public class BroadBandController : Controller
    {

        [HttpGet("GetBillerProducts")]
        public async Task<JsonResult> getBroadbandProducts()
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var request = $"{Globals.baseUrl}/GetBroadBandProducts";
                var json = Json(new
                {
                    AccountSid = Globals.AccountSid,
                    APIKey = Globals.APIKey
                }).Value;
                var jsonstring = JsonConvert.SerializeObject(json);
                var content = new StringContent(jsonstring, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(request, content).Result.Content.ReadAsStringAsync();
                dynamic json_response = JsonConvert.DeserializeObject(response);
                //map the response onto the standard mBillerProduct
                var biller_products = new List<MBillerProduct>();
                foreach (var obj in JArray.Parse(json_response.ToString()))
                {
                    var product = new MBillerProduct();
                    //
                    product.ProductCode = obj.ProductId;
                    product.Name = obj.ProductName;
                    product.Category = null;
                    product.Description = obj.ProductDescription;
                    product.Price = obj.ProductPrices[1].Price;
                    product.ImageUrl = null;
                    string available = obj.Activated;
                    available = available.ToLower();
                    product.Available = bool.Parse(available);
                    //
                    biller_products.Add(product);
                }



                return Json(new
                {
                    res = "ok",
                    msg = biller_products

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// handles the payments, and the cart
        /// </summary>
        [HttpPost("PostBillerCart")]
        public async Task<JsonResult> PostBillerCart([FromBody] mBillerCartRequest cartRequest)
        {
            try
            {
                var array = new JArray();
                foreach (var item in cartRequest.items)
                {
                    dynamic jtoken = new JObject();
                    jtoken.ProductId = item.product_code;
                    jtoken.Quantity = Convert.ToInt32(item.quantity);
                    array.Add(jtoken);
                }
                dynamic req = new JObject();
                req.AccountSid = Globals.AccountSid;
                req.APIKey = Globals.APIKey;
                req.MerchantReference = cartRequest.payment_ref;// cartRequest.payer_mobile;
                req.OrderProducts = array;
                req.Currency = "ZWL";

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var jsonContent = JsonConvert.SerializeObject(req);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                var response = await client.PostAsync($"{Globals.baseUrl}/PurchaseBroadband", content).Result.Content.ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject(response);
                if (json.ResponseCode == "00")
                {
                    return Json(new
                    {
                        res = "ok",
                        msg = $"PIN={json.Vouchers[0].PIN}{Environment.NewLine}CardNumber={json.Vouchers[0].CardNumber}",
                        tracking_id = cartRequest.payment_ref,
                        payment_ref = cartRequest.payment_ref
                    });
                }
                else
                {

                    return Json(new
                    {
                        res = "err",
                        msg = json.ResponseDescription
                    });
                }


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message,
                });
            }
        }


        [HttpPost("CheckAccount")]
        public async Task<IActionResult> CheckAccount([FromBody] mBillerCartRequest cartRequest)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                dynamic json = new JObject();
                json.AccountSid = Globals.AccountSid;
                json.APIKey = Globals.APIKey;
                json.CustomerAccount = cartRequest.payer_details2;

                var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(Globals.baseUrl + "/verifyBroadbandAccount", content).Result.Content.ReadAsStringAsync();
                dynamic jResponse = JsonConvert.DeserializeObject(response);
                var account_details = $"AccountNumber:{jResponse.AccountNumber},AccountName:{jResponse.AccountName},AccountAddress:{jResponse.AccountAddress}";
                return Json(new
                {
                    res = "ok",
                    msg = account_details
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

    }
}
