﻿using System;
using System.Collections.Generic;

namespace TelecelOboPay.Models
{
    public partial class MReferenceNumber
    {
        public int Reference { get; set; }
    }
}
