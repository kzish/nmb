﻿using System;
using System.Collections.Generic;

namespace TelecelOboPay.Models
{
    public partial class MBankAccount
    {
        public int Id { get; set; }
        public string BillerId { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public int? EnumBank { get; set; }
        public int? EnumCurrencyCode { get; set; }
        public bool? Active { get; set; }

        public virtual AspNetUsers Biller { get; set; }
    }
}
