﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

/// <summary>
/// telecel obo pay api
/// </summary>
namespace TelecelOboPay.Controllers
{
    [Route("TelecelOboPay")]
    public class OboController : Controller
    {

        private string endpoint = "https://testonlinepay.telecelzimbabwe.co.zw/ObopayExternalWS/ObopayExternalWebService";
        private string username = "230106";
        private string password = "online1234";
        private string partner_id = "RUBIEM";
        private string encryption_key = "haskingvista127$";

        private IHttpContextAccessor _accessor;

        public OboController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }



        [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// construct and return the client for the request
        /// </summary>
        /// <returns></returns>
        private ObopayExternalWebServiceClient get_client()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            //configure end point
            var endPoint = new EndpointAddress(endpoint);
            //declare client
            ObopayExternalWebServiceClient client = new ObopayExternalWebServiceClient(myBinding, endPoint);
            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(username, password);

            return client;
        }

        [HttpPost("Process")]
        public JsonResult Process(string otp)
        {
            var request = new ObopayWebServiceData();

            try
            {
                request.ApiIdType = null;
                request.Tier1AgentId = null;
                request.Tier1AgentName = null;
                request.Tier2AgentId = null;
                request.Tier3AgentId = null;
                request.Tier1AgentPassword = null;
                request.CustomerPhoneNumber = null;
                request.NationalID = null;
                request.SenderFirstName = null;
                request.SenderLastName = null;
                request.TransactionType = null;
                request.InstrumentType = null;
                request.ProcessorCode = null;
                request.CashInAmount = null;
                request.PaymentDetails1 = null;
                request.PaymentDetails2 = null;
                request.PaymentDetails3 = null;
                request.PaymentDetails4 = null;
                request.TxnAmount = null;
                request.CurrencyType = null;
                request.NetTxnAmount = null;
                request.FeeAmount = null;
                request.TaxAmount = null;
                request.Country = null;
                request.RequestId = null;
                request.TerminalID = null;
                request.TxnStatus = null;
                request.ErrorCode = null;
                request.OpsTransactionId = null;
                request.TransactionDate = null;
                request.Remark = null;
                request.Reference1 = null;
                request.Reference2 = null;
                request.Reference3 = null;
                request.Reference4 = null;
                request.Reference5 = null;


                //enforce defaults
                request.ApiIdType = "REQ";//This field contains API ID type as ‘REQ’=request or ‘RES’=response.
                //request.Tier1AgentId = "RUBIEM";//This field contains the requesting channel. The data to be passed in this field will be shared by the Obopay during implementation
                request.Tier3AgentId = username;//This field contains the Merchant’s mobile number
                request.CustomerPhoneNumber = "0733114692";//This field contains Customer mobile number making the request.
                request.TransactionType = "500";//This field contains specific Transaction code predefined in OPS for every transaction.
                request.InstrumentType = "1";//This field contains the Instrument type used, i.e. Wallet = 1, Cash = 0, Bank Account = 2 or Card = 3. So, In this case it is mentioned as 1.
                request.ProcessorCode = "0026";//This field contains the Processor code for every transaction.
                request.PaymentDetails1 = otp;//OTP for which request is generated from Customer(Mandatory)
                request.TxnAmount = 10;//This field contains the transaction amount
                request.FeeAmount = 20;//Calculated Fee for the transaction amount
                request.TaxAmount = 10;
                request.RequestId = Guid.NewGuid().ToString();//This field contains Unique Request ID for each web service request initiation
                request.TerminalID = "123";
                request.TransactionDate = DateTime.Now;
                request.Reference1 = "ONUS";
                request.Reference2 = "CARDLESS";
                //request.Remark = "rubiem innovation obopay";//This field contains remark entered
                //request.Tier1AgentPassword = "online1234";
                //request.CurrencyType = "$";

                var _namespace = @"obons=""http://www.obopay.com/xml/ns/tpu/v1""";
                var obons_string = $@"<obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""UserName"">{username}</obons:header> 
                                      <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""Password"">{password}</obons:header> 
                                      <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""PartnerId"">{partner_id}</obons:header> 
                                      <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""iv"">{encryption_key}</obons:header>";
                // Create a custom soap header
                var header_username = MessageHeader.CreateHeader("UserName", _namespace, (string)username, false);
                var header_password = MessageHeader.CreateHeader("Password", _namespace, (string)password, false);
                var header_partner_id = MessageHeader.CreateHeader("PartnerId", _namespace, (string)partner_id, false);
                var header_iv = MessageHeader.CreateHeader("iv", _namespace, (string)encryption_key, false);



                var client = get_client();
                using (var scope = new OperationContextScope(client.InnerChannel))
                {

                    // Add the header into request message
                    OperationContext.Current.OutgoingMessageHeaders.Add(header_partner_id);
                    OperationContext.Current.OutgoingMessageHeaders.Add(header_username);
                    OperationContext.Current.OutgoingMessageHeaders.Add(header_password);
                    OperationContext.Current.OutgoingMessageHeaders.Add(header_iv);

                    // Add a HTTP Header to an outgoing request
                    //HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    //requestMessage.Headers["PartnerId"] = partner_id;
                    //requestMessage.Headers["UserName"] = username;
                    //requestMessage.Headers["Password"] = password;
                    //requestMessage.Headers["iv"] = encryption_key;
                    //OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                    var serxml = new System.Xml.Serialization.XmlSerializer(request.GetType());
                    var ms = new MemoryStream();
                    serxml.Serialize(ms, request);
                    string xml = Encoding.UTF8.GetString(ms.ToArray());


                    //var response = client.processAsync(request).Result.Body.@return;
                    var req = $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.obopay.com/xml/oews/v1"" xmlns:java=""java:com.obopay.core.transferobject"" xmlns:java1=""java:com.obopay.ws.data"">
                                <soapenv:Header>
                                   <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""UserName"">{username}</obons:header> 
                                   <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""Password"">{Encrypt(password)}</obons:header> 
                                   <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""PartnerId"">{partner_id}</obons:header> 
                                   <obons:header xmlns:obons=""http://www.obopay.com/xml/ns/tpu/v1"" name=""iv"">{encryption_key}</obons:header>
                                </soapenv:Header>
                                   <soapenv:Body>
                                      <v1:process>
                                         <v1:request>
                                            <java:GwalTraceId></java:GwalTraceId>
                                            <java1:ApiIdType>REQ</java1:ApiIdType>
                                            <java1:Tier3AgentId>{username}</java1:Tier3AgentId>
                                            <java1:Tier1AgentPassword></java1:Tier1AgentPassword>
                                            <java1:CustomerPhoneNumber>0733114692</java1:CustomerPhoneNumber>
                                            <java1:TransactionType>500</java1:TransactionType>
                                            <java1:InstrumentType>1</java1:InstrumentType>
                                            <java1:ProcessorCode>0026</java1:ProcessorCode>
                                            <java1:PaymentDetails1>{otp}</java1:PaymentDetails1>
                                            <java1:TxnAmount>1</java1:TxnAmount>
                                            <java1:CurrencyType>USD</java1:CurrencyType>
                                            <java1:NetTxnAmount></java1:NetTxnAmount>
                                            <java1:FeeAmount>20</java1:FeeAmount>
                                            <java1:TaxAmount>10</java1:TaxAmount>
                                            <java1:Country></java1:Country>
                                            <java1:RequestId>{Guid.NewGuid().ToString()}</java1:RequestId>
                                            <java1:TerminalID>123</java1:TerminalID>
                                            <java1:TxnStatus></java1:TxnStatus>
                                            <java1:ErrorCode></java1:ErrorCode>
                                            <java1:OpsTransactionId></java1:OpsTransactionId>
                                            <java1:TransactionDate>{DateTime.Now.ToString("yyyy-MM-dd")}</java1:TransactionDate> 
                                            <java1:Remark></java1:Remark>
                                            <java1:Reference1>ONUS</java1:Reference1>
                                            <java1:Reference2>CARDLESS</java1:Reference2>
                                         </v1:request>
                                      </v1:process>
                                   </soapenv:Body>
                                </soapenv:Envelope>
                               ";
                    var c = new HttpClient();
                    var r = c.PostAsync(endpoint, new StringContent(req, Encoding.UTF8, "application/soap+xml")).Result.Content.ReadAsStringAsync().Result;
                    return Json(new
                    {
                        res = "ok",
                        msg = r,
                        original_request = req,
                    });
                }

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("TelecelOboPay.Controllers.OboController.Process", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message,
                    original_request = request
                });
            }


        }


        private string Encrypt(string input)
        {
            //
            System.Text.UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            //init
            //RijndaelManaged rijndael = new RijndaelManaged();
            var rijndael = Aes.Create();
            rijndael.Mode = CipherMode.CBC;
            rijndael.Padding = PaddingMode.PKCS7;
            rijndael.KeySize = 128;
            rijndael.BlockSize = 128;
            //
            var base64key = Convert.ToBase64String(Encoding.UTF8.GetBytes(encryption_key));
            var base64iv = "47l5QsSe1POo31adQ/u7nQ==";
            rijndael.Key = Convert.FromBase64String(base64key);
            rijndael.IV = Convert.FromBase64String(base64iv);
            //encryt to byte
            ICryptoTransform encryptor = rijndael.CreateEncryptor();
            byte[] cipher = unicodeEncoding.GetBytes(input);
            byte[] encryptedValue = encryptor.TransformFinalBlock(cipher, 0, cipher.Length);
            //encryt to string
            return Convert.ToBase64String(encryptedValue);
        }
        private string Decrypt(string inputBase64)
        {
            //
            System.Text.UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            //init
            //RijndaelManaged rijndael = new RijndaelManaged();
            var rijndael = Aes.Create();
            rijndael.Mode = CipherMode.CBC;
            rijndael.Padding = PaddingMode.PKCS7;
            rijndael.KeySize = 128;
            rijndael.BlockSize = 128;
            //
            var base64key = Convert.ToBase64String(Encoding.UTF8.GetBytes(encryption_key));
            var base64iv = "47l5QsSe1POo31adQ/u7nQ==";
            rijndael.Key = Convert.FromBase64String(base64key);
            rijndael.IV = Convert.FromBase64String(base64iv);
            //decrypt from byte
            var bytes_to_decrypt = Convert.FromBase64String(inputBase64);
            ICryptoTransform transform = rijndael.CreateDecryptor();
            byte[] decryptedValue = transform.TransformFinalBlock(bytes_to_decrypt, 0, bytes_to_decrypt.Length);
            return unicodeEncoding.GetString(decryptedValue);
        }


        /// <summary>
        /// move money from bank account to wallet
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="customer_mobile"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        [HttpPost("customer_cash_in")]
        private JsonResult customer_cash_in(string pin, string customer_mobile, decimal amount, string currency)
        {
            try
            {
                if (string.IsNullOrEmpty(pin))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "pin is missing."
                    });
                }

                if (string.IsNullOrEmpty(customer_mobile))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "customer_mobile is missing."
                    });
                }

                if (amount <= 0)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "float amount is invalid."
                    });
                }

                if (string.IsNullOrEmpty(currency))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "currency is missing."
                    });
                }

                //get client
                var client = get_client();
                var authorizationRequest = new AuthorizationRequest()
                {
                    Pin = pin,
                    Currency = currency,
                    TerminalId = _accessor.HttpContext.Connection.RemoteIpAddress.ToString()
                };

                var auth = client.authorizeAsync(authorizationRequest).Result.Body.@return;




                var guid_request_id = Guid.NewGuid().ToString();//add 
                var cashInRequest = new CashInRequest();
                cashInRequest.Pin = pin;
                cashInRequest.TerminalId = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                cashInRequest.CustomerPhoneNumber = customer_mobile;
                cashInRequest.Amount = amount;
                cashInRequest.Currency = currency;
                cashInRequest.RequestId = guid_request_id;
                cashInRequest.Country = "ZW";
                cashInRequest.TransactionType = "";
                cashInRequest.AdditionalField1 = "Stan_chart_rubiem_Telecash_obo_pay_api";

                var response = client.customerCashInAsync(cashInRequest).Result.Body.@return;

                return Json(new
                {
                    res = "ok",
                    msg = response
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("TelecelOboPay.Controllers.OboController.customer_cash_in", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// move money from account to wallet bank acccount
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="customer_mobile"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        [HttpPost("customer_cash_out")]
        private JsonResult customer_cash_out(string pin, string customer_mobile, decimal amount, string currency)
        {
            try
            {
                if (string.IsNullOrEmpty(pin))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "pin is missing."
                    });
                }

                if (string.IsNullOrEmpty(customer_mobile))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "customer_mobile is missing."
                    });
                }

                if (amount <= 0)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "float amount is invalid."
                    });
                }

                if (string.IsNullOrEmpty(currency))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "currency is missing."
                    });
                }

                //get client
                var client = get_client();
                var authorizationRequest = new AuthorizationRequest()
                {
                    Pin = pin,
                    Currency = currency,
                    TerminalId = _accessor.HttpContext.Connection.RemoteIpAddress.ToString()
                };

                var auth = client.authorizeAsync(authorizationRequest).Result.Body.@return;




                var guid_request_id = Guid.NewGuid().ToString();//add 
                var cashOutRequest = new CashOutRequest();
                cashOutRequest.Pin = pin;
                cashOutRequest.TerminalId = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                cashOutRequest.CustomerPhoneNumber = customer_mobile;
                cashOutRequest.Amount = amount;
                cashOutRequest.Currency = currency;
                cashOutRequest.RequestId = guid_request_id;
                cashOutRequest.Country = "ZW";
                cashOutRequest.TransactionType = "";
                cashOutRequest.AdditionalField1 = "Stan_chart_rubiem_Telecash_obo_pay_api";

                var response = client.customerCashOutAsync(cashOutRequest).Result.Body.@return;

                return Json(new
                {
                    res = "ok",
                    msg = response
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("TelecelOboPay.Controllers.OboController.customer_cash_in", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        public void customer_cah_out() { }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void customer_registration() { }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void authorize() { }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void customer_viral_cash_out() { }

    }
}
