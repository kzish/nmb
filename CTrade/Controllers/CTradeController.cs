﻿using Ctrade.Models;
using CTrade.Models;
using CTradeEscrow;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CTrade.Controllers
{
    /// <summary>
    /// ctrade api 
    /// </summary>
    [Route("CTrade")]
    public class CTradeController : Controller
    {

        private dbContext db = new dbContext();
        private IHttpContextAccessor _accessor;
        private string ctrade_mno = "STANC";
        private string ctrade_username = "STANC";
        private string ctrade_password = "$t@nch@rt2019";
        private const string c_trade_end_point = "http://197.155.235.78:9000/CTradeEscrowService.asmx";//todo: get live url 

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }



        public CTradeController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }




        /// <summary>
        /// construct and return the scb client for the request
        /// </summary>
        /// <returns></returns>
        private CTradeEscrowServiceSoapClient get_client()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            //configure end point
            var endPoint = new EndpointAddress(c_trade_end_point);
            //declare client
            CTradeEscrowServiceSoapClient ctrade_client = new CTradeEscrowServiceSoapClient(myBinding, endPoint);
            ctrade_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(ctrade_username, ctrade_password, "197.155.235.78");
            return ctrade_client;
        }


        /// <summary>
        /// This method is used to cancel a Non-Executed orders from an Exchange. Cancel order method works
        /// on orders which are Open, Pending and Auto-Trade.Other order statuses like Matched, Settled,
        /// Cancelled and Pending Cancellation cannot be cancelled.
        /// </summary>
        /// <param name="ordernum"></param>
        /// <returns>
        /// The response will be sent back as an XML message with a response code in between the <Response>
        /// tag.The response codes vary depending on the status of the processing
        /// </returns>
        [HttpPost("CancelOrder")]
        public JsonResult CancelOrder(string ordernum)
        {
            //log the activity
            var activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.CancelOrder()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call Cancel Order";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var response = ctrade_client.CancelOrderAsync(ordernum: ordernum,
                    username: ctrade_username,
                    PassWord: ctrade_password
                    ).Result;

                //deserialise api repsonse
                XDocument xdoc = XDocument.Parse(response);
                var result = (from xml in xdoc.Descendants(XName.Get("respondeCode"))
                              select xml).FirstOrDefault();
                dynamic json_call_back_response = JObject.Parse(JsonConvert.SerializeObject(result).ToString());

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.CancelOrder().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = response;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = json_call_back_response
                });


            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.CancelOrder", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="PassWord"></param>
        /// <param name="Amount"></param>
        /// <param name="cds_number"></param>
        /// <param name="receiptnumber"></param>
        /// <param name="TelephoneNumber"></param>
        /// <param name="MNO"></param>
        /// <returns></returns>
        [HttpPost("PostBillerCart")]
        public async Task<JsonResult> PlaceDeposit([FromBody]mBillerCartRequest billerCartRequest)
        {

            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var result = string.Empty;

                //loop through the items to buy the shares
                foreach (var item in billerCartRequest.items)
                {
                    //purchase the number of shares ordered, specified by the quantity
                    for (int i = 0; i < item.quantity; i++)
                    {
                        var res = await ctrade_client.PlacedepositAsync(
                        username: ctrade_username,
                        PassWord: ctrade_password,
                        Amount: item.unit_price.ToString(),
                        cds_number: item.product_code,
                        receiptnumber: $"{billerCartRequest.payment_ref}.{i}",//append index of item to the request
                        TelephoneNumber: billerCartRequest.payer_mobile,
                        MNO: ctrade_mno
                        );
                        var xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(res);
                        string xpath = "respondeCode";
                        var nodes = xmlDoc.SelectNodes(xpath);
                        foreach (XmlNode c in nodes)
                        {
                            result += c.InnerText;
                        }
                    }
                }

                if (result.Contains("1"))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "One or more transactions failed",
                    });
                }
                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "success",
                    tracking_id = billerCartRequest.payment_ref,
                    payment_ref = billerCartRequest.payment_ref
                });

            }
            catch (Exception ex)
            {

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.PlaceDeposit", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// This is for checking status of an existing order. Returns different order statuses for a specified order eg
        /// Open, Pending, Matched, Settled, Rejected etc.
        /// </summary>
        /// <param name="ordernum"></param>
        /// <returns>
        /// Returns the status of the order. Order Statuses to expect are Open, Pending, Matched, Settled,
        /// Rejected, AutoTrade, Cancelled and Pending Cancellation.
        /// </returns>
        [HttpGet("OrderStatus")]
        public JsonResult OrderStatus(string ordernum)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.OrderStatus()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call OrderStatus";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.OrderStatusAsync(
                    username: ctrade_username,
                    PassWord: ctrade_password,
                    ordernum: ordernum
                    ).Result;

                //deserialise api repsonse
                XDocument xdoc = XDocument.Parse(xml_result);
                var result = (from xml in xdoc.Descendants(XName.Get("respondeCode"))
                              select xml).FirstOrDefault();
                dynamic json_call_back_response = JObject.Parse(JsonConvert.SerializeObject(result).ToString());

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.OrderStatus().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = json_call_back_response
                });



            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.OrderStatus", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CDS_Number"></param>
        /// <param name="TelephoneEnqure"></param>
        /// <param name="emailaddress"></param>
        /// <param name="ministt"></param>
        /// <param name="Language"></param>
        /// <param name="company"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        public JsonResult RecieveUSSDEnquireDec(string CDS_Number, string TelephoneEnqure, string emailaddress, string ministt, string Language, string company)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.RecieveUSSDEnquireDec()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to ReceiveUSSDEnquireDecAsync";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var result = ctrade_client.ReceiveUSSDEnquireDecAsync(
                    UserName: ctrade_username,
                    PassWord: ctrade_password,
                    CDS_Number: CDS_Number,
                    TelephoneEnqure: TelephoneEnqure,
                    emailaddress: emailaddress,
                    ministt: ministt,
                    MNO_: ctrade_mno,
                    Language: Language,
                    company: company
                    ).Result;

                //deserialise api repsonse
                //todo:

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.ReceiveUSSDEnquireDec().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = "nil"
                });

            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.RecieveUSSDEnquireDec", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// This method returns all the companies in the system including the current price for each company and
        /// the IPO Status for the company
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetBillerProducts")]
        public JsonResult GetCompanies()
        {


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.GETCOMPANIESAsync().Result;

                //clean the xml result 
                xml_result = xml_result.Replace("<accounts xmlns=\"http://www.example.org/holderConversion\"", "");
                xml_result = xml_result.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                xml_result = xml_result.Replace("xsi:schemaLocation=\"http://www.example.org/companies holderConversion.xsd\">", "");
                xml_result = xml_result.Replace("xsi:schemaLocation=\"http://www.example.org/companies holderConversion.xsd\"/>", "");
                xml_result = xml_result.Replace(">\"", ">");
                xml_result.Trim();
                //deserialise api repsonse
                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("Company")).FirstOrDefault();
                dynamic json = JObject.Parse(JsonConvert.SerializeObject(xml_json).ToString());

                //map the response onto the standard mBillerProduct
                var biller_products = new List<MBillerProduct>();
                foreach (var obj in json.Company.companydetails)
                {
                    var product = new MBillerProduct();
                    //
                    product.ProductCode = obj.companyname;
                    product.Name = obj.fullname;
                    product.Category = obj.securitytype;
                    product.Description = obj.exchange;
                    product.Price = obj.price;
                    product.ImageUrl = null;
                    product.Available = obj.ipostatus == "1" ? true : false;
                    product.BillerCode = "ctrade";
                    //
                    biller_products.Add(product);
                }


                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = biller_products
                });
            }
            catch (Exception ex)
            {

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.GetCompanies", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cds"></param>
        /// <param name="company"></param>
        /// <returns></returns>
        [HttpGet("BalanceEnquire")]
        public JsonResult BalanceEnquire(string cds, string company)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.BalanceEnquire()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var result = ctrade_client.BalanceEnquireAsync(
                    cds: cds,
                    company: company
                    ).Result;

                //deserialise api repsonse


                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.BalanceEnquire().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = result
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.BalanceEnquire", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// comfirm company details
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [HttpPost("CheckAccount")]
        public async Task<JsonResult> CompanyEnquiry([FromBody]mBillerCartRequest billerCartRequest)
        {
            string response = string.Empty;

            try
            {
                //loop thru all the items confirming the companies
                foreach (var item in billerCartRequest.items)
                {
                    string company = item.product_code;
                    if (string.IsNullOrEmpty(company))
                    {
                        response += "No company provided " + Environment.NewLine;
                        continue;
                    }
                    //get client
                    var ctrade_client = get_client();
                    //execute
                    var xml_result = await ctrade_client.CompanyEnquiryAsync(
                        company: company
                        );
                    //clean xml_result
                    xml_result = xml_result.Replace("<accounts xmlns=\"http://www.example.org/holderConversion\"", "");
                    xml_result = xml_result.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    xml_result = xml_result.Replace("xsi:schemaLocation=\"http://www.example.org/companies holderConversion.xsd\">", "");
                    xml_result = xml_result.Replace(">\"", ">");
                    //deserialise api repsonse
                    //var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("Company")).FirstOrDefault();

                    var xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(xml_result);
                    var company_ = xmlDoc.SelectNodes("Company").Item(0).SelectNodes("companydetails").Item(0);
                    var companyname = company_["companyname"].InnerText;
                    var fullname = company_["fullname"].InnerText;
                    var securitytype = company_["securitytype"].InnerText;
                    var exchange = company_["exchange"].InnerText;
                    var price = company_["price"].InnerText;


                    var parsed_response = $@"company:{companyname}
fullname:{fullname}
securitytype:{securitytype}
exchange:{exchange}
price:{price}";

                    //add to response
                    response = response += parsed_response + Environment.NewLine + Environment.NewLine;
                }//foreach

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = response
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("CTrade.Controllers.CTradeController.CompanyEnquiry", ex);
                //create response
                return Json(new
                {
                    res = "err",
                    msg = "Error occurred."
                });
            }
        }

        /// <summary>
        /// This process allows Customers to buy units on the secondary market. The customer starts with entering
        /// their PIN and selecting Buy, they enter the amount which is validated against their wallet balance and
        /// a full quotation produced.On confirmation, they are given necessary messages.
        /// </summary>
        /// <param name="CDS_Number"></param>
        /// <param name="TelephoneBorder"></param>
        /// <param name="NoofNotes"></param>
        /// <param name="Price"></param>
        /// <param name="Company"></param>
        /// <param name="Language"></param>
        /// <returns>
        /// The response will be sent back as an XML message with a response code in between the <Response>
        /// tag.The response codes vary depending on the status of the processing.
        /// </returns>
        [HttpPost("RecieveUSSDBuyOrder")]
        public JsonResult RecieveUSSDBuyOrder(string CDS_Number, string TelephoneBorder, string NoofNotes, string Price, string Company, string Language)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.RecieveUSSDBuyOrder()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to RecieveUSSDBuyOrder";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.ReceiveUSSDBuyOrderAsync(
                    CDS_Number: CDS_Number,
                    TelephoneBorder: TelephoneBorder,
                    NoofNotes: NoofNotes,
                    Price: Price,
                    Company: Company,
                    UserName: ctrade_username,
                    PassWord: ctrade_password,
                    MNO_: ctrade_mno,
                    Language: Language
                    ).Result;
                //re format the xml
                xml_result = "<result>" + xml_result;
                xml_result = xml_result + "</result>";
                //deserialise api repsonse
                var xml_json_response = XDocument.Parse(xml_result).Descendants(XName.Get("response")).FirstOrDefault();
                var xml_json_responseCode = XDocument.Parse(xml_result).Descendants(XName.Get("respondeCode")).FirstOrDefault();
                //create a json object
                dynamic json_response = new JObject();
                json_response.response = xml_json_response.Value;
                json_response.responseCode = xml_json_responseCode.Value;

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.ReceiveUSSDBuyOrder().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = json_response
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.RecieveUSSDBuyOrder", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// This process allows Customers to sell units on the secondary market. The customer starts with entering
        /// their PIN and selecting Sell, they enter the number of notes which is validated against their account
        /// balance and a full quotation produced.On confirmation they are given necessary messages
        /// </summary>
        /// <param name="CDS_Number"></param>
        /// <param name="TelephoneSelorder"></param>
        /// <param name="NoofNotes"></param>
        /// <param name="Price"></param>
        /// <param name="Company"></param>
        /// <param name="Language"></param>
        /// <returns>
        /// The response will be sent back as an XML message with a response code in between the <Response>
        /// tag.The response codes vary depending on the status of the processing
        /// </returns>
        [HttpPost("ReceiveUSSDSellOrder")]
        public JsonResult RecieveUSSDSellOrder(string CDS_Number, string TelephoneSelorder, string NoofNotes, string Price, string Company, string Language)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.ReceiveUSSDSellOrder()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to ReceiveUSSDSellOrder";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.ReceiveUSSDSellOrderAsync(
                    CDS_Number: CDS_Number,
                    TelephoneSelorder: TelephoneSelorder,
                    NoofNotes: NoofNotes,
                    Price: Price,
                    Company: Company,
                    UserName: ctrade_username,
                    PassWord: ctrade_password,
                    MNO_: ctrade_mno,
                    Language: Language
                    ).Result;

                //re format the xml
                xml_result = "<result>" + xml_result;
                xml_result = xml_result + "</result>";
                //deserialise api repsonse
                var xml_json_response = XDocument.Parse(xml_result).Descendants(XName.Get("response")).FirstOrDefault();
                var xml_json_responseCode = XDocument.Parse(xml_result).Descendants(XName.Get("respondeCode")).FirstOrDefault();
                //create a json object
                dynamic json_response = new JObject();
                json_response.response = xml_json_response.Value;
                json_response.responseCode = xml_json_responseCode.Value;

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.ReceiveUSSDSellOrder().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = json_response
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.RecieveUSSDSellOrder", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// This process allows investors to place a request for a quotation before placing an order. The function
        /// applies to both primary market and secondary market orders
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="PassWord"></param>
        /// <param name="OrderType"></param>
        /// <param name="Company"></param>
        /// <param name="InputType"></param>
        /// <param name="InputValue"></param>
        /// <returns></returns>
        [HttpGet("PriceCalculator")]
        public JsonResult PriceCalculator(string OrderType, string Company, string InputType, string InputValue)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.PriceCalculator()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call PriceCalculator";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.PriceCalculatorAsync(
                  UserName: ctrade_username,
                  PassWord: ctrade_password,
                  OrderType: OrderType,
                  Company: Company,
                  InputType: InputType,
                  InputValue: InputValue
                    ).Result;
                //re format the xml
                xml_result = "<result>" + xml_result;
                xml_result = xml_result + "</result>";
                //deserialise api repsonse
                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("result")).FirstOrDefault();



                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.PriceCalculator().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.PriceCalculator", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// This process allows customers to request for an account by providing minimal KYC through the
        /// Mobile Application.The KYC is verified by third parties by confirming ownership of the customer in
        /// question.This includes Banks, Mobile Network Operators among other licensed operators
        /// </summary>
        /// <param name="IDNumber"></param>
        /// <param name="OtherName"></param>
        /// <param name="SurnameOrCompanyName"></param>
        /// <param name="PostalCode"></param>
        /// <param name="Country"></param>
        /// <param name="DateOfBirth"></param>
        /// <param name="Gender"></param>
        /// <param name="Nationality"></param>
        /// <param name="Resident"></param>
        /// <param name="TelephoneNumber"></param>
        /// <param name="MNOCustodian"></param>
        /// <param name="BrokerCode"></param>
        /// <returns>
        /// The response will be sent back as an XML message with a response code in between the <Response>
        /// tag.The response codes vary depending on the status of the processing.
        /// </returns>
        [HttpPost("NewClientRegistrations")]
        public JsonResult NewClientRegistrations(
                    string IDNumber,
                    string OtherName,
                    string SurnameOrCompanyName,
                    string PostalCode,
                    string Country,
                    string DateOfBirth,
                    string Gender,
                    string Nationality,
                    bool Resident,
                    string TelephoneNumber,
                    string MNOCustodian,
                    string BrokerCode
                    )
        {

            //set the callback url
            string base_url = Request.Scheme + "://" + Request.Host.Host + ":" + Request.Host.Port + "/CTrade";
            string CallbackUrl = base_url + "/NewClientRegistrationsCallBackUrl";



            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrations()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call NewClientRegistrations";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.NewClientRegistrationsAsync(
                  MNO: ctrade_mno,
                    Username: ctrade_username,
                    Password: ctrade_password,
                    IDNumber: IDNumber,
                    OtherName: OtherName,
                    SurnameOrCompanyName: SurnameOrCompanyName,
                    PostalCode: PostalCode,
                    Country: Country,
                    DateOfBirth: DateOfBirth,
                    Gender: Gender,
                    Nationality: Nationality,
                    Resident: Resident,
                    TelephoneNumber: TelephoneNumber,
                    MNOCustodian: MNOCustodian,
                    CallbackUrl: CallbackUrl,
                    BrokerCode: BrokerCode
                    ).Result;

                //deserialise api repsonse
                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("respondeCode")).FirstOrDefault();


                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrations().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrations", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// call back url for the client registration
        /// </summary>
        /// <param name="CallBackResponse"></param>
        /// <returns></returns>
        [HttpPost("NewClientRegistrationsCallBackUrl")]
        public JsonResult NewClientRegistrationsCallBackUrl()
        {


            //response is inside the body of the request
            byte[] buffer = new byte[(int)Request.ContentLength];
            Request.Body.Read(buffer, 0, buffer.Length);
            var CallBackResponse = Encoding.ASCII.GetString(buffer);

            //log the callback
            Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl..CallBackResponse", CallBackResponse);

            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call NewClientRegistrationsCallBackUrl";
            db.MActivityLog.Add(activity);
            db.SaveChanges();

            try
            {
                //deserialise api repsonse
                //todo: send push to client when callbackurl is hit
                var xml_json = XDocument.Parse(CallBackResponse).Descendants(XName.Get("CallBackResponse", "EscrowService")).FirstOrDefault();
                var jsonCallBackResponse = Json(xml_json);

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = CallBackResponse;
                //log the callback
                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl..CallBackResponse", CallBackResponse);
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        [HttpPost("NewClientRegistrations_ZP")]
        public JsonResult NewClientRegistrations_ZP(
                    string IDNumber,
                    string OtherName,
                    string SurnameOrCompanyName,
                    string PostalCode,
                    string Country,
                    string DateOfBirth,
                    string Gender,
                    string Nationality,
                    bool Resident,
                    string TelephoneNumber,
                    string MNOCustodian,
                    string BrokerCode,
                    string CDS_Number,
                    string ConversationID,
                    string FileReference,
                    string CheckResult)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrations_ZP()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call NewClientRegistrations_ZP";
            db.MActivityLog.Add(activity);
            db.SaveChanges();

            //set the callback url
            string base_url = Request.Scheme + "://" + Request.Host.Host + ":" + Request.Host.Port + "/CTrade";
            string CallbackUrl = base_url + "/NewClientRegistrations_ZPCallBackUrl";


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.NewClientRegistrations_ZPAsync(
                  MNO: ctrade_mno,
                    Username: ctrade_username,
                    Password: ctrade_password,
                    IDNumber: IDNumber,
                    OtherName: OtherName,
                    SurnameOrCompanyName: SurnameOrCompanyName,
                    PostalCode: PostalCode,
                    Country: Country,
                    DateOfBirth: DateOfBirth,
                    Gender: Gender,
                    Nationality: Nationality,
                    Resident: Resident,
                    TelephoneNumber: TelephoneNumber,
                    MNOCustodian: MNOCustodian,
                    CallbackUrl: CallbackUrl,
                    BrokerCode: BrokerCode,
                    CDS_Number: CDS_Number,
                    ConversationID: ConversationID,
                    FileReference: FileReference,
                    CheckResult: CheckResult
                    ).Result;

                //deserialise api repsonse
                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("respondeCode")).FirstOrDefault();



                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrations_ZP().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrations_ZP", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// call back url
        /// </summary>
        /// <param name="CallBackResponse"></param>
        /// <returns></returns>
        [HttpPost("NewClientRegistrations_ZPCallBackUrl")]
        public JsonResult NewClientRegistrations_ZPCallBackUrl()
        {

            //response is inside the body of the request
            byte[] buffer = new byte[(int)Request.ContentLength];
            Request.Body.Read(buffer, 0, buffer.Length);
            var CallBackResponse = Encoding.ASCII.GetString(buffer);

            //log the callback
            Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl..CallBackResponse", CallBackResponse);

            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrationsCallBackUrl()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call NewClientRegistrations_ZPCallBackUrl";
            db.MActivityLog.Add(activity);
            db.SaveChanges();

            try
            {
                //deserialise api repsonse
                //todo: send push to client when callbackurl is hit
                var xml_json = XDocument.Parse(CallBackResponse).Descendants(XName.Get("CallBackResponse", "EscrowService")).FirstOrDefault();
                var jsonCallBackResponse = Json(xml_json);


                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.NewClientRegistrations_ZPCallBackUrl().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = CallBackResponse;
                //log the callback
                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrations_ZPCallBackUrl..CallBackResponse", CallBackResponse);
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.NewClientRegistrations_ZPCallBackUrl", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }



        /// <summary>
        /// This process provides a customer journey that allows registered customers to Bid for securities in the
        /// Primary Market.They are prompted to enter their PIN and select Buy followed by Amount to Buy. They
        /// must confirm and get respective notifications of order receipt and funds deductions from wallets.
        /// </summary>
        /// <param name="Quantity"></param>
        /// <param name="ReferenceNumber"></param>
        /// <param name="ClientType"></param>
        /// <param name="Custodian"></param>
        /// <param name="CSDAccountNumber"></param>
        /// <param name="MPESATransactionID"></param>
        /// <param name="ISIN"></param>
        /// <param name="TransNum"></param>
        /// <param name="PledgeIndicator"></param>
        /// <param name="PledgeeBPID"></param>
        /// <returns>
        /// The response will be sent back as an XML message with a response code in between the <Response>
        /// tag.The response codes vary depending on the status of the processing.
        /// </returns>
        [HttpPost("RegisterBids")]
        public JsonResult RegisterBids(double Quantity, string ReferenceNumber, string ClientType, string Custodian, string CSDAccountNumber, string MPESATransactionID, string ISIN, string TransNum, bool PledgeIndicator, string PledgeeBPID)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.RegisterBids()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call RegisterBids";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.RegisterBidsAsync(
                    MNO: ctrade_mno,
                    Username: ctrade_username,
                    Password: ctrade_password,
                    Quantity: Quantity,
                    ReferenceNumber: ReferenceNumber,
                    ClientType: ClientType,
                    Custodian: Custodian,
                    CSDAccountNumber: CSDAccountNumber,
                    MPESATransactionID: MPESATransactionID,
                    ISIN: ISIN,
                    TransNum: TransNum,
                    PledgeIndicator: PledgeIndicator,
                    PledgeeBPID: PledgeeBPID
                    ).Result;


                //re format the xml
                xml_result = "<result>" + xml_result;
                xml_result = xml_result + "</result>";
                //deserialise api repsonse
                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("result")).FirstOrDefault();

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.RegisterBids().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.RegisterBids", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// Returns units held by a client in all companies. 
        /// </summary>
        /// <param name="cds_number"></param>
        /// <param name="phone_number"></param>
        /// <returns>Returns all units held by a client in all companies.</returns>
        [HttpPost("FullPortfolio")]
        public JsonResult FullPortfolio(string cds_number, string phone_number)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.FullPortfolio()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call FullPortfolio";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.FullPortfolioAsync(
                    cds_number: cds_number,
                    phone_number: phone_number
                    ).Result;

                if (string.IsNullOrEmpty(xml_result))
                {
                    //generate response
                    return Json(new
                    {
                        res = "err",
                        msg = "nil"
                    });
                }

                //clean the xml
                xml_result = xml_result.Replace("<portfoliofull xmlns=\"http://www.example.org/fullportfolio\"", "");
                xml_result = xml_result.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                xml_result = xml_result.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                xml_result = xml_result.Replace("xsi:schemaLocation=\"http://www.example.org/fullportfolio.xsd\">", "");
                xml_result = xml_result.Replace(">\"", ">");

                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("Portfolio")).FirstOrDefault();


                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.FullPortfolio().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.FullPortfolio", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// This checks for the CDS Number for a client. The method requires the client phone number, then a
        /// CDS Number is returned
        /// </summary>
        /// <param name="phone_number"></param>
        /// <returns>The response will be the CSD Number for the client</returns>
        [HttpGet("GetCDS")]
        public JsonResult GetCDS(string phone_number)
        {
            //log the activity
            MActivityLog activity = new MActivityLog();
            var user_agent = Request.Headers["User-Agent"].ToString();
            activity.UserActivity = "CTrade.Controllers.CTradeController.GetCDS()";
            activity.Username = user_agent;
            activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            activity.Data = "attempt to call GetCDS";
            db.MActivityLog.Add(activity);
            db.SaveChanges();


            try
            {
                //get client
                var ctrade_client = get_client();
                //execute
                var xml_result = ctrade_client.getCDSAsync(
                    phone_number: phone_number
                    ).Result;

                //empty string result if number does not exist
                if (string.IsNullOrEmpty(xml_result))
                {
                    //generate response
                    return Json(new
                    {
                        res = "err",
                        msg = "nil"
                    });
                }

                var xml_json = XDocument.Parse(xml_result).Descendants(XName.Get("ClientDetails")).FirstOrDefault();

                //log api response
                activity = new MActivityLog();
                activity.UserActivity = "CTrade.Controllers.CTradeController.GetCDS().Response";
                activity.Username = user_agent;
                activity.UserIpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                activity.Data = xml_result;
                db.MActivityLog.Add(activity);
                db.SaveChanges();

                //generate response
                return Json(new
                {
                    res = "ok",
                    msg = xml_json
                });
            }
            catch (Exception ex)
            {
                //log the error
                var error_log = new MErrorLog();
                error_log.ActivityId = activity.Id;
                error_log.ErrorMessage = ex.Message;
                error_log.FullErrorMessage = ex.ToString();
                db.MErrorLog.Add(error_log);
                db.SaveChanges();

                Globals.log_data_to_file("CTrade.Controllers.CTradeController.GetCDS", ex);

                //create response
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }



    }//class
}//namespace
