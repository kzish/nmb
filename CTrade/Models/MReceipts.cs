﻿using System;
using System.Collections.Generic;

namespace CTrade.Models
{
    public partial class MReceipts
    {
        public int Id { get; set; }
        public string TransactionNumber { get; set; }
        public string PayerName { get; set; }
        public string PayerPhoneNumber { get; set; }
        public string PayerAccount { get; set; }
        public decimal? AmountPaid { get; set; }
        public bool? Posted { get; set; }
        public string BillerCode { get; set; }
        public string BillerProductId { get; set; }
        public int? BillerProductQuantity { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TrackingId { get; set; }
        public string Notes { get; set; }
        public bool? PaidToBiller { get; set; }
        public string PaymentToBillerRef { get; set; }
        public string OrderId { get; set; }
        public bool? ReversedByBiller { get; set; }
    }
}
