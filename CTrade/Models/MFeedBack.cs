﻿using System;
using System.Collections.Generic;

namespace CTrade.Models
{
    public partial class MFeedBack
    {
        public int Id { get; set; }
        public string Feedbacknumber { get; set; }
        public string Billercode { get; set; }
        public string Feedback { get; set; }
        public string Customername { get; set; }
        public string Customerphone { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsRead { get; set; }
    }
}
