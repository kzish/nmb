﻿using System;
using System.Collections.Generic;

namespace CTrade.Models
{
    public partial class EnumCurrencyCode
    {
        public EnumCurrencyCode()
        {
            MBankAccount = new HashSet<MBankAccount>();
        }

        public int Id { get; set; }
        public string CurrencyCode { get; set; }

        public virtual ICollection<MBankAccount> MBankAccount { get; set; }
    }
}
