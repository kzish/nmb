﻿using System;
using System.Collections.Generic;

namespace CTrade.Models
{
    public partial class EnumBillerCategory
    {
        public EnumBillerCategory()
        {
            MBillerInfo = new HashSet<MBillerInfo>();
        }

        public int Id { get; set; }
        public string Category { get; set; }

        public virtual ICollection<MBillerInfo> MBillerInfo { get; set; }
    }
}
