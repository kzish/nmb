﻿using System;
using System.Collections.Generic;

namespace CTrade.Models
{
    public partial class MTransAction
    {
        public MTransAction()
        {
            MBillerOrders = new HashSet<MBillerOrders>();
        }

        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string FromAccountNumber { get; set; }
        public string ToAccountNumber { get; set; }
        public string FromCurrencyCode { get; set; }
        public string ToCurrencyCode { get; set; }
        public string MessageSubType { get; set; }
        public string TransactionCurrency { get; set; }
        public string TransactionAmount { get; set; }
        public string FromBankCode { get; set; }
        public string ToBankCode { get; set; }
        public string CostRate { get; set; }
        public string ConversionRate { get; set; }
        public string UniqueReferenceIdentifier { get; set; }
        public string TransactionBranch { get; set; }
        public string TransactionPostingDate { get; set; }
        public string SourceSystemName { get; set; }
        public string TransactionType { get; set; }
        public string LcyEquivalent { get; set; }
        public string Countrycode { get; set; }
        public string TrackingId { get; set; }
        public string BillerCode { get; set; }
        public int? ApiCallType { get; set; }
        public string PayerName { get; set; }
        public string PayerMobile { get; set; }
        public string PayerDetails1 { get; set; }
        public string PayerDetails2 { get; set; }
        public string PayerDetails3 { get; set; }
        public string ApiResponseData { get; set; }
        public bool PostedToBiller { get; set; }

        public virtual ICollection<MBillerOrders> MBillerOrders { get; set; }
    }
}
