﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoolFunds.Models
{
    public class mContribution
    {
        public mPayerSCAccountDetails PayerSCAccountDetails { get; set; }
        public string poolname { get; set; }
        public string trackingid { get; set; }
        public decimal poolTransAmount { get; set; }
    }
}
