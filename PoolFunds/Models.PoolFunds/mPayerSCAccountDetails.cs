﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoolFunds.Models
{
    public class mPayerSCAccountDetails
    {
        public string payerAccountNumber { get; set; }
        public string payerCurrencyCode { get; set; }
        public string payerTransactionBranch { get; set; }
        public string payerCountrycode { get; set; }
    }
}
