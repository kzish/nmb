﻿using System;
using System.Collections.Generic;

namespace PoolFunds.Models
{
    public partial class MTransLoyalty
    {
        public int Id { get; set; }
        public string TransNumber { get; set; }
        public DateTime? TransDate { get; set; }
        public int? TransPoints { get; set; }
        public decimal? TransAmount { get; set; }
        public string TransCustomer { get; set; }
        public decimal? TransBalBf { get; set; }
        public decimal? TransBalCf { get; set; }
        public int? TransLoyaltyType { get; set; }
        public string TransActivity { get; set; }
        public string LoyaltyModelName { get; set; }
        public int? ApprovalRef { get; set; }
    }
}
