﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PoolFunds.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<MActivityLog> MActivityLog { get; set; }
        public virtual DbSet<MAddress> MAddress { get; set; }
        public virtual DbSet<MApiClients> MApiClients { get; set; }
        public virtual DbSet<MApiResources> MApiResources { get; set; }
        public virtual DbSet<MBankAccount> MBankAccount { get; set; }
        public virtual DbSet<MBillerInfo> MBillerInfo { get; set; }
        public virtual DbSet<MBillerProduct> MBillerProduct { get; set; }
        public virtual DbSet<MEnquiryAnswer> MEnquiryAnswer { get; set; }
        public virtual DbSet<MEnquiryQuestions> MEnquiryQuestions { get; set; }
        public virtual DbSet<MErrorLog> MErrorLog { get; set; }
        public virtual DbSet<MFeedBack> MFeedBack { get; set; }
        public virtual DbSet<MLoyaltyModel> MLoyaltyModel { get; set; }
        public virtual DbSet<MNotification> MNotification { get; set; }
        public virtual DbSet<MPayments> MPayments { get; set; }
        public virtual DbSet<MPool> MPool { get; set; }
        public virtual DbSet<MPoolMembers> MPoolMembers { get; set; }
        public virtual DbSet<MPoolTrans> MPoolTrans { get; set; }
        public virtual DbSet<MPoolTransRun> MPoolTransRun { get; set; }
        public virtual DbSet<MReceipts> MReceipts { get; set; }
        public virtual DbSet<MRedeemRule> MRedeemRule { get; set; }
        public virtual DbSet<MReferenceNumber> MReferenceNumber { get; set; }
        public virtual DbSet<MServiceEnquiry> MServiceEnquiry { get; set; }
        public virtual DbSet<MTransAction> MTransAction { get; set; }
        public virtual DbSet<MTransLoyalty> MTransLoyalty { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=localhost;database=SimbaApi;User Id=sa;Password=abc123!;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<Logs>(entity =>
            {
                entity.Property(e => e.Data).HasColumnType("text");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MActivityLog>(entity =>
            {
                entity.ToTable("mActivityLog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserActivity)
                    .HasColumnName("user_activity")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserDevice)
                    .HasColumnName("user_device")
                    .IsUnicode(false);

                entity.Property(e => e.UserIpAddress)
                    .HasColumnName("user_ip_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MAddress>(entity =>
            {
                entity.ToTable("mAddress");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AptNum).HasMaxLength(50);

                entity.Property(e => e.BillerCode).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Extention).HasMaxLength(50);

                entity.Property(e => e.Street).HasMaxLength(50);
            });

            modelBuilder.Entity<MApiClients>(entity =>
            {
                entity.ToTable("mApiClients");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secret)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.MApiClients)
                    .HasForeignKey<MApiClients>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mApiClients_AspNetUsers");
            });

            modelBuilder.Entity<MApiResources>(entity =>
            {
                entity.ToTable("mApiResources");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ResourceName)
                    .HasColumnName("resource_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MBankAccount>(entity =>
            {
                entity.ToTable("mBankAccount");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AccountName).HasMaxLength(50);

                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.BillerId)
                    .IsRequired()
                    .HasColumnName("Biller_Id")
                    .HasMaxLength(450);

                entity.Property(e => e.EnumBank).HasColumnName("enum_Bank");

                entity.Property(e => e.EnumCurrencyCode).HasColumnName("enum_CurrencyCode");

                entity.HasOne(d => d.Biller)
                    .WithMany(p => p.MBankAccount)
                    .HasForeignKey(d => d.BillerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mBankAccount_AspNetUsers");
            });

            modelBuilder.Entity<MBillerInfo>(entity =>
            {
                entity.ToTable("mBillerInfo");

                entity.HasIndex(e => e.ContactEmail)
                    .HasName("fk_aspNetUserEmail");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.BillerCode).HasMaxLength(50);

                entity.Property(e => e.BillerName).HasMaxLength(50);

                entity.Property(e => e.ConnectionType).HasMaxLength(50);

                entity.Property(e => e.ContactEmail).HasMaxLength(256);

                entity.Property(e => e.ContactPerson).HasMaxLength(50);

                entity.Property(e => e.ContactPhone).HasMaxLength(50);

                entity.Property(e => e.CredentialsPassword).HasMaxLength(50);

                entity.Property(e => e.CredentialsUsername).HasMaxLength(50);

                entity.Property(e => e.Enabler)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EnumBillerCategory).HasColumnName("enum_BillerCategory");

                entity.Property(e => e.EnumBillerType).HasColumnName("enum_BillerType");

                entity.Property(e => e.EnumClientIndentifier).HasColumnName("enum_ClientIndentifier");

                entity.Property(e => e.HasAccounts).HasColumnName("hasAccounts");

                entity.Property(e => e.HasAdditionalData).HasColumnName("hasAdditionalData");

                entity.Property(e => e.HasCart).HasColumnName("hasCart");

                entity.Property(e => e.HasProducts).HasColumnName("hasProducts");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.MBillerInfo)
                    .HasForeignKey<MBillerInfo>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mBillerInfo_AspNetUsers");
            });

            modelBuilder.Entity<MBillerProduct>(entity =>
            {
                entity.ToTable("mBillerProduct");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Available).HasColumnName("available");

                entity.Property(e => e.BillerCode)
                    .IsRequired()
                    .HasColumnName("biller_code")
                    .HasMaxLength(450);

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("image_url")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasColumnName("product_code")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MEnquiryAnswer>(entity =>
            {
                entity.ToTable("mEnquiryAnswer");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Answer)
                    .HasColumnName("answer")
                    .IsUnicode(false);

                entity.Property(e => e.Question)
                    .HasColumnName("question")
                    .IsUnicode(false);

                entity.Property(e => e.Questionid).HasColumnName("questionid");

                entity.Property(e => e.ServiceEnquiryId).HasColumnName("service_enquiry_id");
            });

            modelBuilder.Entity<MEnquiryQuestions>(entity =>
            {
                entity.ToTable("mEnquiryQuestions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Billercode)
                    .HasColumnName("billercode")
                    .HasMaxLength(50);

                entity.Property(e => e.Question).HasColumnName("question");

                entity.Property(e => e.Status).HasColumnName("status");
            });

            modelBuilder.Entity<MErrorLog>(entity =>
            {
                entity.ToTable("mErrorLog");

                entity.HasIndex(e => e.ActivityId)
                    .HasName("IX_mErrorLog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActivityId).HasColumnName("activity_id");

                entity.Property(e => e.ErrorMessage)
                    .HasColumnName("error_message")
                    .IsUnicode(false);

                entity.Property(e => e.FullErrorMessage)
                    .HasColumnName("full_error_message")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MFeedBack>(entity =>
            {
                entity.ToTable("mFeedBack");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Billercode)
                    .HasColumnName("billercode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Customername)
                    .HasColumnName("customername")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Customerphone)
                    .HasColumnName("customerphone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Feedback)
                    .HasColumnName("feedback")
                    .IsUnicode(false);

                entity.Property(e => e.Feedbacknumber)
                    .IsRequired()
                    .HasColumnName("feedbacknumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsRead).HasColumnName("isRead");
            });

            modelBuilder.Entity<MLoyaltyModel>(entity =>
            {
                entity.ToTable("mLoyaltyModel");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BonusPoints).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LoyaltyModelName)
                    .HasColumnName("loyaltyModelName")
                    .HasMaxLength(50);

                entity.Property(e => e.LoyaltyPointsCriteria)
                    .HasColumnName("loyaltyPointsCriteria")
                    .HasMaxLength(50);

                entity.Property(e => e.LoyaltyPointsMaxValue)
                    .HasColumnName("loyaltyPointsMaxValue")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LoyaltyPointsMinValue)
                    .HasColumnName("loyaltyPointsMinValue")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PointsPerAmount).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<MNotification>(entity =>
            {
                entity.ToTable("mNotification");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BillerCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsRead).HasColumnName("isRead");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MPayments>(entity =>
            {
                entity.ToTable("mPayments");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BatchNo).HasMaxLength(50);

                entity.Property(e => e.BillerCode).HasMaxLength(50);

                entity.Property(e => e.PaidAmount).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId)
                    .HasColumnName("TrackingID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MPool>(entity =>
            {
                entity.ToTable("mPool");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.EnumContributioncycle).HasColumnName("enum_contributioncycle");

                entity.Property(e => e.Poolcontributionamount)
                    .HasColumnName("poolcontributionamount")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Pooldiscription)
                    .HasColumnName("pooldiscription")
                    .HasMaxLength(50);

                entity.Property(e => e.Poolenddate)
                    .HasColumnName("poolenddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Poolname)
                    .HasColumnName("poolname")
                    .HasMaxLength(50);

                entity.Property(e => e.Poolstartdate)
                    .HasColumnName("poolstartdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Terms).HasColumnName("terms");
            });

            modelBuilder.Entity<MPoolMembers>(entity =>
            {
                entity.ToTable("mPoolMembers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("accountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Administrator).HasColumnName("administrator");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Poolname)
                    .HasColumnName("poolname")
                    .HasMaxLength(50);

                entity.Property(e => e.Position).HasColumnName("position");
            });

            modelBuilder.Entity<MPoolTrans>(entity =>
            {
                entity.ToTable("mPoolTrans");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApprovalRef).HasColumnName("approvalRef");

                entity.Property(e => e.PoolTransAmount)
                    .HasColumnName("poolTransAmount")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.PoolTransCustomer)
                    .HasColumnName("poolTransCustomer")
                    .HasMaxLength(50);

                entity.Property(e => e.PoolTransDate)
                    .HasColumnName("poolTransDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Poolname)
                    .HasColumnName("poolname")
                    .HasMaxLength(50);

                entity.Property(e => e.Pooltransnumber)
                    .HasColumnName("pooltransnumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Trackingid)
                    .HasColumnName("trackingid")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MPoolTransRun>(entity =>
            {
                entity.ToTable("mPoolTransRun");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NextPoolRunDate)
                    .HasColumnName("nextPoolRunDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PoolTransAmount)
                    .HasColumnName("poolTransAmount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PoolTransCustomer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoolTransDate)
                    .HasColumnName("poolTransDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Poolname)
                    .HasColumnName("poolname")
                    .HasMaxLength(50);

                entity.Property(e => e.Pooltransnumber)
                    .HasColumnName("pooltransnumber")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MReceipts>(entity =>
            {
                entity.ToTable("mReceipts");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AmountPaid).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.BillerCode).HasMaxLength(50);

                entity.Property(e => e.BillerProductId)
                    .HasColumnName("BillerProductID")
                    .HasMaxLength(50);

                entity.Property(e => e.Notes).HasMaxLength(50);

                entity.Property(e => e.OrderId)
                    .HasColumnName("OrderID")
                    .HasMaxLength(50);

                entity.Property(e => e.PayerAccount).HasMaxLength(50);

                entity.Property(e => e.PayerName).HasMaxLength(50);

                entity.Property(e => e.PayerPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PaymentToBillerRef).HasMaxLength(50);

                entity.Property(e => e.TrackingId).HasMaxLength(50);

                entity.Property(e => e.TransactionDate).HasColumnType("datetime");

                entity.Property(e => e.TransactionNumber)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MRedeemRule>(entity =>
            {
                entity.ToTable("mRedeemRule");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AmountPerPoint).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.RedeemRuleCriteria).HasMaxLength(50);

                entity.Property(e => e.RedeemRuleMinPoints).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.RedeemRuleName).HasMaxLength(50);
            });

            modelBuilder.Entity<MReferenceNumber>(entity =>
            {
                entity.HasKey(e => e.Reference);

                entity.ToTable("mReferenceNumber");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<MServiceEnquiry>(entity =>
            {
                entity.ToTable("mServiceEnquiry");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Billercode)
                    .HasColumnName("billercode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsRead).HasColumnName("isRead");

                entity.Property(e => e.PayerName)
                    .HasColumnName("payerName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phoneNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MTransAction>(entity =>
            {
                entity.ToTable("mTransAction");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BillerCode)
                    .HasColumnName("billerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversionRate")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CostRate)
                    .HasColumnName("costRate")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Countrycode)
                    .HasColumnName("countrycode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FromAccountNumber)
                    .HasColumnName("fromAccountNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FromBankCode)
                    .HasColumnName("fromBankCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FromCurrencyCode)
                    .HasColumnName("fromCurrencyCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LcyEquivalent)
                    .HasColumnName("lcyEquivalent")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MessageSubType)
                    .HasColumnName("messageSubType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceSystemName)
                    .HasColumnName("sourceSystemName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToAccountNumber)
                    .HasColumnName("toAccountNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToBankCode)
                    .HasColumnName("toBankCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToCurrencyCode)
                    .HasColumnName("toCurrencyCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrackingId)
                    .HasColumnName("trackingId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionAmount)
                    .HasColumnName("transactionAmount")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionBranch)
                    .HasColumnName("transactionBranch")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionCurrency)
                    .HasColumnName("transactionCurrency")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionPostingDate)
                    .HasColumnName("transactionPostingDate")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionType)
                    .HasColumnName("transactionType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UniqueReferenceIdentifier)
                    .HasColumnName("uniqueReferenceIdentifier")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MTransLoyalty>(entity =>
            {
                entity.ToTable("mTransLoyalty");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApprovalRef).HasColumnName("approvalRef");

                entity.Property(e => e.LoyaltyModelName)
                    .HasColumnName("loyaltyModelName")
                    .HasMaxLength(50);

                entity.Property(e => e.TransActivity).HasMaxLength(50);

                entity.Property(e => e.TransAmount).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.TransBalBf).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TransBalCf).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TransCustomer).HasMaxLength(50);

                entity.Property(e => e.TransDate).HasColumnType("datetime");

                entity.Property(e => e.TransNumber).HasMaxLength(50);
            });

            modelBuilder.Entity<Providers>(entity =>
            {
                entity.HasIndex(e => e.ProductId);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ZssproviderName).HasColumnName("ZSSProviderName");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
