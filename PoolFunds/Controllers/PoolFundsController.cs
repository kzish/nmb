﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PoolFunds.Models;


namespace PoolFunds.Controllers
{
    [Route("scpayment/v1")]
    public class PoolFundsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpPost("AddPoolFundGroup")]
        public JsonResult AddPoolFundGroup([FromBody] MPool PoolFundGroup)
        {
            try
            {


                db.MPool.Add(PoolFundGroup);
                db.SaveChanges();


                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Pool Fund Group",

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("AddPoolFundGroupMember")]
        public JsonResult AddPoolFundGroupFundMember([FromBody] MPoolMembers poolMembers)
        {
            try
            {


                db.MPoolMembers.Add(poolMembers);
                db.SaveChanges();

                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Pool Fund Member"

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("PoolFundRun")]
        public JsonResult PoolFundRun([FromBody] MPoolTransRun poolTransRun)
        {
            try
            {


                db.MPoolTransRun.Add(poolTransRun);
                db.SaveChanges();

                return Json(new
                {
                    res = "ok",
                    msg = "Pool Funds Run Succesfull "

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("AddPoolFundContribution")]
        public JsonResult AddPoolFundContribution([FromBody] mContribution contribution)
        {
            var poolTrans = new MPoolTransRun();
            try
            {
                poolTrans.PoolTransAmount = contribution.poolTransAmount;
                poolTrans.PoolTransCustomer = contribution.PayerSCAccountDetails.payerAccountNumber;
                poolTrans.PoolTransDate = DateTime.Now;
                poolTrans.Poolname = contribution.poolname;
                db.MPoolTransRun.Add(poolTrans);
                db.SaveChanges();

                return Json(new
                {
                    res = "ok",
                    msg = "Contribution done successfully "

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpGet("getPoolFundMembers")]
        public JsonResult GetPoolFundMembers(string poolname)
        {
            var poolMembers = new List<MPoolMembers>();
            try
            {
                poolMembers = db.MPoolMembers.Where(i=>i.Poolname==poolname).ToList();

                return Json(new
                {
                    res = "ok",
                    msg = poolMembers

                });

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }

    }
}
