﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoyaltyPoints.Models
{
    public class mCustomer
    {
        public MLoyaltyModel Loyalty { get; set; }
        public int Points { get; set; }
    }
}
