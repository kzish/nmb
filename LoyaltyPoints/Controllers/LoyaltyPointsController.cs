﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoyaltyPoints.Models;

namespace LoyaltyPoints.Controllers
{
    [Route("scpayment/v1")]
    public class LoyaltyPointsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpPost("AddLoyaltyPointsRule")]
        public JsonResult AddLoyaltyPointsModel(MLoyaltyModel mLoyaltyModel)
        {
            try
            {


                db.MLoyaltyModel.Add(mLoyaltyModel);
                db.SaveChanges();
                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Loyalty Points Settings",

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpPost("AddLoyaltyRedeemRule")]
        public JsonResult AddRedeemPointsModel(MRedeemRule redeemRule)
        {
            try
            {


                db.MRedeemRule.Add(redeemRule);
                db.SaveChanges();

                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Added Redeem Points Settings",

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }

        [HttpPost("EarnedLoyaltyPoints")]
        public JsonResult LoyaltyPoints(string CustomerAccountNumber, decimal TransactionAmount, string Activity, string loyaltyPointsModelName)
        {
            var model = new MLoyaltyModel();
            var transLoyalty = new MTransLoyalty();
            var earnedLoyalty = new MTransLoyalty();
            decimal earnedpoints = 0;
            try
            {
                model = db.MLoyaltyModel.Where(i => i.LoyaltyModelName == loyaltyPointsModelName).FirstOrDefault();

                if (model == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Invalid loyalty Points Model Name",

                    });
                }
                transLoyalty = db.MTransLoyalty
                    .Where(i => i.TransCustomer == CustomerAccountNumber)
                    .OrderByDescending(i => i.ApprovalRef)
                    .FirstOrDefault();

                if (transLoyalty == null)
                {
                    transLoyalty.TransBalBf = 0;
                    transLoyalty.ApprovalRef = 0;
                }
                if (model.LoyaltyPointsMinValue < TransactionAmount)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Invalid loyalty Points Model Name",

                    });
                }
                else
                {
                    if (model.LoyaltyPointsMaxValue > TransactionAmount)
                    {
                        earnedpoints = (decimal)model.LoyaltyPointsMaxValue / (decimal)model.PointsPerAmount + (decimal)model.BonusPoints;
                    }
                    else
                    {
                        earnedpoints = TransactionAmount / (decimal)model.PointsPerAmount + (decimal)model.BonusPoints;
                    }
                }



                earnedLoyalty.ApprovalRef = (transLoyalty.ApprovalRef + 1);
                earnedLoyalty.LoyaltyModelName = loyaltyPointsModelName;
                earnedLoyalty.TransBalBf = transLoyalty.TransBalBf;
                earnedLoyalty.TransNumber = "";
                earnedLoyalty.TransPoints = (int)earnedpoints;
                earnedLoyalty.TransBalCf = earnedLoyalty.TransBalBf + earnedpoints;
                earnedLoyalty.TransDate = DateTime.Now;
                earnedLoyalty.TransActivity = Activity;
                earnedLoyalty.TransLoyaltyType = 0;
                earnedLoyalty.TransCustomer = CustomerAccountNumber;
                earnedLoyalty.TransAmount = TransactionAmount;

                db.MTransLoyalty.Add(earnedLoyalty);
                db.SaveChanges();
                return Json(new
                {
                    res = "ok",
                    msg = "You have earned " + earnedpoints + " and your Loyalty points balnace is " + earnedLoyalty.TransBalCf

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }
        [HttpGet("GetCustomerLoyaltyPoints")]
        public JsonResult CustomerLoyaltyPoints(string CustomerAccountNumber)
        {
            var model = new MLoyaltyModel();
            var transLoyalty = new MTransLoyalty();
            var earnedLoyalty = new MTransLoyalty();
            var custbalance = new customerbalance();
            try
            {

                transLoyalty = db.MTransLoyalty.Where(i => i.TransCustomer == CustomerAccountNumber)
                    .OrderByDescending(i => i.ApprovalRef).FirstOrDefault();

                if (transLoyalty == null)
                {
                    custbalance.customernumber = CustomerAccountNumber;
                    custbalance.balance = 0;

                }
                else
                {
                    custbalance.customernumber = CustomerAccountNumber;
                    custbalance.balance = 0;
                }



                return Json(new
                {
                    res = "ok",
                    msg = custbalance

                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }


        }

        [HttpPost("RedeemCustomerLoyaltyPoints")]
        public JsonResult RedeemCustomerLoyaltyPoints(string CustomerAccountNumber, decimal LoyaltyPoints, string RedeemNameRule)
        {
            var model = new MRedeemRule();
            var transLoyalty = new MTransLoyalty();
            var earnedLoyalty = new MTransLoyalty();
            decimal redeemedAmount = 0;
            try
            {

                model = db.MRedeemRule.Where(i => i.RedeemRuleName == RedeemNameRule).FirstOrDefault();

                if (model == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Invalid loyalty Points Model Name",

                    });
                }
                transLoyalty = db.MTransLoyalty.Where(i => i.TransCustomer == CustomerAccountNumber).FirstOrDefault();

                if (transLoyalty == null)
                {
                    transLoyalty.TransBalBf = 0;
                    transLoyalty.ApprovalRef = 0;
                }
                if (model.RedeemRuleMinPoints < LoyaltyPoints)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Points cannot redeemed they are below minimum redeemable points",

                    });
                }
                else
                {

                    redeemedAmount = LoyaltyPoints / (decimal)model.AmountPerPoint;

                }

                earnedLoyalty.ApprovalRef = (transLoyalty.ApprovalRef + 1);
                earnedLoyalty.LoyaltyModelName = RedeemNameRule;
                earnedLoyalty.TransBalBf = transLoyalty.TransBalBf;
                earnedLoyalty.TransNumber = "";
                earnedLoyalty.TransPoints = (int)LoyaltyPoints;
                earnedLoyalty.TransBalCf = earnedLoyalty.TransBalBf - LoyaltyPoints;
                earnedLoyalty.TransDate = DateTime.Now;
                earnedLoyalty.TransActivity = "Redeem";
                earnedLoyalty.TransLoyaltyType = 0;
                earnedLoyalty.TransCustomer = CustomerAccountNumber;
                earnedLoyalty.TransAmount = redeemedAmount;
                db.MTransLoyalty.Add(earnedLoyalty);
                return Json(new
                {
                    res = "ok",
                    msg = "You have redeemed " + LoyaltyPoints + " and your Loyalty points balnace is " + earnedLoyalty.TransBalCf

                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }
        }
        [HttpPost("TransferCustomerLoyaltyPoints")]
        public JsonResult TransferCustomerLoyaltyPoints(string FromCustomerAccountNumber, string ToCustomerAccountNumber, decimal LoyaltyPoints)
        {


            var TotransLoyalty = new MTransLoyalty();
            var FromtransLoyalty = new MTransLoyalty();
            var earnedLoyalty = new MTransLoyalty();
            var ToearnedLoyalty = new MTransLoyalty();
            decimal redeemedAmount = 0;

            try
            {
                FromtransLoyalty = db.MTransLoyalty
                    .Where(i => i.TransCustomer == FromCustomerAccountNumber)
                    .OrderByDescending(i => i.ApprovalRef)
                    .FirstOrDefault();

                TotransLoyalty = db.MTransLoyalty
                    .Where(i => i.TransCustomer == ToCustomerAccountNumber)
                    .OrderByDescending(i => i.ApprovalRef)
                    .FirstOrDefault();
              
                if (TotransLoyalty == null)
                {
                    TotransLoyalty.TransBalBf = 0;
                    TotransLoyalty.ApprovalRef = 0;
                }

                ToearnedLoyalty.ApprovalRef = TotransLoyalty.ApprovalRef + 1;
                ToearnedLoyalty.LoyaltyModelName = "Transfer";
                ToearnedLoyalty.TransBalBf = TotransLoyalty.TransBalBf;
                ToearnedLoyalty.TransNumber = "";
                ToearnedLoyalty.TransPoints = (int)LoyaltyPoints;
                ToearnedLoyalty.TransBalCf = ToearnedLoyalty.TransBalBf + LoyaltyPoints;
                ToearnedLoyalty.TransDate = DateTime.Now;
                ToearnedLoyalty.TransActivity = "Transfer";
                ToearnedLoyalty.TransLoyaltyType = 0;
                ToearnedLoyalty.TransCustomer = ToCustomerAccountNumber;
                ToearnedLoyalty.TransAmount = 0;

                db.MTransLoyalty.Add(ToearnedLoyalty);
                db.SaveChanges();
                if (FromtransLoyalty == null)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "The customer has no points to transfer",

                    });
                }
                if (FromtransLoyalty.TransBalBf < LoyaltyPoints)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "Points cannot transfer points above your balance",

                    });
                }
                else
                {

                    earnedLoyalty.ApprovalRef = (FromtransLoyalty.ApprovalRef + 1);
                    earnedLoyalty.LoyaltyModelName = "Transfer";
                    earnedLoyalty.TransBalBf = FromtransLoyalty.TransBalBf;
                    earnedLoyalty.TransNumber = "";
                    earnedLoyalty.TransPoints = (int)LoyaltyPoints;
                    earnedLoyalty.TransBalCf = earnedLoyalty.TransBalBf - LoyaltyPoints;
                    earnedLoyalty.TransDate = DateTime.Now;
                    earnedLoyalty.TransActivity = "Redeem";
                    earnedLoyalty.TransLoyaltyType = 0;
                    earnedLoyalty.TransCustomer = FromCustomerAccountNumber;
                    earnedLoyalty.TransAmount = redeemedAmount;

                    db.MTransLoyalty.Add(earnedLoyalty);
                    db.SaveChanges();

                }




                return Json(new
                {
                    res = "ok",
                    msg = "You have Transfered " + LoyaltyPoints + " and your Loyalty points balnace is " + earnedLoyalty.TransBalCf

                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message

                });
            }
        }
    }
}
