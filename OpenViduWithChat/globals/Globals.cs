﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

public class Globals
{
    private static string logging_base_url = "http://localhost:2004";
    public static string activity_logger_url = logging_base_url + "/ActivityLogger/LogActivity";
    public static string transaction_logger_url = logging_base_url + "/TransactionLogger/LogTranaction";
    public static string error_logger_url = logging_base_url + "/ErrorLogger/LogError";

    /***** common ****/
    public static string log_file = "C:\\rubiem\\stanchart_simba\\stanchart_simba_logs.txt";
    //config file path
    public static string simba_config = @"c:\rubiem\simba\simba_config.json";//this is keyvalue pairs json file with configuration

    //public static string open_vidu_server = "https://localhost:4443";
    public static string open_vidu_server = "https://ec2-46-51-145-83.eu-west-1.compute.amazonaws.com:4443";



    //log data to a file
    public static void log_data_to_file(string source = "", object data = null)
    {
        try
        {
            dynamic obj = new JObject();
            obj.source = source;
            obj.msg = data.ToString();
            var logdata = JsonConvert.SerializeObject(obj);
            System.IO.File.AppendAllText(log_file, logdata + Environment.NewLine);
        }
        catch (Exception ex)
        {

        }
    }


}
