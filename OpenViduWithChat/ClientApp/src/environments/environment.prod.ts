export const environment = {
  production: true,
  openvidu_url: 'https://ec2-46-51-145-83.eu-west-1.compute.amazonaws.com:4443',
  openvidu_secret: 'MY_SECRET'
};
