﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace OpenVidu.Controllers
{
    /// <summary>
    /// displays the browser to the client
    /// </summary>
    [Route("RubiemVideoChat")]
    public class BrowserController : Controller
    {
        //todo: get the permanant certificate url
        private string accept_certificate_url = $"{Globals.open_vidu_server}/Accept-certificate";

        /// <summary>
        /// return the url for the certificate
        /// </summary>
        /// <returns></returns>
        [HttpGet("AcceptCertificateURL")]
        public string AcceptCertificateURL()
        {
            return accept_certificate_url;
        }

        /// <summary>
        /// force the client browser to accept the certificate
        /// </summary>
        /// <returns></returns>
        [HttpGet("AcceptCertificate")]
        public IActionResult AcceptCertificate()
        {
            return RedirectPermanent(accept_certificate_url);
        }

    }
}
