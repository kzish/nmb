﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OpenVidu.Models;
using System;
using System.Net.Http;
using System.Text;

/// <summary>
/// open vidu rest api
/// </summary>
namespace OpenVidu.Controllers
{
    //rest api url
    //https://openvidu.io/docs/reference-docs/REST-API/#post-apirecordingsstart


    [Route("RubiemVideoChat")]
    public class RestApiController : Controller
    {

        private string base_path = "OpenVidu.Controllers.RestApiController";
        private string app_secret = "MY_SECRET";
        


        /// <summary>
        /// create a  new session, session_id is the name of the session/chatroom/videoconference/videocall
        /// this method is called internally by create_token
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        //[HttpPost("create_session")]
        private JsonResult create_session(string session_id)
        {
            try
            {
                //validate
                if(string.IsNullOrEmpty(session_id))
                {
                    return Json(new {
                        res="err",
                        msg= "session_id param is missing"
                    });
                }

                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                //body of request
                var json = new JObject();
                json.Add("mediaMode","ROUTED");
                json.Add("recordingMode","MANUAL");
                json.Add("customSessionId",session_id);
                json.Add("defaultOutputMode","COMPOSED");
                json.Add("defaultRecordingLayout","BEST_FIT");
                //json.defaultCustomLayout = "CUSTOM_LAYOUT";

                //make request
                var response = client.PostAsJsonAsync(Globals.open_vidu_server + "/api/sessions", json)
                    .Result.Content.ReadAsStringAsync().Result;

                
                //resposnse
                return Json(new
                {
                    res = "ok",
                    msg = string.IsNullOrEmpty(response) ? null : JObject.Parse(response)
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".create_session", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }

        


        /// <summary>
        /// create a  new token based on the session 
        /// creates a web socket token the user can use to connect to the video chat
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        [HttpPost("create_token")]
        public JsonResult create_token(string session_id)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(session_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "session_id param is missing"
                    });
                }

                //first call the create_session method and ensure you get a "res="ok"" response
                dynamic session = create_session(session_id).Value;
                if(session.res=="err")
                {
                    return Json(new {
                        res="err",
                        msg=session.msg
                    });
                }



                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                //body of request
                var json = new JObject();
                json.Add("session", session_id);
                json.Add("role", eRole.SUBSCRIBER.ToString());//defaulting to subscriber
                //json.Add("data", session_id);
                //json.Add("kurentoOptions", "COMPOSED");

                //make request
                var response = client.PostAsJsonAsync(Globals.open_vidu_server + "/api/tokens", json)
                    .Result.Content.ReadAsStringAsync().Result;

                //parse the json response
                dynamic jobj = JObject.Parse(response);
                //get the web socket token from the json object and convert it to base 64 string
                var token_base_64 =  Convert.ToBase64String(Encoding.UTF8.GetBytes((string)jobj.token));
                //construct the complete url for the client to connect
                //var complete_web_url = $"http://localhost:4200/#/{session_id}?wss_token={token_base_64}";
                var complete_web_url = $"https://{Request.Host.Host}:4200/#/{session_id}?wss_token={token_base_64}";
                //var complete_web_url = $"{Request.Scheme}://{Request.Host.Host}:{Request.Host.Port}/#/{session_id}?wss_token={token_base_64}";
                //resposnse
                return Json(new
                {
                    res = "ok",
                    //msg1 = string.IsNullOrEmpty(response) ? null : JObject.Parse(response),
                    msg = complete_web_url
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".create_token", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }




        /// <summary>
        /// get_session_info based on the session 
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        [HttpGet("get_session_info")]
        public JsonResult get_session_info(string session_id)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(session_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "session_id param is missing"
                    });
                }

                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");

               

                //make request
                var response = client.GetAsync(Globals.open_vidu_server + $"/api/sessions/{session_id}")
                    .Result.Content.ReadAsStringAsync().Result;


                //resposnse
                return Json(new
                {
                    res = "ok",
                    msg = string.IsNullOrEmpty(response) ? null : JObject.Parse(response),
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".get_session_info", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }



        /// <summary>
        /// get_all_session_info based on the session 
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        [HttpGet("get_all_session_info")]
        public JsonResult get_all_session_info()
        {
            try
            {

                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);



                //make request
                var response = client.GetAsync(Globals.open_vidu_server + "/api/sessions")
                    .Result.Content.ReadAsStringAsync().Result;


                //resposnse
                return Json(new
                {
                    res = "ok",
                    msg = string.IsNullOrEmpty(response) ? null : JObject.Parse(response),
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".get_all_session_info", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }


        /// <summary>
        /// close_session based on the session ie when participant leaves the room
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        [HttpGet("close_session")]
        public JsonResult close_session(string session_id)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(session_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "session_id param is missing"
                    });
                }

                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");



                //make request
                var response = client.DeleteAsync(Globals.open_vidu_server + $"/api/sessions/{session_id}")
                    .Result.StatusCode.ToString();

                var formatted_response = "Unknown Error";
                if (response == "204") formatted_response = "The session has been successfully closed";
                if (response == "404") formatted_response = "No session exists for the passed SESSION_ID";

                //resposnse
                return Json(new
                {
                    res = "ok",
                    msg = response,
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".close_session", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }



        /// <summary>
        ///  the user has been forcefully evicted from the session
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        [HttpGet("close_session_for_user")]
        public JsonResult close_session_for_user(string session_id,string connection_id)
        {
            try
            {
                //validate
                if (string.IsNullOrEmpty(session_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "session_id param is missing"
                    });
                }

                if (string.IsNullOrEmpty(connection_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "connection_id param is missing"
                    });
                }

                //todo: validate the certificate https://khalidabuhakmeh.com/validating-ssl-certificates-with-dotnet-servicepointmanager
                //client handler to bypass the certificate
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                //client and headers
                var client = new HttpClient(clientHandler);
                var encoded_secret = Convert.ToBase64String(Encoding.ASCII.GetBytes($"OPENVIDUAPP:{app_secret}"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded_secret);
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");



                //make request
                var response = client.DeleteAsync(Globals.open_vidu_server + $"/api/sessions/{session_id}/connection/{connection_id}")
                    .Result.StatusCode.ToString();

              
                //resposnse
                return Json(new
                {
                    res = "ok",
                    msg = response,
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(base_path + ".close_session_for_user", ex);
                return Json(new { res = "err", msg = ex.Message });
            }
        }














    }
}

