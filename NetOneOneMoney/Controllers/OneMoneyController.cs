﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;

/// <summary>
/// netone one money api
/// </summary>
namespace NetOneOneMoney.Controllers
{

    [Route("OneMoney")]
    public class OneMoneyController : Controller
    {

        //end point url
        private string base_url = "http://197.211.237.141:12004/one-api/api";

        private IHttpContextAccessor _accessor;

        public OneMoneyController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        [HttpGet("Initialize")]
        public JsonResult Initialize(string msisdn)
        {
            try
            {

                if (string.IsNullOrEmpty(msisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "msisdn number is missing"
                    });
                }


                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("language", "en");
                client.DefaultRequestHeaders.Add("uuid", Guid.NewGuid().ToString());
                client.DefaultRequestHeaders.Add("token1", "4321");

                var response = client.GetAsync(base_url + $"/initialize/{msisdn}").Result;
                var response_header = response.Headers.GetValues("token").FirstOrDefault();
                var response_body = response.Content.ReadAsStringAsync().Result;


                return Json(new
                {
                    res = "ok",
                    msg = Json(new
                    {
                        token = response_header,
                        body = JObject.Parse(response_body)
                    }).Value
                });


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NetOneOneMoney.Controllers.OneMoneyController.Initialize", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Check_Balance")]
        public JsonResult Check_Balance(string msisdn, string token)
        {
            try
            {

                if (string.IsNullOrEmpty(msisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "msisdn number is missing"
                    });
                }

                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }


                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("language", "en");
                client.DefaultRequestHeaders.Add("uuid", Guid.NewGuid().ToString());
                client.DefaultRequestHeaders.Add("token1", "4321");
                client.DefaultRequestHeaders.Add("token", token);

                var response = client.GetAsync(base_url + $"/resources/balances/{msisdn}").Result.Content.ReadAsStringAsync().Result;

                return Json(new
                {
                    res = "ok",
                    msg = JObject.Parse(response)
                });


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NetOneOneMoney.Controllers.OneMoneyController.Check_Balance", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceMsisdn"></param>
        /// <param name="rechargeMsisdn"></param>
        /// <param name="amount"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Buy_Airtime")]
        public JsonResult Buy_Airtime(string sourceMsisdn, string rechargeMsisdn, double amount, string token)
        {
            try
            {

                if (string.IsNullOrEmpty(sourceMsisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "sourceMsisdn number is missing"
                    });
                }

                if (string.IsNullOrEmpty(rechargeMsisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "rechargeMsisdn is missing"
                    });
                }

                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                if (amount <= 0)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "amount is invalid"
                    });
                }


                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("language", "en");
                client.DefaultRequestHeaders.Add("uuid", Guid.NewGuid().ToString());
                client.DefaultRequestHeaders.Add("token1", "4321");
                client.DefaultRequestHeaders.Add("token", token);
                //Convert.ToBase64String( Encoding.UTF8.GetBytes(token) )

                var response = client.GetAsync(base_url + $"/resources/airtime/{sourceMsisdn}/{rechargeMsisdn}/{amount}").Result.Content.ReadAsStringAsync().Result;

                return Json(new
                {
                    res = "ok",
                    msg = JObject.Parse(response)
                });


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NetOneOneMoney.Controllers.OneMoneyController.Buy_Airtime", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceMsisdn"></param>
        /// <param name="destinationMsisdn"></param>
        /// <param name="amount"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Transfer_Funds")]
        public JsonResult Transfer_Funds(string sourceMsisdn, string destinationMsisdn, double amount, string token)
        {
            try
            {

                if (string.IsNullOrEmpty(sourceMsisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "sourceMsisdn number is missing"
                    });
                }

                if (string.IsNullOrEmpty(destinationMsisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "destinationMsisdn is missing"
                    });
                }

                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                if (amount <= 0)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "amount is invalid"
                    });
                }


                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("language", "en");
                client.DefaultRequestHeaders.Add("uuid", Guid.NewGuid().ToString());
                client.DefaultRequestHeaders.Add("token1", "4321");
                client.DefaultRequestHeaders.Add("token", token);

                var response = client.GetAsync(base_url + $"/resources/transfer/{amount}/from/{sourceMsisdn}/to/{destinationMsisdn}").Result.Content.ReadAsStringAsync().Result;

                return Json(new
                {
                    res = "ok",
                    msg = (response)
                });


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NetOneOneMoney.Controllers.OneMoneyController.Transfer_Funds", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Check_Bank")]
        public JsonResult Check_Bank(string msisdn, string token)
        {
            try
            {
                if (string.IsNullOrEmpty(msisdn))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "msisdn is missing"
                    });
                }

                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("language", "en");
                client.DefaultRequestHeaders.Add("uuid", Guid.NewGuid().ToString());
                client.DefaultRequestHeaders.Add("token1", "4321");
                client.DefaultRequestHeaders.Add("token", token);

                var response = client.GetAsync(base_url + $"/resources/bankingservices/query/{msisdn}").Result.Content.ReadAsStringAsync().Result;

                return Json(new
                {
                    res = "ok",
                    msg = (response)
                });


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NetOneOneMoney.Controllers.OneMoneyController.Check_Bank", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }











    }
}
