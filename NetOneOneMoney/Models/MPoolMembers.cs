﻿using System;
using System.Collections.Generic;

namespace NetOneOneMoney.Models
{
    public partial class MPoolMembers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public int? Position { get; set; }
        public bool? Administrator { get; set; }
        public string Poolname { get; set; }
    }
}
