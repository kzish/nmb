USE [master]
GO
/****** Object:  Database [stanchart_simba]    Script Date: 11/26/2019 5:00:34 PM ******/
CREATE DATABASE [stanchart_simba]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'stanchart_simba', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\stanchart_simba.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'stanchart_simba_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\stanchart_simba_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [stanchart_simba] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [stanchart_simba].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [stanchart_simba] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [stanchart_simba] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [stanchart_simba] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [stanchart_simba] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [stanchart_simba] SET ARITHABORT OFF 
GO
ALTER DATABASE [stanchart_simba] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [stanchart_simba] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [stanchart_simba] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [stanchart_simba] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [stanchart_simba] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [stanchart_simba] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [stanchart_simba] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [stanchart_simba] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [stanchart_simba] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [stanchart_simba] SET  DISABLE_BROKER 
GO
ALTER DATABASE [stanchart_simba] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [stanchart_simba] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [stanchart_simba] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [stanchart_simba] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [stanchart_simba] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [stanchart_simba] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [stanchart_simba] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [stanchart_simba] SET RECOVERY FULL 
GO
ALTER DATABASE [stanchart_simba] SET  MULTI_USER 
GO
ALTER DATABASE [stanchart_simba] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [stanchart_simba] SET DB_CHAINING OFF 
GO
ALTER DATABASE [stanchart_simba] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [stanchart_simba] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [stanchart_simba] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'stanchart_simba', N'ON'
GO
USE [stanchart_simba]
GO
/****** Object:  User [telone]    Script Date: 11/26/2019 5:00:34 PM ******/
CREATE USER [telone] FOR LOGIN [telone] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[Data] [text] NULL,
	[Date] [datetime] NULL,
	[LogType] [int] NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mActivityLog]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mActivityLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[_id] [varchar](50) NULL,
	[date] [datetime] NULL,
	[username] [varchar](max) NULL,
	[user_device] [varchar](max) NULL,
	[user_ip_address] [varchar](50) NULL,
	[user_activity] [varchar](50) NULL,
	[data] [varchar](max) NULL,
 CONSTRAINT [PK_mActivityLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mAddress]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillerCode] [nvarchar](50) NULL,
	[AptNum] [nvarchar](50) NULL,
	[Street] [nvarchar](50) NULL,
	[Extention] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
 CONSTRAINT [PK_mAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mBankAccount]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mBankAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillerCode] [nvarchar](50) NULL,
	[AccountName] [nvarchar](50) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Bank] [int] NULL,
	[CurrencyCode] [int] NULL,
 CONSTRAINT [PK_mBankAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mBillerInfo]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mBillerInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillerName] [nvarchar](50) NULL,
	[ContactPerson] [nvarchar](50) NULL,
	[ContactPhone] [nvarchar](50) NULL,
	[ContactEmail] [nvarchar](50) NULL,
	[BillerType] [int] NULL,
	[BillerCategory] [int] NULL,
	[hasProducts] [bit] NULL,
	[hasAccounts] [bit] NULL,
	[hasAdditionalData] [bit] NULL,
	[RequestEndPoint] [nvarchar](max) NULL,
	[CredentialsUsername] [nvarchar](50) NOT NULL,
	[CredentialsPassword] [nvarchar](50) NULL,
	[ConnectionType] [nvarchar](50) NULL,
	[PaymentCycle] [int] NULL,
	[BillerCode] [nvarchar](50) NULL,
	[BillerStatus] [bit] NULL,
	[Enabler] [varchar](50) NULL,
	[hasCart] [bit] NULL,
	[ClientIndentifier] [int] NULL,
 CONSTRAINT [PK_mBillerInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mBillerProduct]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mBillerProduct](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[price] [decimal](18, 2) NULL,
	[image_url] [varchar](max) NULL,
	[available] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mEnquiryAnswer]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mEnquiryAnswer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questionid] [int] NULL,
	[question] [varchar](max) NULL,
	[answer] [varchar](max) NULL,
	[service_enquiry_id] [int] NULL,
 CONSTRAINT [PK_mEnquiryAnswer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mEnquiryQuestions]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mEnquiryQuestions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question] [nvarchar](max) NULL,
	[billercode] [nvarchar](50) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_mInquiryQuestions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mErrorLog]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mErrorLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[_id] [varchar](50) NULL,
	[error_message] [varchar](max) NULL,
	[full_error_message] [varchar](max) NULL,
	[activity_id] [varchar](50) NULL,
 CONSTRAINT [PK_mErrorLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mFeedBack]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mFeedBack](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[feedbacknumber] [varchar](50) NOT NULL,
	[billercode] [varchar](50) NULL,
	[feedback] [varchar](max) NULL,
	[noted] [int] NULL,
	[customername] [varchar](50) NULL,
	[customerphone] [varchar](50) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_mFeedBack] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mLoyaltyModel]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mLoyaltyModel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[loyaltyModelName] [nvarchar](50) NULL,
	[loyaltyPointsCriteria] [nvarchar](50) NULL,
	[loyaltyPointsMinValue] [numeric](18, 0) NULL,
	[loyaltyPointsMaxValue] [numeric](18, 0) NULL,
	[PointsPerAmount] [numeric](18, 0) NULL,
	[BonusPoints] [numeric](18, 0) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mNotification]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mNotification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[BillerCode] [varchar](50) NULL,
	[message] [varchar](max) NULL,
	[date] [datetime] NULL,
	[isRead] [bit] NULL,
 CONSTRAINT [PK_mNotification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mPayments]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mPayments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillerCode] [nvarchar](50) NULL,
	[PaymentDate] [nvarchar](50) NULL,
	[PaidAmount] [numeric](18, 0) NULL,
	[TrackingID] [nvarchar](50) NULL,
	[BatchNo] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mPool]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mPool](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[poolname] [nvarchar](50) NULL,
	[pooldiscription] [nvarchar](50) NULL,
	[poolstartdate] [nvarchar](50) NULL,
	[poolenddate] [nvarchar](50) NULL,
	[active] [bit] NULL,
	[poolcontributionamount] [numeric](18, 0) NULL,
	[contributioncycle] [bit] NULL,
	[terms] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mPoolMembers]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mPoolMembers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[accountNumber] [nvarchar](50) NULL,
	[position] [int] NULL,
	[administrator] [bit] NULL,
	[poolname] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mPoolTrans]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mPoolTrans](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pooltransnumber] [nvarchar](50) NULL,
	[poolTransDate] [nvarchar](50) NULL,
	[poolTransAmount] [numeric](18, 0) NULL,
	[poolTransCustomer] [nvarchar](50) NULL,
	[approvalRef] [int] NULL,
	[poolname] [nvarchar](50) NULL,
	[trackingid] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mPoolTransRun]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mPoolTransRun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pooltransnumber] [nvarchar](50) NULL,
	[poolname] [nvarchar](50) NULL,
	[poolTransDate] [nvarchar](50) NULL,
	[poolTransAmount] [nvarchar](50) NULL,
	[nextPoolRunDate] [nvarchar](50) NULL,
	[BenefitedPosition] [int] NULL,
	[NextBenefitingPosition] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mProductOffline]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mProductOffline](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productcode] [nvarchar](50) NULL,
	[price] [numeric](18, 0) NULL,
	[description] [nvarchar](max) NULL,
	[image_url] [nvarchar](max) NULL,
	[available] [bit] NULL,
	[name] [nvarchar](50) NULL,
	[disabled] [bit] NULL,
	[billercode] [nvarchar](50) NULL,
 CONSTRAINT [PK_mProductOffline] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mReceipts]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mReceipts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionNumber] [nvarchar](50) NOT NULL,
	[PayerName] [nvarchar](50) NULL,
	[PayerPhoneNumber] [nvarchar](50) NULL,
	[PayerAccount] [nvarchar](50) NULL,
	[AmountPaid] [numeric](18, 0) NULL,
	[Posted] [int] NULL,
	[TransactionDate] [nvarchar](50) NULL,
	[TrackingId] [nvarchar](50) NULL,
	[Notes] [nvarchar](50) NULL,
	[PaidToBiller] [bit] NULL,
	[PaymentToBillerRef] [nvarchar](50) NULL,
	[BillerCode] [nvarchar](50) NULL,
	[BillerProductID] [nvarchar](50) NULL,
	[BillerProductQuantity] [numeric](18, 0) NULL,
	[OrderID] [nvarchar](50) NULL,
	[ReversedByBiller] [bit] NULL,
 CONSTRAINT [PK_mReceipts] PRIMARY KEY CLUSTERED 
(
	[TransactionNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mRedeemRule]    Script Date: 11/26/2019 5:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mRedeemRule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RedeemRuleName] [nvarchar](50) NULL,
	[RedeemRuleCriteria] [nvarchar](50) NULL,
	[RedeemRuleMinPoints] [numeric](18, 0) NULL,
	[AmountPerPoint] [numeric](18, 0) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mReferenceNumber]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mReferenceNumber](
	[reference] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mServiceEnquiry]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mServiceEnquiry](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[payerName] [varchar](50) NULL,
	[phoneNumber] [varchar](50) NULL,
	[billercode] [varchar](50) NULL,
	[date] [datetime] NULL,
	[isRead] [bit] NULL,
 CONSTRAINT [PK_mServiceInquiry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mTransAction]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mTransAction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[_id] [varchar](50) NULL,
	[date] [datetime] NULL,
	[fromAccountNumber] [varchar](50) NULL,
	[toAccountNumber] [varchar](50) NULL,
	[fromCurrencyCode] [varchar](50) NULL,
	[toCurrencyCode] [varchar](50) NULL,
	[messageSubType] [varchar](50) NULL,
	[transactionCurrency] [varchar](50) NULL,
	[transactionAmount] [varchar](50) NULL,
	[fromBankCode] [varchar](50) NULL,
	[toBankCode] [varchar](50) NULL,
	[costRate] [varchar](50) NULL,
	[conversionRate] [varchar](50) NULL,
	[uniqueReferenceIdentifier] [varchar](50) NULL,
	[transactionBranch] [varchar](50) NULL,
	[transactionPostingDate] [varchar](50) NULL,
	[sourceSystemName] [varchar](50) NULL,
	[transactionType] [varchar](50) NULL,
	[lcyEquivalent] [varchar](50) NULL,
	[countrycode] [varchar](50) NULL,
	[trackingId] [varchar](50) NULL,
	[billerCode] [varchar](50) NULL,
	[ApiCallType] [int] NULL,
 CONSTRAINT [PK_mTransAction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mTransLoyalty]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mTransLoyalty](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TransNumber] [nvarchar](50) NULL,
	[TransDate] [nvarchar](50) NULL,
	[TransPoints] [nvarchar](50) NULL,
	[TransCustomer] [nvarchar](50) NULL,
	[TransBalBf] [numeric](18, 0) NULL,
	[TransBalCf] [numeric](18, 0) NULL,
	[TransLoyaltyType] [bit] NULL,
	[TransActivity] [nvarchar](50) NULL,
	[loyaltyModelName] [nvarchar](50) NULL,
	[approvalRef] [int] NOT NULL,
	[TransAmount] [numeric](18, 0) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Providers]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Providers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ZSSProviderName] [nvarchar](max) NULL,
	[ProviderDescription] [nvarchar](max) NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_Providers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/26/2019 5:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Role] [varchar](10) NULL,
	[Token] [nvarchar](max) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Logs] ON 

INSERT [dbo].[Logs] ([Id], [Title], [Data], [Date], [LogType]) VALUES (33, N'request', N'{"payerdetails":{"payerAccountReference":"8700803329300","payerCardReference":"40000","payerMsisdn":"string","payerName":"Bruce","payerReference":"xxx11"},"billAccount":"4117068963","billAmount":6500,"provider":"DSTV","product":"BOUQUET","billDatas":[{"name":"DstvProduct1","value":"PRM"},{"name":"DstvProduct2","value":"NONE"},{"name":"DstvMonths","value":"1"}],"participantRef":"string114","RequestType":1}', CAST(N'2019-06-12 11:26:02.437' AS DateTime), 1)
INSERT [dbo].[Logs] ([Id], [Title], [Data], [Date], [LogType]) VALUES (34, N'response', N'success:True ,statusSode:0 ,customermsg:Payment was made successfully -- {"additionalBillData":{"billData":[{"name":"DstvCustomerName","value":"Mr A ROSCOE"},{"name":"DstvCustomerId","value":"4117068963"},{"name":"ASResponseCode","value":"00"},{"name":"ASResponseDesc","value":"No accountMapping found for participantId=16, tranType=BILL_PAYMENT"},{"name":"ProviderDisplayName","value":"DSTV"}]},"participant":"406896","participantReference":"string114","portalDescription":"PORTAL APPROVED","portalReference":"19439898","portalResponse":"000","provider":"DSTV","providerDescription":null,"providerReference":null,"providerResponse":"000","receiptData":null,"success":true}', CAST(N'2019-06-12 11:26:05.710' AS DateTime), 2)
INSERT [dbo].[Logs] ([Id], [Title], [Data], [Date], [LogType]) VALUES (35, N'request', N'{"payerdetails":{"payerAccountReference":"8700803329300","payerCardReference":"40000","payerMsisdn":"string","payerName":"Bruce","payerReference":"xxx11"},"billAccount":"4117068963","billAmount":6500,"provider":"DSTV","product":"BOUQUET","billDatas":[{"name":"DstvProduct1","value":"PRM"},{"name":"DstvProduct2","value":"NONE"},{"name":"DstvMonths","value":"1"}],"participantRef":"string114","RequestType":0}', CAST(N'2019-06-12 11:26:59.460' AS DateTime), 1)
INSERT [dbo].[Logs] ([Id], [Title], [Data], [Date], [LogType]) VALUES (36, N'response', N'success:True ,statusSode:0 ,customermsg:Payment was made successfully -- {"additionalBillData":{"billData":[{"name":"ProviderDisplayName","value":"DSTV"},{"name":"ASResponseCode","value":"00"},{"name":"ASResponseDesc","value":"No accountMapping found for participantId=16, tranType=BILL_PAYMENT"}]},"participant":"406896","participantReference":"string114","portalDescription":"TRANSACTION SUCCESSFUL","portalReference":"19439899","portalResponse":"000","provider":"DSTV","providerDescription":null,"providerReference":"3389014","providerResponse":"000","receiptData":"3389014","success":true}', CAST(N'2019-06-12 11:27:14.510' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Logs] OFF
SET IDENTITY_INSERT [dbo].[mAddress] ON 

INSERT [dbo].[mAddress] ([Id], [BillerCode], [AptNum], [Street], [Extention], [City]) VALUES (1, N'ac', N'1', N'joina city', N'1', N'harare')
SET IDENTITY_INSERT [dbo].[mAddress] OFF
SET IDENTITY_INSERT [dbo].[mBankAccount] ON 

INSERT [dbo].[mBankAccount] ([Id], [BillerCode], [AccountName], [AccountNumber], [Bank], [CurrencyCode]) VALUES (1, N'ac', N'accc', N'1122333', 11, 4)
SET IDENTITY_INSERT [dbo].[mBankAccount] OFF
SET IDENTITY_INSERT [dbo].[mBillerInfo] ON 

INSERT [dbo].[mBillerInfo] ([Id], [BillerName], [ContactPerson], [ContactPhone], [ContactEmail], [BillerType], [BillerCategory], [hasProducts], [hasAccounts], [hasAdditionalData], [RequestEndPoint], [CredentialsUsername], [CredentialsPassword], [ConnectionType], [PaymentCycle], [BillerCode], [BillerStatus], [Enabler], [hasCart], [ClientIndentifier]) VALUES (1, N'ac', N'boss man', N'acwww', N'a@gmail.com', 1, 1, 1, 1, 1, N'', N'1', N'1', N'1', 1, N'ac', 1, NULL, NULL, NULL)
INSERT [dbo].[mBillerInfo] ([Id], [BillerName], [ContactPerson], [ContactPhone], [ContactEmail], [BillerType], [BillerCategory], [hasProducts], [hasAccounts], [hasAdditionalData], [RequestEndPoint], [CredentialsUsername], [CredentialsPassword], [ConnectionType], [PaymentCycle], [BillerCode], [BillerStatus], [Enabler], [hasCart], [ClientIndentifier]) VALUES (2, N'ac', N'boss man', N'acwww', N'a@gmail.com', 1, 1, 1, 1, 1, N'', N'1', N'1', N'1', 1, N'ac', 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[mBillerInfo] OFF
SET IDENTITY_INSERT [dbo].[mBillerProduct] ON 

INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (1, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (2, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (3, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (4, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (5, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (6, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (7, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
INSERT [dbo].[mBillerProduct] ([id], [name], [description], [price], [image_url], [available]) VALUES (8, N'q', N'q', CAST(12.00 AS Decimal(18, 2)), N'11', 1)
SET IDENTITY_INSERT [dbo].[mBillerProduct] OFF
SET IDENTITY_INSERT [dbo].[mEnquiryAnswer] ON 

INSERT [dbo].[mEnquiryAnswer] ([id], [questionid], [question], [answer], [service_enquiry_id]) VALUES (8, 1, N'wwwweewmdewdoeidmemdoemr', N'string', 7)
INSERT [dbo].[mEnquiryAnswer] ([id], [questionid], [question], [answer], [service_enquiry_id]) VALUES (9, 2, N'hassab', N'string', 7)
INSERT [dbo].[mEnquiryAnswer] ([id], [questionid], [question], [answer], [service_enquiry_id]) VALUES (10, 4, N'q', N'string', 7)
SET IDENTITY_INSERT [dbo].[mEnquiryAnswer] OFF
SET IDENTITY_INSERT [dbo].[mEnquiryQuestions] ON 

INSERT [dbo].[mEnquiryQuestions] ([id], [question], [billercode], [status]) VALUES (1, N'wwwweewmdewdoeidmemdoemr', N'ac', 1)
INSERT [dbo].[mEnquiryQuestions] ([id], [question], [billercode], [status]) VALUES (2, N'hassab', N'ac', 0)
INSERT [dbo].[mEnquiryQuestions] ([id], [question], [billercode], [status]) VALUES (4, N'q', N'ac', 0)
SET IDENTITY_INSERT [dbo].[mEnquiryQuestions] OFF
SET IDENTITY_INSERT [dbo].[mFeedBack] ON 

INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (3, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (4, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (5, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (6, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (7, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (8, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (9, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (10, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (11, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (12, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (13, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (14, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (15, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (16, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (17, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (18, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (19, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (20, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (21, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (22, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (23, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (24, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
INSERT [dbo].[mFeedBack] ([id], [feedbacknumber], [billercode], [feedback], [noted], [customername], [customerphone], [date]) VALUES (25, N'ac', N'ac', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, N'ac', N'ac', CAST(N'2019-11-12 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[mFeedBack] OFF
SET IDENTITY_INSERT [dbo].[mNotification] ON 

INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (6, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (7, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (8, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (9, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (10, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (11, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (12, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (13, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (14, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (15, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (16, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (17, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[mNotification] ([id], [BillerCode], [message], [date], [isRead]) VALUES (18, N'ac', N'a message', CAST(N'2019-11-23 00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[mNotification] OFF
SET IDENTITY_INSERT [dbo].[mPayments] ON 

INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (1, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (2, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (3, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (4, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (5, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (6, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (7, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
INSERT [dbo].[mPayments] ([Id], [BillerCode], [PaymentDate], [PaidAmount], [TrackingID], [BatchNo]) VALUES (8, N'ac', N'2019-11-12', CAST(1000 AS Numeric(18, 0)), N'1234567', N'1')
SET IDENTITY_INSERT [dbo].[mPayments] OFF
SET IDENTITY_INSERT [dbo].[mProductOffline] ON 

INSERT [dbo].[mProductOffline] ([id], [productcode], [price], [description], [image_url], [available], [name], [disabled], [billercode]) VALUES (17, N'123', CAST(123 AS Numeric(18, 0)), N'cjhe ckher kc erlkcercsan', NULL, 0, N'a product again edited', 0, N'ac')
INSERT [dbo].[mProductOffline] ([id], [productcode], [price], [description], [image_url], [available], [name], [disabled], [billercode]) VALUES (18, N'1', CAST(123 AS Numeric(18, 0)), N'wdws', NULL, 1, N'dededd', 0, N'ac')
INSERT [dbo].[mProductOffline] ([id], [productcode], [price], [description], [image_url], [available], [name], [disabled], [billercode]) VALUES (19, N'1', CAST(1 AS Numeric(18, 0)), N'1', NULL, 1, N'1', 0, N'ac')
SET IDENTITY_INSERT [dbo].[mProductOffline] OFF
SET IDENTITY_INSERT [dbo].[mReceipts] ON 

INSERT [dbo].[mReceipts] ([Id], [TransactionNumber], [PayerName], [PayerPhoneNumber], [PayerAccount], [AmountPaid], [Posted], [TransactionDate], [TrackingId], [Notes], [PaidToBiller], [PaymentToBillerRef], [BillerCode], [BillerProductID], [BillerProductQuantity], [OrderID], [ReversedByBiller]) VALUES (1, N'1', N'a', N'11', N'1', CAST(1 AS Numeric(18, 0)), 1, N'2019-01-01', N'1', N'1', 1, N'1', N'ac', N'1', CAST(1 AS Numeric(18, 0)), N'1', 1)
INSERT [dbo].[mReceipts] ([Id], [TransactionNumber], [PayerName], [PayerPhoneNumber], [PayerAccount], [AmountPaid], [Posted], [TransactionDate], [TrackingId], [Notes], [PaidToBiller], [PaymentToBillerRef], [BillerCode], [BillerProductID], [BillerProductQuantity], [OrderID], [ReversedByBiller]) VALUES (4, N'2', N'b', N'22', N'1', CAST(1 AS Numeric(18, 0)), 1, N'2019-01-01', N'1', N'1', 1, N'1', N'ac', N'1', CAST(1 AS Numeric(18, 0)), N'1', 1)
INSERT [dbo].[mReceipts] ([Id], [TransactionNumber], [PayerName], [PayerPhoneNumber], [PayerAccount], [AmountPaid], [Posted], [TransactionDate], [TrackingId], [Notes], [PaidToBiller], [PaymentToBillerRef], [BillerCode], [BillerProductID], [BillerProductQuantity], [OrderID], [ReversedByBiller]) VALUES (5, N'3', N'c', N'33', N'1', CAST(1 AS Numeric(18, 0)), 1, N'2019-01-01', N'1', N'1', 1, N'1', N'ac', N'1', CAST(1 AS Numeric(18, 0)), N'1', 1)
INSERT [dbo].[mReceipts] ([Id], [TransactionNumber], [PayerName], [PayerPhoneNumber], [PayerAccount], [AmountPaid], [Posted], [TransactionDate], [TrackingId], [Notes], [PaidToBiller], [PaymentToBillerRef], [BillerCode], [BillerProductID], [BillerProductQuantity], [OrderID], [ReversedByBiller]) VALUES (6, N'4', N'd', N'44', N'1', CAST(1 AS Numeric(18, 0)), 1, N'2019-01-01', N'1', N'1', 1, N'1', N'ac', N'1', CAST(1 AS Numeric(18, 0)), N'1', 0)
SET IDENTITY_INSERT [dbo].[mReceipts] OFF
SET IDENTITY_INSERT [dbo].[mServiceEnquiry] ON 

INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (1, N'saind nigger', N'0773303669', N'ac', CAST(N'2019-11-26 15:01:15.770' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (2, N'string', N'string', N'ac', CAST(N'2019-11-26 16:02:57.243' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (3, N'string', N'string', N'ac', CAST(N'2019-11-26 16:09:41.697' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (4, N'string', N'string', N'ac', CAST(N'2019-11-26 16:09:56.383' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (5, N'string', N'string', N'ac', CAST(N'2019-11-26 16:16:12.140' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (6, N'string', N'string', N'ac', CAST(N'2019-11-26 16:18:39.230' AS DateTime), 1)
INSERT [dbo].[mServiceEnquiry] ([id], [payerName], [phoneNumber], [billercode], [date], [isRead]) VALUES (7, N'string', N'string', N'ac', CAST(N'2019-11-26 16:22:15.467' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[mServiceEnquiry] OFF
SET IDENTITY_INSERT [dbo].[mTransAction] ON 

INSERT [dbo].[mTransAction] ([id], [_id], [date], [fromAccountNumber], [toAccountNumber], [fromCurrencyCode], [toCurrencyCode], [messageSubType], [transactionCurrency], [transactionAmount], [fromBankCode], [toBankCode], [costRate], [conversionRate], [uniqueReferenceIdentifier], [transactionBranch], [transactionPostingDate], [sourceSystemName], [transactionType], [lcyEquivalent], [countrycode], [trackingId], [billerCode], [ApiCallType]) VALUES (1, N'string', CAST(N'2019-09-26 18:06:30.033' AS DateTime), N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', N'string', 0)
SET IDENTITY_INSERT [dbo].[mTransAction] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Username], [Password], [Role], [Token]) VALUES (1, N'scb', N'tefvWAxfdGvVuuhPr/FhNyD++rFyEBBHj+b1X4ZBLzNrO+O/', N'admin', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  Index [IX_Providers_ProductId]    Script Date: 11/26/2019 5:00:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_Providers_ProductId] ON [dbo].[Providers]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Logs] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Providers]  WITH CHECK ADD  CONSTRAINT [FK_Providers_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[mBillerProduct] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Providers] CHECK CONSTRAINT [FK_Providers_Products_ProductId]
GO
USE [master]
GO
ALTER DATABASE [stanchart_simba] SET  READ_WRITE 
GO
