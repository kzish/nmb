﻿using System;
using System.Collections.Generic;

namespace Logging.Models
{
    public partial class MApiResources
    {
        public int Id { get; set; }
        public string ResourceName { get; set; }
    }
}
