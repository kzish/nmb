﻿using System;
using System.Collections.Generic;

namespace Logging.Models
{
    public partial class MActivityLog
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Username { get; set; }
        public string UserDevice { get; set; }
        public string UserIpAddress { get; set; }
        public string UserActivity { get; set; }
        public string Data { get; set; }
    }
}
