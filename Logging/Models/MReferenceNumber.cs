﻿using System;
using System.Collections.Generic;

namespace Logging.Models
{
    public partial class MReferenceNumber
    {
        public int Reference { get; set; }
    }
}
