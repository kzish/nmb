﻿using Logging.Models;
using Microsoft.AspNetCore.Mvc;
using System;

/// <summary>
/// logs all transactions made to stan chart
/// </summary>
namespace Logging.Controllers
{
    /// <summary>
    /// log the transactions
    /// </summary>
    /// <param name="activity">trasnactions param must be in json format: Content-Type = "application/json"</param>
    /// <returns></returns>
    [Route("TransactionLogger")]
    public class TransactionLoggerController : Controller
    {
        private dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
        /// <summary>
        /// log the transactions
        /// </summary>
        /// <param name="activity">trasnactions param must be in json format: Content-Type = "application/json"</param>
        /// <returns></returns>
        [HttpPost("LogTranaction")]
        public bool LogActivity([FromBody]MTransAction transaction)
        {
            try
            {
                db.MTransAction.Add(transaction);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //log the error
                Globals.log_data_to_file("Logging.Controllers.TranactionLogger.LogTransaction", ex);
                return false;
            }
        }
    }
}
