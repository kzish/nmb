﻿using Logging.Models;
using Microsoft.AspNetCore.Mvc;
using System;

/// <summary>
/// logs all system errors
/// </summary>
namespace Logging.Controllers
{
    /// <summary>
    /// log the error
    /// </summary>
    /// <param name="activity">error param must be in json format: Content-Type = "application/json"</param>
    /// <returns></returns>
    [Route("ErrorLogger")]
    public class ErrorLoggerController : Controller
    {
        private dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
        /// <summary>
        /// log the error
        /// </summary>
        /// <param name="activity">error param must be in json format: Content-Type = "application/json"</param>
        /// <returns></returns>
        [HttpPost("LogError")]
        public bool LogError([FromBody]MErrorLog error)
        {
            try
            {
                db.MErrorLog.Add(error);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //log the error
                Globals.log_data_to_file("Logging.Controllers.ErrorLogger.LogError",ex);
                return false;
            }
        }
    }
}
