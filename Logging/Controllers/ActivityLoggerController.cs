﻿using Logging.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using Logging.Models;

/// <summary>
/// logs all system activies
/// </summary>
namespace Logging.Controllers
{
    [Route("ActivityLogger")]
    public class ActivityLoggerController : Controller
    {
        private dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
        /// <summary>
        /// log the activity
        /// </summary>
        /// <param name="activity">activity param must be in json format: Content-Type = "application/json"</param>
        /// <returns></returns>
        [HttpPost("LogActivity")]
        public bool LogActivity([FromBody]MActivityLog activity)
        {
            try
            {
                db.MActivityLog.Add(activity);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //log the error
                Globals.log_data_to_file("Logging.Controllers.ActivityLogger.LogActivity",ex);
                return false;
            }
        }
    }
}
