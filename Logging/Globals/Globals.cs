﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;


//common functions

public class Globals
{

    /***** common ****/
    public static string log_file = "C:\\rubiem\\simba\\simba_logs.txt";
    //config file path
    public static string simba_config = @"c:\rubiem\simba\simba_config.json";//this is keyvalue pairs json file with configuration


    public static void log_data_to_file(string source, object data)
    {
        try
        {
            dynamic obj = new JObject();
            obj.source = source;
            obj.msg = data.ToString();
            var logdata = JsonConvert.SerializeObject(obj);
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, logdata);
        }
        catch (Exception ex)
        {
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, Environment.NewLine);
            System.IO.File.AppendAllText(log_file, ex.Message);
        }
    }


    /***** common ***/

    //get a guid string id
    public static string get_guid()
    {
        return Guid.NewGuid().ToString();
    }

    //use the application name as is verbatim for the microservices names
    /// <summary>
    /// return the key from the config file
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public static dynamic read_config(string key)
    {
        var file = System.IO.File.ReadAllText(simba_config);
        dynamic obj = JsonConvert.DeserializeObject(file);
        return obj.key;
    }
    /// <summary>
    ///write the key to the file
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public static void write_config(string key, string value)
    {

        if (!File.Exists(simba_config))
        {
            var path = Path.GetDirectoryName(simba_config);
            Directory.CreateDirectory(path);
            File.Create(simba_config).Close();
        };
        var file = File.ReadAllText(simba_config);
        dynamic obj = JsonConvert.DeserializeObject(file);
        if (obj == null)
        {
            obj = new JObject();
        }
        obj[key] = value;
        File.WriteAllText(simba_config, obj.ToString());
    }

   
    //hash password with md5
    public static string get_md5(string input)
    {
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }
        return sb.ToString();
    }

    //email message class
    public class EmailMessage
    {
        public string from_email { get; set; } = "postmansentinel@gmail.com";
        public string from_display_name { get; set; } = "CloudSmS";
        public string password { get; set; } = "QazWsxEdc@123";
        public string to_email { get; set; }
        public string subject { get; set; } = "System notification";
        public string message { get; set; }
    }

    //send the emails to many recipients
    public static void send_email(List<string> to, string message, string subject = "System Notification")
    {
        message = message.Replace("<br>", "/n");
        try
        {
            var email = new EmailMessage();
            Task.Run(() =>
            {
                foreach (var recipient in to)
                {
                    try
                    {
                        email = new EmailMessage();
                        email.to_email = recipient;
                        email.message = message;
                        email.subject = subject;
                        send_email(email);
                    }
                    catch (Exception ex)
                    {
                        Globals.log_data_to_file("Globals.send Email Logging",ex);
                    }
                }


            });
        }
        catch (Exception ex)
        {
            Globals.log_data_to_file("Globals.send Email Logging", ex);
        }

    }

    //send the actual email
    private static bool send_email(EmailMessage email)
    {
        bool sent = false;
        try
        {
            new SmtpClient
            {
                Host = "Smtp.Gmail.com",
                Port = 587,
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(email.from_email, email.password)
            }.Send(new MailMessage { IsBodyHtml = true, From = new MailAddress(email.from_email, email.from_display_name), To = { email.to_email }, Subject = email.subject, Body = email.message, BodyEncoding = Encoding.UTF8 });

            sent = true;
        }
        catch (Exception ex)
        {
            Globals.log_data_to_file("Globals.send Email Logging", ex);
        }

        return sent;
    }


















}//class