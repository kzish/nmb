﻿using System;
using System.Collections.Generic;

namespace BillerClientConsole.Models
{
    public partial class MBillerOrders
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string ProductCode { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? TransactionId { get; set; }
        public string BillerCode { get; set; }

        public virtual MTransAction Transaction { get; set; }
    }
}
