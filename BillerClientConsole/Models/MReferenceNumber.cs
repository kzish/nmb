﻿using System;
using System.Collections.Generic;

namespace BillerClientConsole.Models
{
    public partial class MReferenceNumber
    {
        public int Reference { get; set; }
    }
}
