﻿using System;
using System.Collections.Generic;

namespace BillerClientConsole.Models
{
    public partial class MApiResources
    {
        public int Id { get; set; }
        public string ResourceName { get; set; }
    }
}
