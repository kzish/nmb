﻿using System;
using System.Collections.Generic;

namespace BillerClientConsole.Models
{
    public partial class EnumBillerType
    {
        public EnumBillerType()
        {
            MBillerInfo = new HashSet<MBillerInfo>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<MBillerInfo> MBillerInfo { get; set; }
    }
}
