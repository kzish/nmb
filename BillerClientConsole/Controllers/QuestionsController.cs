﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace BillerClientConsole.Controllers
{
    [Route("Questions")]
    [Authorize(Roles = "biller")]
    public class QuestionsController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet("AddNewQuestion")]
        public IActionResult AddNewQuestion()
        {
            ViewBag.title = "Add Question";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            ViewBag.billercode = user.MBillerInfo.BillerCode;
            return View();
        }

        /// <summary>
        /// allows the user to create a new product and upload it to the server
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("AddNewQuestion")]
        public IActionResult AddNewQuestion(MEnquiryQuestions quiz)
        {
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                ViewBag.title = "Add Question";
                quiz.Billercode = user.MBillerInfo.BillerCode;
                db.MEnquiryQuestions.Add(quiz);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerQuestions");
        }

        /// <summary>
        /// update the question
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("UpdateQuestion")]
        public IActionResult UpdateQuestion(MEnquiryQuestions qiz)
        {
            ViewBag.title = "Update Question";
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i => i.MBillerInfo).FirstOrDefault();
                qiz.Billercode = user.MBillerInfo.BillerCode;
                db.Entry(qiz).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerQuestions");
        }

        //fetch the ques from the servr
        [HttpGet("EditQuestion/{id}")]
        public IActionResult EditQuestion(int id)
        {
            ViewBag.title = "Edit Question";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var question = db.MEnquiryQuestions.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
            ViewBag.question = question ?? new MEnquiryQuestions();
            return View();
        }



        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("EnableQuestion/{id}")]
        public IActionResult EnableQuestion(int id)
        {
            ViewBag.title = "Enable Question";
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var question = db.MEnquiryQuestions.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                question.Status = true;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }

            return RedirectToAction("ListBillerQuestions");
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("DisableQuestion/{id}")]
        public IActionResult DisableQuestion(int id)
        {
            ViewBag.title = "Disable Question";
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var question = db.MEnquiryQuestions.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                question.Status = false;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerQuestions");
        }




        [HttpGet("ListBillerQuestions")]
        [HttpGet("")]
        public IActionResult ListBillerQuestions()
        {
            ViewBag.title = "Questions";
            //ajax reports is in partial controllers
            TempData["loadUrl"] = $"~/ajax/questions/BillerQuestions";
            return View();
        }




        [HttpGet("DeleteQuestion/{id}")]
        public IActionResult DeleteQuestion(int id)
        {
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var question = db.MEnquiryQuestions.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                db.Remove(question);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerQuestions");
        }

    }
}
