﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace BillerClientConsole.Controllers
{
    [Route("Notifications")]
    [Authorize(Roles = "biller")]
    public class NotificationsController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


        [HttpGet("AllNotifications")]
        public IActionResult AllNotifications(string date_from, string date_to)
        {
            ViewBag.title = "Notifications / All Notifications";

            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            var notifications = db.MNotification.Where(i => i.BillerCode == user.MBillerInfo.BillerCode);
            if (!string.IsNullOrEmpty(date_from))
            {
                notifications = notifications.Where(i => i.Date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            if (!string.IsNullOrEmpty(date_to))
            {
                notifications = notifications.Where(i => i.Date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            var notifications_ = notifications.OrderByDescending(i => i.Date).ToList();
            ViewBag.notifications = notifications.ToList() ?? new List<MNotification>();
            return View();
        }



        /// <summary>
        /// show the notifications that have been read
        /// </summary>
        /// <returns></returns>
        [HttpGet("Read")]
        public IActionResult Read(string date_from, string date_to)
        {
            ViewBag.title = "Notifications / Read";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            var notifications = db.MNotification.Where(i => i.BillerCode == user.MBillerInfo.BillerCode && i.IsRead == true);
            if (!string.IsNullOrEmpty(date_from))
            {
                notifications = notifications.Where(i => i.Date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            if (!string.IsNullOrEmpty(date_to))
            {
                notifications = notifications.Where(i => i.Date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            var notifications_ = notifications.OrderByDescending(i => i.Date).ToList();
            ViewBag.notifications = notifications.ToList() ?? new List<MNotification>();
            return View();
        }


        /// <summary>
        /// show the notifications that have not been read
        /// </summary>
        /// <returns></returns>
        [HttpGet("UnRead")]
        public IActionResult UnRead(string date_from, string date_to)
        {
            ViewBag.title = "Notifications / UnRead";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            var notifications = db.MNotification.Where(i => i.BillerCode == user.MBillerInfo.BillerCode && i.IsRead == false);
            if (!string.IsNullOrEmpty(date_from))
            {
                notifications = notifications.Where(i => i.Date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            if (!string.IsNullOrEmpty(date_to))
            {
                notifications = notifications.Where(i => i.Date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            var notifications_ = notifications.OrderByDescending(i => i.Date).ToList();
            ViewBag.notifications = notifications.ToList() ?? new List<MNotification>();
            return View();
        }


        /// <summary>
        /// view a single notification
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("ViewDetails/{id}")]
        public IActionResult ViewDetails(int id)
        {
            ViewBag.title = "Notification";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var notification = db.MNotification.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
            ViewBag.notification = notification ?? new MNotification();
            return View();
        }

        [Route("MarkAsRead/{id}/{date_from?}/{date_to?}/{view}")]
        public IActionResult MarkAsRead(int id, int pageNumber = 1, string date_from = null, string date_to = null, string view = "AllNotifications")
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var notification = db.MNotification.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
            notification.IsRead = true;
            db.SaveChanges();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return RedirectToAction(view, "Notifications", new { date_from, date_to });
        }

        [Route("MarkAsUnRead/{id}/{date_from?}/{date_to?}/{view}")]
        public IActionResult MarkAsUnRead(int id, int pageNumber = 1, string date_from = null, string date_to = null, string view = "AllNotifications")
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var notification = db.MNotification.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
            notification.IsRead = false;
            db.SaveChanges();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return RedirectToAction(view, "Notifications", new { date_from, date_to });
        }



        [Route("Delete/{id}/{date_from?}/{date_to?}/{view}")]
        public IActionResult Delete(int id, string date_from = null, string date_to = null, string view = "AllNotifications")
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var notification = db.MNotification.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
            db.Remove(notification);
            db.SaveChanges();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return RedirectToAction(view, "Notifications", new { date_from, date_to });
        }
    }
}
