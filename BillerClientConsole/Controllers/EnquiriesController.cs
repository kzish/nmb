﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BillerClientConsole.Controllers
{

    [Route("Enquiries")]
    [Authorize(Roles="biller")]
    public class EnquiriesController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        [HttpGet("ListAllEnquiries")]
        public IActionResult ListAllEnquiries(string date_from, string date_to)
        {
            ViewBag.title = "Enquiries";
            TempData["loadUrl"] = "~/ajax/enquiries/ListAllEnquiries";
            TempData["date_from"] = date_from;
            TempData["date_to"] = date_to;
            TempData["open"] = false;
            TempData["closed"] = false;
            return View();
        }

        [HttpGet("ListAllOpenEnquiries")]
        public IActionResult ListAllOpenEnquiries(string date_from, string date_to)
        {
            ViewBag.title = "Open Enquiries";
            TempData["loadUrl"] = "~/ajax/enquiries/ListAllEnquiries";
            TempData["date_from"] = date_from;
            TempData["date_to"] = date_to;
            TempData["open"] = true;
            TempData["closed"] = false;
            return View();
        }


        [HttpGet("ListAllClosedEnquiries")]
        public IActionResult ListAllClosedEnquiries(string date_from, string date_to)
        {
            ViewBag.title = "Closed Enquiries";
            TempData["loadUrl"] = "~/ajax/enquiries/ListAllEnquiries";
            TempData["date_from"] = date_from;
            TempData["date_to"] = date_to;
            TempData["open"] = false;
            TempData["closed"] = true;
            return View();
        }


        /// <summary>
        /// this route shows the questions and answers of 
        /// a service enquiry uploaded by a mobile user
        /// </summary>
        /// <returns></returns>
        [HttpGet("ViewEnquiryDetails/{id}")]
        public IActionResult ViewEnquiryDetails(int id)
        {
            ViewBag.title = "Enquiry Questions";
            var questions_and_answers = db.MEnquiryAnswer.Where(i => i.ServiceEnquiryId == id).ToList();
            ViewBag.enqs = questions_and_answers;
            return View();
        }

        /// <summary>
        /// show the update form
        /// fetch the enquiry by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("EditEnquiryQuestion")]
        public IActionResult EditEnquiryQuestion(int id)
        {
            try
            {
                ViewBag.title = "Edit Enquiry Question";
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(r=>r.MBillerInfo).FirstOrDefault();
                var enq = db.MEnquiryQuestions.Where(i => i.Id == id).FirstOrDefault();
                ViewBag.enq = enq;
                ViewBag.billercode = user.MBillerInfo.BillerCode;
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return View();
        }


        // to create a new enquiry question
        [HttpPost("CreateEnquiryQuestion")]
        public IActionResult CreateEnquiryQuestion(MEnquiryQuestions _question)
        {
            try
            {
                ViewBag.title = "Create Enquiry Question";
                db.MEnquiryQuestions.Add(_question);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("CreateEnquiry");
        }


        //update the question
        [HttpPost("UpdateEnquiryQuestion")]
        public IActionResult UpdateEnquiryQuestion(MEnquiryQuestions _question)
        {
            ViewBag.title = "Create Enquiry";
            try
            {
                db.Entry(_question).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListAllEnquiries");
        }

        /// <summary>
        /// delete an enquiry
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("DeleteEnquiryQuestion")]
        public IActionResult DeleteEnquiryQuestion(int id)
        {
            try
            {
                var question=db.MEnquiryQuestions.Where(i => i.Id == id).FirstOrDefault();
                db.Remove(question);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListAllEnquiries");
        }


      


    }
}
