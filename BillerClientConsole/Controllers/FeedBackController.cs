﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using BillerClientConsole.Models;
using Microsoft.EntityFrameworkCore;

namespace BillerClientConsole.Controllers
{
    [Route("FeedBack")]
    [Authorize(Roles = "biller")]
    public class FeedBackController : Controller
    {

        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }



        [Route("ViewDetails/{id}")]
        public IActionResult ViewDetails(int id)
        {
            ViewBag.title = "Report";
            //fetch the feedback from the api
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var feedback = db.MFeedBack.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
            ViewBag.feedback = feedback;
            return View();
        }

        [Route("MarkAsRead/{id}/{pageNumber?}/{date_from?}/{date_to?}")]
        public IActionResult MarkAsRead(int id, int pageNumber = 1, string date_from = null, string date_to = null)
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var feedback = db.MFeedBack.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                feedback.IsRead = false;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListFeedBack", "Reports", new { pageNumber, date_from, date_to });
        }

        [Route("MarkAsUnRead/{id}/{pageNumber?}/{date_from?}/{date_to?}")]
        public IActionResult MarkAsUnRead(int id, int pageNumber = 1, string date_from = null, string date_to = null)
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var feedback = db.MFeedBack.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                feedback.IsRead = false;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListFeedBack", "Reports", new { pageNumber, date_from, date_to });
        }



        [Route("Delete/{id}/{pageNumber?}/{date_from?}/{date_to?}")]
        public IActionResult Delete(int id, int pageNumber = 1, string date_from = null, string date_to = null)
        {
            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var feedback = db.MFeedBack.Where(i => i.Id == id && i.Billercode == user.MBillerInfo.BillerCode).FirstOrDefault();
                db.Remove(feedback);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListFeedBack", "Reports", new { pageNumber, date_from, date_to });
        }


    }
}
