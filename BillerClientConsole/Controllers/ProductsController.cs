﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace BillerClientConsole.Controllers
{
    [Route("Products")]
    [Authorize(Roles = "biller")]
    public class ProductsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet("AddNewProduct")]
        public IActionResult AddNewProduct()
        {
            ViewBag.title = "Add Product";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            ViewBag.billercode = user.MBillerInfo.BillerCode;
            return View();
        }

        /// <summary>
        /// allows the user to create a new product and upload it to the server
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("AddNewProduct")]
        public IActionResult AddProduct(MBillerProduct product)
        {
            try
            {
                ViewBag.title = "Add Product";
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                product.BillerCode = user.MBillerInfo.BillerCode;//link to the billercode
                db.MBillerProduct.Add(product);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerProducts");
        }


        /// <summary>
        /// fetch the product and display it
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("ViewDetails/{id}")]
        public IActionResult ViewDetails(int id)
        {
            ViewBag.title = "View Product Details";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var product = db.MBillerProduct.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
            ViewBag.product = product;
            return View();
        }




        [HttpGet("ListBillerProducts")]
        [HttpGet("")]
        public IActionResult ListBillerProducts(int pageNumber, string date_from, string date_to)
        {
            ViewBag.title = "Products";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            //ajax reports is in partial controllers
            TempData["loadUrl"] = $"~/ajax/products/BillerProducts";
            TempData["pageNumber"] = pageNumber;
            TempData["billercode"] = user.MBillerInfo.BillerCode;
            TempData["date_to"] = date_to;
            TempData["date_from"] = date_from;
            return View();
        }

        /// <summary>
        /// update the product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("UpdateProduct")]
        public IActionResult UpdateProduct(MBillerProduct product)
        {
            try
            {
                db.Entry(product).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerProducts");
        }


        [HttpGet("DeleteProduct/{id}")]
        public IActionResult DeleteProduct(int id)
        {
            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
                var product = db.MBillerProduct.Where(i => i.Id == id && i.BillerCode == user.MBillerInfo.BillerCode).FirstOrDefault();
                db.Remove(product);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ListBillerProducts");
        }


    }
}
