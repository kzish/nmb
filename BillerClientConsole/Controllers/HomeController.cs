﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BillerClientConsole.Controllers
{
    [Authorize(Roles = "biller")]
    [Route("Home")]
    [Route("")]
    public class HomeController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }



        [Route("Dashboard")]
        [Route("")]
        public async Task<IActionResult> Dashboard()
        {
            ViewBag.title = "Dashboard";

            try
            {
                var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();

                //notifications
                var notifications_read = db.MNotification.Where(i => i.IsRead == true && i.BillerCode == user.MBillerInfo.BillerCode).Count();
                var notifications_un_read = db.MNotification.Where(i => i.IsRead == false && i.BillerCode == user.MBillerInfo.BillerCode).Count();
                var notifications_total = db.MNotification.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).Count();

                ViewBag.notifications_read = notifications_read;
                ViewBag.notifications_un_read = notifications_un_read;
                ViewBag.notifications_total = notifications_total;


                //products
                var products_active = db.MBillerProduct.Where(i => i.BillerCode == user.MBillerInfo.BillerCode && i.Available==true).Count();
                var products_in_active = db.MBillerProduct.Where(i => i.BillerCode == user.MBillerInfo.BillerCode && i.Available==false).Count();
                var products_total = db.MBillerProduct.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).Count();

                ViewBag.products_active = products_active;
                ViewBag.products_in_active = products_in_active;
                ViewBag.products_total = products_total;


                //transactions and payments
                var payment_history = db.MPayments.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).Count();
                var transaction_history = db.MTransAction.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).Count();

                ViewBag.payment_history = payment_history;
                ViewBag.transaction_history = transaction_history;

                //feedback
                var read_feedback = db.MFeedBack.Where(i => i.Billercode == user.MBillerInfo.BillerCode && i.IsRead == true).Count();
                var un_read_feedback = db.MFeedBack.Where(i => i.Billercode == user.MBillerInfo.BillerCode && i.IsRead == false).Count();

                ViewBag.read_feedback = read_feedback;
                ViewBag.un_read_feedback = un_read_feedback;

                //enquiries
                var read_enquiries = db.MServiceEnquiry.Where(i => i.Billercode == user.MBillerInfo.BillerCode && i.IsRead==true).Count();
                var un_read_enquiries = db.MServiceEnquiry.Where(i => i.Billercode == user.MBillerInfo.BillerCode && i.IsRead == false).Count();

                ViewBag.read_enquiries = read_enquiries;
                ViewBag.un_read_enquiries = un_read_enquiries;
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error fetching data from server";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }

            return View();
        }


        [Route("About")]
        public IActionResult About()
        {
            ViewBag.title = "About";
            return View();
        }


        [Route("Products")]
        public IActionResult Products()
        {
            ViewBag.title = "Products";
            return View();
        }



    }
}
