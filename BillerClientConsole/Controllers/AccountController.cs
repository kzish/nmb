﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace BillerClientConsole.Controllers
{
    [Route("Account")]
    [Authorize]
    public class AccountController : Controller
    {

        private UserManager<IdentityUser> userManager;
        private SignInManager<IdentityUser> signInManager;
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet("ChangePassword")]
        public IActionResult ChangePassword()
        {
            ViewBag.title = "Account / Change Password";
            return View();
        }


        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword(string new_password, string current_password)
        {
            ViewBag.title = "Account / Change Password";
            var user_id = userManager.GetUserId(User);
            var user = await userManager.FindByIdAsync(user_id);
            var result = await userManager.ChangePasswordAsync(user, current_password, new_password);

            if (result.Succeeded)
            {
                TempData["msg"] = "Password Changed";
                TempData["type"] = "success";
                await signInManager.SignOutAsync();
                return RedirectToAction("Login", "Auth");
            }
            else
            {
                var errors = string.Empty;
                foreach(var e in result.Errors.ToList())
                {
                    errors += e.Description + ", ";
                }
                TempData["msg"] = errors;
                TempData["type"] = "error";
            }
            return View();
        }


        /// <summary>
        /// view biller details
        /// </summary>
        /// <returns></returns>
        [HttpGet("BillerDetails")]
        public IActionResult BillerDetails()
        {
            ViewBag.title = "Account / Biller Details";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name)
                .Include(i=>i.MBillerInfo)
                .FirstOrDefault();
            var banks = db.EnumBanks.ToList();
            ViewBag.banks = banks;
            ViewBag.user = user;
            return View();
        }


        [HttpPost("UpdateBillerDetails")]
        public IActionResult UpdateBillerDetails(MBillerInfo biller, MBankAccount bankAccount, MAddress address)
        {
            try
            {
                //dbSimbaModels.Entry(bankAccount).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                //dbSimbaModels.Entry(biller).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                //dbSimbaModels.Entry(address).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                //dbSimbaModels.SaveChanges();
                TempData["tmsg"] = "Details Saved";
                TempData["type"] = "success";

            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error occurred";
                TempData["type"] = "error";
            }
            return RedirectToAction("BillerDetails");
        }


        /// <summary>
        /// handles the billers banking details
        /// </summary>
        /// <returns></returns>
        [HttpGet("BillerAccounts")]
        public IActionResult BillerAccounts()
        {
            ViewBag.title = "Bank Details";
            var biller = db.AspNetUsers.Where(i => i.Email == User.Identity.Name)
                .Include(i => i.MBankAccount)
                .Include(i => i.MBillerInfo)
                .FirstOrDefault();

            ViewBag.biller = biller;
            ViewBag.banks = db.EnumBanks.ToList();
            ViewBag.currency = db.EnumCurrencyCode.ToList();

            return View();
        }


        [HttpPost("CreateBillerAccount")]
        public IActionResult CreateBillerAccount(string Id, MBankAccount account)
        {
            ViewBag.title = "Create Biller";
            try
            {
                account.BillerId = Id;
                db.MBankAccount.Add(account);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"BillerAccounts", "Account", new { Id });
        }

        //delete bank account
        [HttpGet("DeleteAccount/{id}")]
        public IActionResult DeleteAccount(int id)
        {
            ViewBag.title = "Delete Account";
            var Id = string.Empty;
            try
            {
                var account = db.MBankAccount.Find(id);
                Id = account.BillerId;
                db.Remove(account);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction($"BillerAccounts", "Account", new { Id });
        }

        //make the selected account the default account
        [HttpGet("SetDefaultAccount/{id}")]
        public IActionResult SetDefaultAccount(int id)
        {
            ViewBag.title = "Set Default Account";
            var biller_id = string.Empty;
            try
            {
                var account = db.MBankAccount.Find(id);
                biller_id = account.BillerId;
                var accounts = db.MBankAccount.Where(i => i.BillerId == biller_id).ToList();
                //disable all other accounts to leave this one active
                foreach (var a in accounts)
                {
                    a.Active = false;
                }
                account.Active = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction($"BillerAccounts", "Account", new { Id=biller_id });
        }

    }
}
