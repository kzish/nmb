﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BillerClientConsole.Controllers
{
    [Route("PaymentHistory")]
    [Authorize(Roles = "biller")]
    public class PaymentHistoryController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        [Route("ViewDetails/{id}")]
        public IActionResult ViewDetails(int id)
        {
            ViewBag.title = "Report";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var payment = db.MPayments.Where(i => i.BillerCode == user.MBillerInfo.BillerCode && i.Id == id).FirstOrDefault();
            ViewBag.PaymentDate = payment.PaymentDate;
            ViewBag.PaidAmount = payment.PaidAmount;
            ViewBag.TrackingID = payment.TrackingId;
            ViewBag.BatchNo = payment.BatchNo;
            return View();
        }


    }
}
