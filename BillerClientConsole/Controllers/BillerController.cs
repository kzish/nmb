﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace BillerClientConsole.Controllers
{
    public class BillerController : Controller
    {
    }
}
/*   [HttpPost("UpdateTransactionPostStatus")]
    public JsonResult UpdateTransactionPostStatus([FromBody] PostStatusUpdate postUpdate)
    {
        try
        {

            using (var db = new db())
            {
                db.mReceipts
              .Where(p => p.TransactionNumber == postUpdate.transnumber)
              .Set(p => p.Posted, postUpdate.status)
              .Update();

            }


            return Json(new
            {
                res = "ok",
                msg = "Successfully Updated Product",

            });


        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    [HttpPost("ReverseTransaction")]
    public JsonResult ReverseTransaction([FromBody] string TransactionNumber)
    {
        try
        {

            using (var db = new db())
            {
                db.mReceipts
              .Where(p => p.TransactionNumber == TransactionNumber)
              .Set(p => p.ReversedByBiller, true)
              .Update();

            }


            return Json(new
            {
                res = "ok",
                msg = "Transaction Successfully Reversed",

            });


        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    [HttpPost("UpdateFeedBackViewStatus")]
    public JsonResult UpdateFeedBackStatus([FromBody] string feedbacknumber)
    {
        try
        {

            using (var db = new db())
            {
                db.feedback
                .Where(p => p.feedbacknumber == feedbacknumber)
                .Set(p => p.noted, true)
                .Update();

            }


            return Json(new
            {
                res = "ok",
                msg = "Feedback viewed",

            });


        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    [HttpPost("UpdateTransactionPaymentStatus")]
    public JsonResult UpdateTransactionPaymentStatus([FromBody] string trackingID)
    {
        try
        {

            var db = new db();


            //     db.Update(receipts);
            return Json(new
            {
                res = "ok",
                msg = "Successfully Updated Product",

            });


        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }

    [HttpGet("GetBillerPendingTransactions")]
    public JsonResult GetBillerPendingTransactions(string BillerCode)
    {
        try
        {
            var db = new db();
            var BillerTrans = (from translist in db.mReceipts
                               where translist.Posted == 0 && translist.BillerCode == BillerCode
                               select translist).ToList();


            //return success
            return Json(new
            {
                responce_code = "00",
                res = "ok",
                data = BillerTrans
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }




    [HttpGet("get_transactionhistory_by_id")]
    public JsonResult get_transactionhistory_by_id(int id)
    {
        var db = new db();
        var trans = db.mReceipts.Where(i => i.Id == id).FirstOrDefault();
        db.Dispose();
        return Json(trans);
    }

    [HttpGet("GetAllBillerPaymentHistory")]
    public JsonResult GetAllBillerPaymentHistory(string BillerCode, string date_from, string date_to)
    {
        try
        {
            var db = new db();
            var biller = db.payments.Where(i => i.BillerCode == BillerCode);
            if (!string.IsNullOrEmpty(date_from))
            {
                biller = biller.Where(i => DateTime.Parse(i.PaymentDate) >= DateTime.Parse(date_from));
            }

            if (!string.IsNullOrEmpty(date_to))
            {
                biller = biller.Where(i => DateTime.Parse(i.PaymentDate) <= DateTime.Parse(date_to));
            }


            var BillerPayments = biller.ToList();
            return Json(new
            {
                responce_code = "00",
                res = "ok",
                data = BillerPayments
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }

    [HttpGet("get_biller_payment_history_by_id")]
    public JsonResult get_biller_payment_by_id(int id)
    {
        var db = new db();
        var payment = db.payments.Where(i => i.Id == id).First();
        db.Dispose();
        return Json(payment);
    }




    //[HttpGet("GetBillerFeedback")]
    private JsonResult GetBillerFeedBack(string BillerCode, int per_page = 10, int page = 0)
    {
        try
        {
            var db = new db();
            var BillerFeedBack = (from translist in db.feedback
                                  where translist.billercode == BillerCode
                                  select translist).Skip(per_page * page).Take(per_page).ToList();


            return Json(new
            {
                responce_code = "00",
                res = "ok",
                data = BillerFeedBack
            });GetBillerQuestionById
        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }

    [HttpGet("GetAllBillerFeedback")]
    public JsonResult GetAllBillerFeedBack(string BillerCode, string date_from, string date_to)
    {

        try
        {
            var db = new db();
            var biller = db.feedback.Where(i => i.billercode == BillerCode);
            if (!string.IsNullOrEmpty(date_from))
            {
                biller = biller.Where(i => i.date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                biller = biller.Where(i => i.date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }
            var BillerFeedBack = biller.ToList();
            db.Dispose();

            return Json(new
            {
                responce_code = "00",
                res = "ok",
                data = BillerFeedBack
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }

    [HttpGet("get_feedback_by_id")]
    public JsonResult get_feedback_by_id(int id)
    {
        var db = new db();
        var feedback = db.feedback.Where(i => i.id == id).First();
        db.Dispose();
        return Json(feedback);
    }


    [HttpGet("mark_as_read_feedback_by_id")]
    public JsonResult mark_as_read_feedback_by_id(int id)
    {
        var db = new db();
        db.feedback
          .Where(p => p.id == id)
          .Set(p => p.noted, true)
          .Update();
        db.Dispose();
        return Json(true);
    }


    [HttpGet("mark_as_un_read_feedback_by_id")]
    public JsonResult mark_as_un_read_feedback_by_id(int id)
    {
        var db = new db();
        db.feedback
          .Where(p => p.id == id)
          .Set(p => p.noted, false)
          .Update();
        db.Dispose();
        return Json(true);
    }


    [HttpGet("delete_feedback_by_id")]
    public JsonResult delete_feedback_by_id(int id)
    {
        var db = new db();
        var feedback = db.feedback.Delete(i => i.id == id);
        db.Dispose();
        return Json(feedback);
    }



    [HttpGet("GetAllBillerTransactionsHistory")]
    public JsonResult GetAllBillerTransactionsHistory(string BillerCode, string date_from, string date_to)
    {
        try
        {
            var db = new db();
            var biller = db.mReceipts.Where(i => i.BillerCode == BillerCode);
            if (!string.IsNullOrEmpty(date_from))
            {
                biller = biller.Where(i => DateTime.Parse(i.TransactionDate) >= DateTime.Parse(date_from));
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                biller = biller.Where(i => DateTime.Parse(i.TransactionDate) <= DateTime.Parse(date_to));
            }
            var BillerTrans = biller.ToList();


            return Json(new
            {
                responce_code = "00",
                res = "ok",
                data = BillerTrans
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }

    /// <summary>
    /// allows the offline biller to add his/her products from the console
    /// </summary>
    /// <param name="product"></param>
    /// <returns></returns>
    [HttpPost("postBillerProduct")]
    public JsonResult postBillerProductOffline([FromBody]mProductOffline product)
    {
        try
        {
            if (string.IsNullOrEmpty(product.billercode))
            {
                return Json(new
                {
                    res = "err",
                    msg = "Biller code parameter is missing"
                });
            }
            using (var db = new db())
            {

                db.Insert(product);
                db.Dispose();
            }


            return Json(new
            {
                res = "ok",
                msg = "Successfully Added Product",

            });
        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    /// <summary>
    /// delete an enquiry question by its id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("delete_biller_enquiry_question_by_id")]
    public JsonResult delete_biller_enquiry_question_by_id(int id)
    {
        var db = new db();
        db.questions.Delete(i => i.id == id);
        db.Dispose();
        return Json(true);
    }


    /// <summary>
    /// fetches a single enquiry question for viewing before updating
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("fetchEnquiryQuestionByID")]
    public JsonResult fetchEnquiryQuestionByID(int id)
    {
        var db = new db();
        var enq = db.questions.Where(i => i.id == id).FirstOrDefault();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = enq
        });
    }

    /// <summary>
    /// update the enquiry question
    /// </summary>
    /// <param name="_enq"></param>
    /// <returns></returns>
    [HttpPost("UpdateBillerEnquiryQuestionByID")]
    public JsonResult UpdateBillerEnquiryQuestionByID([FromBody]mEnquiryQuestions _enq)
    {
        var db = new db();
        var enq = db.questions.Where(i => i.id == _enq.id)
        .Set(i => i.question, _enq.question)
        .Set(i => i.status, _enq.status)
        .Update();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = "Saved"
        });
    }


    /// <summary>
    /// post an enquiry question and save into the database
    /// creates a new enquiry question
    /// </summary>
    /// <param name="inquiry"></param>
    /// <returns></returns>
    [HttpPost("AddBillerEnquiryQuestion")]
    public JsonResult AddBillerEnquiryQuestion([FromBody]mEnquiryQuestions enq)
    {
        try
        {
            var db = new db();
            db.Insert(enq);
            db.Dispose();
            return Json(new
            {
                res = "ok",
                msg = "Saved",
            });
        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    [HttpGet("GetAllBillerQuestions")]
    public JsonResult GetAllBillerQuestions(string billercode)
    {
        try
        {
            var db = new db();
            var quiz = db.questions.Where(i => i.billercode == billercode).ToList();
            db.Dispose();
            return Json(new
            {
                res = "ok",
                msg = quiz,
            });
        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }

    [HttpGet("GetBillerQuestionById")]
    public JsonResult GetBillerQuestionById(int id)
    {
        try
        {
            var db = new db();
            var quiz = db.questions.Where(i => i.id == id).FirstOrDefault();
            db.Dispose();
            return Json(new
            {
                res = "ok",
                msg = quiz,
            });
        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }



    [HttpGet("EnableBillerEnquiryQuestionById")]
    public JsonResult EnableBillerEnquiryQuestionById(int id)
    {
        var db = new db();
        var q = db.questions.Where(i => i.id == id)
            .Set(i => i.status, true)
            .Update();
        db.Dispose();

        return Json(new
        {
            res = "ok",
            msg = "deleted"
        });
    }

    [HttpGet("DisableBillerEnquiryQuestionById")]
    public JsonResult DisableBillerEnquiryQuestionById(int id)
    {
        var db = new db();
        var q = db.questions.Where(i => i.id == id)
            .Set(i => i.status, false)
            .Update();
        db.Dispose();

        return Json(new
        {
            res = "ok",
            msg = "deleted"
        });
    }

    /// <summary>
    /// used by the client console to retrive enquiries made by the biller's clients
    /// will return the service enquiry containing both questions and answers
    /// these are enquiries not the questions
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpGet("GetAllBillerEnquiries")]
    public JsonResult GetAllBillerEnquiries(string billercode, string date_from, string date_to)
    {
        try
        {
            var db = new db();
            //
            var service_enquiries = db.mServiceEnquiries.Where(i => i.billercode == billercode);

            if (!string.IsNullOrEmpty(date_from))
            {
                service_enquiries = service_enquiries.Where(i => i.date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                service_enquiries = service_enquiries.Where(i => i.date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }

            var _service_enquiries = service_enquiries.ToList();

            db.Dispose();

            return Json(new
            {
                res = "ok",
                msg = _service_enquiries
            });


        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                data = ex.Message
            });
        }

    }

    [HttpGet("DeleteBillerEnquiryQuestionById")]
    public JsonResult DeleteBillerEnquiryQuestionById(int id)
    {
        var db = new db();
        db.questions.Delete(i => i.id == id);
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = "deleted"
        });

    }


    //allows the biller to view all the questions in his repository of questions
    [HttpGet("getAllBillerEnquiryQuestions")]
    public JsonResult getAllBillerEnquiryQuestions(string billercode)
    {
        try
        {
            var db = new db();
            var BillerQuestions = db.questions.Where(i => i.billercode == billercode).ToList();
            db.Dispose();
            if (BillerQuestions != null)
            {
                return Json(new
                {
                    res = "ok",
                    msg = BillerQuestions
                });
            }
            else
            {
                return Json(new
                {
                    res = "ok",
                    msg = "No Biller Enquiry Questions"
                });
            }

        }
        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message
            });
        }

    }


    [HttpPost("UpdateBillerProduct")]
    public JsonResult UpdateBillerProductOffline([FromBody]mProductOffline product)
    {
        try
        {
            mProductOffline productoffline = new mProductOffline();
            var db = new db();

            productoffline = (from products in db.product
                              where products.billercode == product.billercode
                              select products).FirstOrDefault();
            if (productoffline != null)
            {
                db.Update(product);
                return Json(new
                {
                    res = "ok",
                    msg = "Successfully Updated Product",

                });
            }
            else
            {
                return Json(new
                {
                    res = "err",
                    msg = "You have supplied a product with an Invalid Product Code"

                });
            }

        }

        catch (Exception ex)
        {
            return Json(new
            {
                res = "err",
                msg = ex.Message

            });
        }

    }


    /// <summary>
    /// retreives all biller notifications by biller code
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpGet("fetchBillerNotifications")]
    public JsonResult fetchBillerNotifications(string billercode, string date_from, string date_to)
    {
        var db = new db();
        var notifications = db.notifications.Where(i => i.BillerCode == billercode);
        if (!string.IsNullOrEmpty(date_from))
        {
            notifications = notifications.Where(i => i.date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
        }

        if (!string.IsNullOrEmpty(date_to))
        {
            notifications = notifications.Where(i => i.date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
        }

        var notifications_ = notifications.OrderByDescending(i => i.date).ToList();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = notifications_
        });

    }


    /// <summary>
    /// retreives biller notification by notification id
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpGet("fetchBillerNotificationById")]
    public JsonResult fetchBillerNotificationById(int id)
    {
        var db = new db();
        var notification = db.notifications.Where(i => i.id == id).FirstOrDefault();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = notification
        });
    }


    /// <summary>
    /// mark notification as read
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpGet("markNotificationAsRead")]
    public JsonResult markNotificationAsRead(int id)
    {
        var db = new db();
        var notification = db.notifications.Where(i => i.id == id)
            .Set(i => i.isRead, true)
            .Update();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = notification
        });
    }

    /// <summary>
    /// mark notification as read
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpGet("markNotificationAsUnRead")]
    public JsonResult markNotificationAsUnRead(int id)
    {
        var db = new db();
        var notification = db.notifications.Where(i => i.id == id)
            .Set(i => i.isRead, false)
            .Update();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = notification
        });
    }

    /// <summary>
    /// delete a notification
    /// </summary>
    /// <param name="billercode"></param>
    /// <returns></returns>
    [HttpPost("deleteNotificationById")]
    public JsonResult deleteNotificationById(int id)
    {
        var db = new db();
        var notification = db.notifications.Delete(i => i.id == id);
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = notification
        });
    }

    [HttpGet("countUnReadNotificationsForBiller")]
    public int countUnReadNotificationsForBiller(string billercode)
    {
        var db = new db();
        var items = db.notifications.Where(i => i.BillerCode == billercode && !i.isRead).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countReadNotificationsForBiller")]
    public int countReadNotificationsForBiller(string billercode)
    {
        var db = new db();
        var items = db.notifications.Where(i => i.BillerCode == billercode && i.isRead).Count();
        db.Dispose();
        return items;
    }


    [HttpGet("countAllActiveProductsForBiller")]
    public int countAllActiveProductsForBiller(string billercode)
    {
        var db = new db();
        var items = db.product.Where(i => i.billercode == billercode && i.available).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllInActiveProductsForBiller")]
    public int countAllInActiveProductsForBiller(string billercode)
    {
        var db = new db();
        var items = db.product.Where(i => i.billercode == billercode && !i.available).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllProductsForBiller")]
    public int countAllProductsForBiller(string billercode)
    {
        var db = new db();
        var items = db.product.Where(i => i.billercode == billercode).Count();
        db.Dispose();
        return items;
    }


    [HttpGet("countAllNotificationsForBiller")]
    public int countAllNotificationsForBiller(string billercode)
    {
        var db = new db();
        var items = db.notifications.Where(i => i.BillerCode == billercode).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllTransactionHistoryForBiller")]
    public int countAllTransactionHistoryForBiller(string billercode)
    {
        var db = new db();
        var items = db.mReceipts.Where(i => i.BillerCode == billercode).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllPaymentHistoryForBiller")]
    public int countAllPaymentHistoryForBiller(string billercode)
    {
        var db = new db();
        var items = db.payments.Where(i => i.BillerCode == billercode).Count();
        db.Dispose();
        return items;
    }


    [HttpGet("countAllReadFeedBackForBiller")]
    public int countAllReadFeedBackForBiller(string billercode)
    {
        var db = new db();
        var items = db.feedback.Where(i => i.billercode == billercode && i.noted).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllUnReadFeedBackForBiller")]
    public int countAllUnReadFeedBackForBiller(string billercode)
    {
        var db = new db();
        var items = db.feedback.Where(i => i.billercode == billercode && !i.noted).Count();
        db.Dispose();
        return items;
    }


    [HttpGet("countAllReadEnquiriesForBiller")]
    public int countAllReadEnquiriesForBiller(string billercode)
    {
        var db = new db();
        var items = db.mServiceEnquiries.Where(i => i.billercode == billercode && i.isRead).Count();
        db.Dispose();
        return items;
    }

    [HttpGet("countAllUnReadEnquiriesForBiller")]
    public int countAllUnReadEnquiriesForBiller(string billercode)
    {
        var db = new db();
        var items = db.mServiceEnquiries.Where(i => i.billercode == billercode && !i.isRead).Count();
        db.Dispose();
        return items;
    }

    /// <summary>
    /// return the service enquiry questions and answers
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("ViewEnquiryDetailsById")]
    public JsonResult ViewEnquiryDetailsById(int id)
    {
        var db = new db();
        var questions_and_answers = db.EnquiryAnswers.Where(i => i.service_enquiry_id == id).ToList();
        db.Dispose();
        return Json(new
        {
            res = "ok",
            msg = questions_and_answers
        });
    }




    /// <summary>
    /// client may update the product via a post
    /// </summary>
    /// <param name="product"></param>
    /// <returns></returns>
    [HttpPost("UpdateBillerProductById")]
    public JsonResult UpdateBillerProductById([FromBody]mProductOffline product)
    {
        var db = new db();
        db.Update(product);
        db.Dispose();
        return Json(true);
    }


    /// <summary>
    /// allows the client to delete his/her product
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("DeleteBillerProductByID")]
    public JsonResult DeleteBillerProductByID(int id)
    {
        var db = new db();
        var productid = db.product.Delete(i => i.id == id);
        db.Dispose();
        return Json(productid);
    }





}
}*/

