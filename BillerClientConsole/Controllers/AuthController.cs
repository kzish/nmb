﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace BillerClientConsole.Controllers
{
    [Authorize]
    [Route("Auth")]
    public class AuthController : Controller
    {
        private UserManager<IdentityUser> userManager;
        private SignInManager<IdentityUser> signInManager;

        dbContext db = new dbContext();
        private readonly IHttpContextAccessor _http;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }


        public AuthController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IHttpContextAccessor http)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            _http = http;
        }

        private Task<IdentityUser> GetCurrentUserAsync() => userManager.GetUserAsync(HttpContext.User);

        private void seed()
        {
            try
            {
                var user = db.AspNetUsers.ToList();
                if (user.Count == 0)
                {
                    var nuser = new IdentityUser { Email = "admin@rubiem.com", UserName = "admin@rubiem.com", PasswordHash = "Rubiem#99" };
                    userManager.CreateAsync(nuser, nuser.PasswordHash).Wait();
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = "Error connecting to database";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
        }


        [AllowAnonymous]
        [HttpGet("Login")]
        public IActionResult Login()
        {
            //seed the temp account

            ViewBag.title = "Login";
            seed();
            return View();
        }


        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login(string email, string password, string returnUrl)
        {
            ViewBag.title = "Login";
            //sign in
            var result = await signInManager.PasswordSignInAsync(email, password, false, false);
            if (result.Succeeded)
            {
                //also ensure that the user is in the billers role
                var user = db.AspNetUsers
                    .Where(i => i.Email == email && i.AspNetUserRoles.Any(r => r.Role.Name == "biller"))
                    .FirstOrDefault();

                if (user == null)
                {
                    await signInManager.SignOutAsync();
                    ViewBag.email = email;
                    TempData["msg"] = "User is not a biller";
                    TempData["type"] = "error";
                    return View();
                }

                else
                {
                    //go to the return url
                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return RedirectPermanent(returnUrl);
                    }
                    else
                    {
                        //go to admin dashboard
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
            }
            ViewBag.email = email;
            TempData["msg"] = "Invalid Login Credentials";
            TempData["type"] = "error";
            return View();
        }


        [HttpPost("Logout")]
        public IActionResult Logout()
        {
            signInManager.SignOutAsync();
            return RedirectToAction("Login", "Auth");
        }





    }
}
