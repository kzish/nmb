﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Newtonsoft.Json;
using X.PagedList;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BillerClientConsole.Controllers.Partials
{

    /// <summary>
    /// will return partial enquiries views called by ajax
    /// </summary>
    [Route("ajax/enquiries")]
    [Authorize(Roles = "biller")]
    public class EnquiriesPartialController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


        //show questions and answers from the client/app
        [Route("ListAllEnquiries")]
        public IActionResult ListAllEnquiries(bool open, bool closed, string date_from, string date_to)
        {
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            //
            var service_enquiries = db.MServiceEnquiry.Where(i => i.Billercode == user.MBillerInfo.BillerCode);
            if (!string.IsNullOrEmpty(date_from))
            {
                service_enquiries = service_enquiries.Where(i => i.Date >= DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                service_enquiries = service_enquiries.Where(i => i.Date <= DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            }
            var _service_enquiries = service_enquiries.ToList();
            if (open)
            {
                _service_enquiries = _service_enquiries.Where(i => i.IsRead == false).ToList();
            }
            if (closed)
            {
                _service_enquiries = _service_enquiries.Where(i => i.IsRead == false).ToList();
            }
            ViewBag.enqs = _service_enquiries;
            return PartialView("_ListAllEnquiries");
        }


        /// <summary>
        /// show only the questions to the biller console
        /// </summary>
        /// <param name="open"></param>
        /// <param name="closed"></param>
        /// <returns></returns>
        [Route("ListAllEnquiryQuestions")]
        public IActionResult ListAllEnquiryQuestions(bool open, bool closed)
        {
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var questions = db.MEnquiryQuestions.Where(i => i.Billercode == user.MBillerInfo.BillerCode).ToList();
            ViewBag.enqs = questions;
            return PartialView("_ListBillerQuestions");
        }


    }
}
