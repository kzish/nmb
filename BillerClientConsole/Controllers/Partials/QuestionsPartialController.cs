﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Newtonsoft.Json;
using X.PagedList;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BillerClientConsole.Controllers.Partials
{

    /// <summary>
    /// will return partial report views called by ajax
    /// </summary>
    [Route("ajax/questions")]
    [Authorize(Roles = "biller")]
    public class QuestionsPartialController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        
        [Route("BillerQuestions")]
        public IActionResult BillerQuestions()
        {
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var questions = db.MEnquiryQuestions.Where(i => i.Billercode == user.MBillerInfo.BillerCode).ToList();
            ViewBag.questions = questions;
            return PartialView("_ListBillerQuestions",questions??new List<MEnquiryQuestions>());
        }


    }
}
