﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Newtonsoft.Json;
using X.PagedList;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BillerClientConsole.Controllers.Partials
{

    /// <summary>
    /// will return partial report views called by ajax
    /// </summary>
    [Route("ajax/reports")]
    [Authorize(Roles = "biller")]
    public class ReportsPartialController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// invoked from ajax
        /// fetches the feedback async
        /// and returns a partial view with the feedback rendered
        /// </summary>
        /// <param name="billercode"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("Feedback")]
        public IActionResult Feedback(int pageNumber, string billercode,string date_from,string date_to)
        {
            //now using datatables , paging and pagenumber now obsolete
            if (pageNumber < 1) pageNumber = 1;//ensure page never goes below one
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var feedback= db.MFeedBack.Where(i => i.Billercode == user.MBillerInfo.BillerCode).ToList();
            ViewBag.feedback = feedback;
            ViewBag.pageNumber = pageNumber;
            if(!string.IsNullOrEmpty(date_from))
            {
                ViewBag.date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                ViewBag.date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            return PartialView("_ListFeedback",feedback);
        }



        [Route("PaymentHistory")]
        public IActionResult PaymentHistory(int pageNumber, string billercode, string date_from, string date_to)
        {
            //now using datatables , paging and pagenumber now obsolete
            if (pageNumber < 1) pageNumber = 1;//ensure page never goes below one
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var payments = db.MPayments.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).ToList();
            ViewBag.payments = payments;//.AsQueryable().ToPagedList(pageNumber, 5);
            ViewBag.pageNumber = pageNumber;
            if (!string.IsNullOrEmpty(date_from))
            {
                ViewBag.date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                ViewBag.date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            return PartialView("_ListPaymentHistory", payments);
        }


        [Route("TransactionHistory")]
        public IActionResult TransactionHistory(int transaction_id, string date_from, string date_to)
        {
            //now using datatables , paging and pagenumber now obsolete
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var transactions = db.MTransAction.AsQueryable();
            if (transaction_id != 0)
            {
                transactions = transactions.Where(i => i.Id == transaction_id && i.BillerCode==user.MBillerInfo.BillerCode);
            }
            if (!string.IsNullOrEmpty(date_from))
            {
                var datefrom = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_from = datefrom.ToString("yyyy-MM-dd");
                transactions = transactions.Where(i => i.Date >= datefrom);
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var dateto = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_to = dateto.ToString("yyyy-MM-dd");
                transactions = transactions.Where(i => i.Date <= dateto);
            }
            transactions= transactions
                .Where(i => i.BillerCode == user.MBillerInfo.BillerCode)
                .Include(i=>i.MBillerOrders)
                .OrderByDescending(i => i.Date); 
            ViewBag.transactions = transactions.ToList();
            return PartialView("_ListTransactionHistory", transactions);
        }

        [Route("Orders")]
        public IActionResult Orders(int transaction_id,  string date_from, string date_to)
        {
            //now using datatables , paging and pagenumber now obsolete
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i => i.MBillerInfo).FirstOrDefault();
            var orders = db.MBillerOrders.AsQueryable();

            if(transaction_id!=0)
            {
                orders = orders.Where(i => i.TransactionId == transaction_id && i.BillerCode == user.MBillerInfo.BillerCode);
            }
            
            if (!string.IsNullOrEmpty(date_from))
            {
                var datefrom = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_from = datefrom.ToString("yyyy-MM-dd");
                orders = orders.Where(i => i.Date >= datefrom);
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var dateto = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.date_to = dateto.ToString("yyyy-MM-dd");
                orders = orders.Where(i => i.Date <= dateto);
            }

            orders = orders.Where(i => i.BillerCode == user.MBillerInfo.BillerCode)
                .OrderByDescending(i=>i.Date);
            ViewBag.orders = orders.ToList();
            return PartialView("_ListOrders", orders);
        }


    }
}
