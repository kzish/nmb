﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Newtonsoft.Json;
using X.PagedList;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BillerClientConsole.Controllers.Partials
{

    /// <summary>
    /// will return partial report views called by ajax
    /// </summary>
    [Route("ajax/products")]
    public class ProductsPartialController : Controller
    {

        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// invoked from ajax
        /// fetches the biller products async
        /// and returns a partial view with the products are rendered
        /// </summary>
        /// <param name="billercode"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("BillerProducts")]
        [Authorize(Roles = "biller")]
        public IActionResult BillerProducts(int pageNumber, string billercode,string date_from,string date_to)
        {
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).FirstOrDefault();
            var products = db.MBillerProduct.Where(i => i.BillerCode == user.MBillerInfo.BillerCode).ToList();//fetch by billercode
            ViewBag.products = products;
            ViewBag.pageNumber = pageNumber;
            if(!string.IsNullOrEmpty(date_from))
            {
                ViewBag.date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                ViewBag.date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            return PartialView("_ListBillerProducts");
        }



     


    }
}
