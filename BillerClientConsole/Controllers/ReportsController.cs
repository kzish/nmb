﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BillerClientConsole.Models;
using Microsoft.EntityFrameworkCore;

namespace BillerClientConsole.Controllers
{
    [Route("Reports")]
    [Authorize(Roles = "biller")]
    public class ReportsController : Controller
    {
        dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        /***** reports ******/
        [HttpGet("ListEnquiries")]
        public IActionResult ListEnquiries(int page=0)
        {
            ViewBag.title = "Reports / Enquiries";
            ViewBag.page = page;
            return View();
        }

        
        [HttpGet("ListTransactionHistory")]
        [HttpGet("")]
        public IActionResult ListTransactionHistory(int transaction_id, string date_from, string date_to)
        {
            ViewBag.title = "Reports / Transaction History";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            //ajax reports is in partial controllers
            TempData["loadUrl"] = $"~/ajax/reports/TransactionHistory";
            TempData["transaction_id"] = transaction_id;
            TempData["billercode"] = user.MBillerInfo.BillerCode;
            TempData["date_to"] = date_to;
            TempData["date_from"] = date_from;
            return View();
        }


        [HttpGet("ListPaymentHistory")]
        public IActionResult ListPaymentHistory(int pageNumber, string date_from, string date_to)
        {
            ViewBag.title = "Reports / Payment History";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i=>i.MBillerInfo).First();
            //ajax reports is in partial controllers
            TempData["loadUrl"] = $"~/ajax/reports/PaymentHistory";
            TempData["pageNumber"] = pageNumber;
            TempData["billercode"] = user.MBillerInfo.BillerCode;
            TempData["date_to"] = date_to;
            TempData["date_from"] = date_from;
            return View();
        }


        [HttpGet("ListFeedback")]
        public IActionResult ListFeedback(int pageNumber,string date_from,string date_to)
        {
            ViewBag.title = "Reports / Feedback";
            var user = db.AspNetUsers.Where(i=>i.Email==User.Identity.Name).Include(i=>i.MBillerInfo).First();

            TempData["loadUrl"] = $"~/ajax/reports/Feedback";
            TempData["pageNumber"] = pageNumber;
            TempData["billercode"] = user.MBillerInfo.BillerCode;
            TempData["date_to"] = date_to;
            TempData["date_from"] =date_from;
            
            return View();
        }

        [HttpGet("ListOrders")]
        public IActionResult ListOrders(string date_from, string date_to,int transaction_id)
        {
            ViewBag.title = "Reports / Orders";
            var user = db.AspNetUsers.Where(i => i.Email == User.Identity.Name).Include(i => i.MBillerInfo).First();
            //ajax reports is in partial controllers
            TempData["loadUrl"] = $"~/ajax/reports/Orders";
            TempData["billercode"] = user.MBillerInfo.BillerCode;
            TempData["date_to"] = date_to;
            TempData["date_from"] = date_from;
            TempData["transaction_id"] = transaction_id;
            return View();
        }


    }
}
