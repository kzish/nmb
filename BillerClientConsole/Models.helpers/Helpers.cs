﻿using BillerClientConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Helpers
{
    public static string getBillerCategory(int id)
    {
        var db = new dbContext();
        var res = db.EnumBillerCategory.Find(id);
        db.Dispose();
        return res.Category;
    }
    public static string getBillerType(int id)
    {
        var db = new dbContext();
        var res = db.EnumBillerType.Find(id);
        db.Dispose();
        return res.Type;
    }
    public static string getBank(int id)
    {
        var db = new dbContext();
        var res = db.EnumBanks.Find(id);
        db.Dispose();
        return res.Name;
    }

    public static string getCurrecy(int id)
    {
        var db = new dbContext();
        var res = db.EnumCurrencyCode.Find(id);
        db.Dispose();
        return res.CurrencyCode;
    }
}
