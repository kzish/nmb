﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WcfService;
using ZipitService;
using ZssZimSwitch.Models;

namespace ZssZimSwitch.Controllers
{
    [Route("Zss")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    public class ZssController : Controller
    {


        private string zss_participant_id = "588832";
        private string zss_username = "zss";
        private string zss_password = "password";
        private string zss_endpoint_url = "https://secure.zss.co.zw/uat/vportal/integration/zipit/soap/ZipitSoapService";
        private string zss_domain = "secure.zss.co.zw";
        private string currency_code = "840";

        //initiate credit/debit
        [HttpPost("processZipitBankRequest")]
        public async Task<JsonResult> processZipitBankRequest(long amount,
            string destination_account,
            string destination_institution,
            string unique_reference_guid,
            string sender_identifier,
            string msisdn,
            string account_reference
            )
        {
            try
            {

                //
                var zss_client = new ZipitSoapServiceClient();
                //
                var zipit_request = new zipitRequest();
                zipit_request.amount = amount;
                zipit_request.channel = "INTERNET";
                zipit_request.currencyCode = currency_code;//get correct currency code
                zipit_request.destinationAccount = destination_account;
                zipit_request.destinationInstitution = destination_institution;
                zipit_request.location = "rubiem_whatsapp_banking";
                zipit_request.participantId = zss_participant_id;
                zipit_request.password = zss_password;
                zipit_request.senderIdentifier = sender_identifier;
                zipit_request.tranDateTime = DateTime.Now;
                zipit_request.uniqueReference = unique_reference_guid;//must be unique per request
                zipit_request.username = zss_username;
                //
                var payer_details = new payerDetails();
                payer_details.payerName = "rubiem_whatsapp_banking";
                payer_details.payerMsisdn = msisdn;//format +263773303669
                payer_details.accountReference = account_reference;//account reference orr card reference
                //
                zipit_request.payerDetails = payer_details;
                //
                var zss_response= await zss_client.processZipitBankRequestAsync(zipit_request);

                return Json(new
                {
                    res = "ok",
                    data = zss_response.ZipitResponse
                }, new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented
                }) ;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                }, new Newtonsoft.Json.JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented
                });
            }
        }


       



    }
}
