﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// allows the client/app to authenticate its self before making any other requests
/// </summary>
namespace Biller.Controllers
{
    [Route("Auth")]
    public class AuthController : Controller
    {
        /// <summary>
        /// client using client id and client secret to authenticate and recieve access_token
        /// Add { Bearer <access_token> } in header request of each subsequent request
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="clientSecret"></param>
        /// <returns></returns>
        [HttpGet("RequestToken")]
        public async Task<IActionResult> RequestToken(string clientID, string clientSecret)
        {
            try
            {
                string identity_server_end_point = Globals.oauth_server;
                var disco = await DiscoveryClient.GetAsync(identity_server_end_point);
                var tokenClient = new TokenClient(
                    address: disco.TokenEndpoint,
                    clientId: clientID,
                    clientSecret: clientSecret);
                var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api");
                if (tokenResponse.IsError)
                {
                    return Json(tokenResponse.Exception);
                }
                else
                {
                    return Json(tokenResponse.Json);
                }
            }catch(Exception ex)
            {
               return Ok(ex);
            }
        }

      
    }
}
