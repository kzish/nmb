﻿using System;
using System.Collections.Generic;

namespace Auth.Models
{
    public partial class MProductOffline
    {
        public int Id { get; set; }
        public string Productcode { get; set; }
        public decimal? Price { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool? Available { get; set; }
        public string Name { get; set; }
        public bool? Disabled { get; set; }
        public string Billercode { get; set; }
    }
}
