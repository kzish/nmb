﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using Auth.Models;
using Microsoft.EntityFrameworkCore;

namespace Auth.Models
{
    public class Config
    {

        /// <summary>
        /// fetch all the reources from the db
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ApiResource> GetApiResources()
        {
            var db = new dbContext();
            var apiResources = new List<ApiResource>();
            foreach(var r in db.MApiResources.ToList())
            {
                apiResources.Add(new ApiResource(r.ResourceName));
            }
            db.Dispose();
            return apiResources;
        }

        /// <summary>
        /// fetch all the clients from the db
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Client> GetApiClients()
        {
            var db = new dbContext();
            var apiClients = new List<Client>();
            var api_users = db.AspNetUsers.Where(i => i.AspNetUserRoles.Any(r => r.Role.Name == "api_client")).Include(i=>i.MApiClients).ToList();
            foreach (var c in api_users)
            {
                var nc = new Client
                {
                    ClientId = c.MApiClients?.ClientId,
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets ={
                                new Secret(c.MApiClients?.Secret.Sha256())
                        },
                    AllowedScopes = { "api" },
                };

                apiClients.Add(nc);
            }
            db.Dispose();
            return apiClients;
        }



    }
}
