﻿using System;
using System.Collections.Generic;

namespace Auth.Models
{
    public partial class MServiceEnquiry
    {
        public int Id { get; set; }
        public string PayerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Billercode { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsRead { get; set; }
    }
}
