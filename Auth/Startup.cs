﻿using Auth.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Auth
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IHostingEnvironment host)
        {
            Configuration = configuration;
            this.host = host;
        }

        public IConfiguration Configuration { get; }
        private IHostingEnvironment host { get; set;}
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentityServer()
               .AddDeveloperSigningCredential()
           .AddInMemoryApiResources(Config.GetApiResources())
           .AddInMemoryClients(Config.GetApiClients());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseIdentityServer();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            //app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            
            app.UseMvc();
            //self register in simba_config
            var app_name = env.ApplicationName;
            app.Run(async (context) =>
            {
                var path = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedUrl(context.Request);
                path = path.Replace("/favicon.ico", "");
                Globals.write_config(app_name, $"{path}");
                await context.Response.WriteAsync(app_name);
            });
        }
    }
}
