﻿using FreshInABox.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace FreshInABox.Controllers
{
    [Route("FreshInABox")]
    public class CartController : Controller
    {

        [HttpPost("CreateCart")]
        public bool CreateCart(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return false;
            }

            try
            {
                //create web client add token and post data
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token", token);
                var response = client.PostAsync("https://api.freshinabox.co.zw/cart/", null)
                    .Result.Content.ReadAsStringAsync().Result;
                dynamic json=JObject.Parse(response);
                if (json.success == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.CartController.CreateCart", ex);
                return false;
            }
        }


        [HttpPost("ClearCart")]
        private JsonResult ClearCart(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return Json(new
                {
                    res = "err",
                    msg = "token is missing"
                });
            }

            try
            {
                //create Http client add token and delete cart
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("token", token);
                var response = client.DeleteAsync("https://api.freshinabox.co.zw/cart/").Result;
                var response_string = response.Content.ReadAsStringAsync().Result;
                return Json(new
                {
                    res = "ok",
                    msg = JObject.Parse(response_string)
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.CartController.ClearCart", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        [HttpPost("AddToCart")]
        public bool AddToCart(string token,[FromBody]mBillerCartRequest cart)
        {

            if (cart.items == null || cart.items.Count == 0)
            {
                return false;
            }

            try
            {
                //map the cart items to FreshInABox product
                var json_array = new JArray();
                foreach (var item in cart.items)
                {
                    var json_obj = new JObject();
                    json_obj.Add("id", item.product_code);
                    json_obj.Add("quantity", item.quantity);
                    json_array.Add(json_obj);
                }

                //create json object to post
                var json_post_data = "{ \"items\" : " + json_array.ToString() + " }";

                //create web client add token and post data
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("token", token);
                var response = client.PutAsync("https://api.freshinabox.co.zw/cart/", new StringContent(json_post_data, Encoding.UTF8, "application/json"))
                    .Result.Content.ReadAsStringAsync().Result;

                dynamic json = JObject.Parse(response);
                if (json.success == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.CartController.AddToCart", ex);
                return false;
            }
        }


       /* [HttpGet("RetrieveCart")]
        private JsonResult RetrieveCart(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return Json(new
                {
                    res = "err",
                    msg = "token is missing"
                });
            }

            try
            {

                //create web client add token and get cart data
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token", token);
                var response = client.GetAsync("https://api.freshinabox.co.zw/cart/")
                    .Result.Content.ReadAsStringAsync().Result;
                dynamic json_response = JObject.Parse(response);
                //map into mBillerCart
                var biller_cart = new mBillerCart();
                biller_cart.id = json_response.data.id;
                biller_cart.mCartProducts = new List<mCartProduct>();
                biller_cart.Total = json_response.data.total;
                //add items to cart
                foreach (dynamic item in json_response.data.products)
                {
                    var cart_product = new mCartProduct();
                    //cart_product. = new MBillerProduct
                    //{
                    //    Id = item.id,
                    //    Name = item.name,
                    //    Category = item.category_id,
                    //    Description = item.description,
                    //    Price = item.price,
                    //    ImageUrl = item.image_url,
                    //    Available = bool.Parse(((string)(item.available)).ToLower())
                    //};
                    cart_product.quantity = item.quantity;
                    biller_cart.mCartProducts.Add(cart_product);
                }

                return Json(new
                {
                    res = "ok",
                    msg = biller_cart
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.CartController.RetrieveCart", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

    */

        [HttpPost("CheckOut")]
        public JsonResult CheckOut(string token, string delivery_address, string delivery_name, string delivery_city)
        {
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                if (string.IsNullOrEmpty(delivery_address))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "delivery_address is missing"
                    });
                }

                if (string.IsNullOrEmpty(delivery_name))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "delivery_name is missing"
                    });
                }


                if (string.IsNullOrEmpty(delivery_city))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "delivery_city is missing"
                    });
                }


                //create json object 
                var json = new JObject();
                json.Add("delivery_address", delivery_address);
                json.Add("delivery_name", delivery_name);
                json.Add("delivery_city", delivery_city);

                var json_post_data = json.ToString();



                //create web client and post data
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token", token);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var response = client.PostAsJsonAsync("https://api.freshinabox.co.zw/cart/checkout", json)
                    .Result.Content.ReadAsStringAsync().Result;

                return Json(new
                {
                    res = "ok",
                    msg = JObject.Parse(response)
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.CartController.CheckOut", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }



    }
}
