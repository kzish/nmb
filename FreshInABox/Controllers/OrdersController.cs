﻿using FreshInABox.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace FreshInABox.Controllers
{
    [Route("FreshInABox")]
    public class OrdersController : Controller
    {
        [HttpPost("PayforOrder")]
        public JsonResult PayforOrder(string token, string order_id, string paymentMethod = "PaynowMobile", string currency = "ZWL", string mobileNumber = "")
        {
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                if (string.IsNullOrEmpty(order_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "order_id is missing"
                    });
                }

                if (string.IsNullOrEmpty(paymentMethod))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "paymentMethod is missing"
                    });
                }

                if (string.IsNullOrEmpty(currency))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "currency is missing"
                    });
                }


                if (string.IsNullOrEmpty(mobileNumber))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "mobileNumber is missing"
                    });
                }

                //create json post data
                var json = new JObject();
                json.Add("paymentMethod", paymentMethod);
                json.Add("currency", currency);
                json.Add("mobileNumber", mobileNumber);

                var json_post_data = json.ToString(Newtonsoft.Json.Formatting.Indented);
                var post_string = "https://api.freshinabox.co.zw/orders/u/" + order_id + "/pay";
                //create client,add token and headers, post payment
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token", token);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var response = client.PostAsJsonAsync(post_string, json).Result;
                var response_string = response.Content.ReadAsStringAsync().Result;
                var obj = new
                {
                    res = "ok",
                    msg = JObject.Parse(response_string)
                };
                return Json(JsonConvert.SerializeObject(obj));

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.OrdersController.PayforOrder", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        [HttpGet("RetrieveOrder")]
        public IActionResult RetrieveOrder(string token, string order_id)
        {
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "token is missing"
                    });
                }

                if (string.IsNullOrEmpty(order_id))
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "order_id is missing"
                    });
                }


                //create httpclient
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("token", token);
                var response = client.GetAsync("https://api.freshinabox.co.zw/orders/u/" + order_id).Result;
                var response_string = response.Content.ReadAsStringAsync().Result;
                return Json(new
                {
                    res = "ok",
                    msg = JObject.Parse(response_string)
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("FreshInABox.Controllers.OrdersController.RetrieveOrder", ex);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// handles the payments, and the cart
        /// </summary>
        [HttpPost("PostBillerCart")]
        public JsonResult PostBillerCart([FromBody] mBillerCartRequest billerCartRequest)
        {
            try
            {
                //first get the auth token
                var token = new AuthController().AnonyAuth();
                if (token == "err")
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "failed to get token"
                    });
                }


                //then create a cart with the supplied token
                var cart = new CartController().CreateCart(token);
                if (!cart)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "failed to create cart"
                    });
                }

                //then add items to cart
                var add_to_cart = new CartController().AddToCart(token, billerCartRequest);
                if (!add_to_cart)
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "failed to add to cart"
                    });
                }

                //then do a check out
                var check_out = new CartController().CheckOut(token,
                    delivery_address: billerCartRequest.payer_details1,
                    delivery_name: billerCartRequest.payer_details2,
                    delivery_city: billerCartRequest.payer_details3).Value.ToString();

                check_out = check_out.Replace("msg = ", "\"msg\" =")
                    .Replace("res = ok", "\"res\" = \"ok\"")
                    .Replace("res = err", "\"res\" = \"err\"")
                    .Replace("=", ":");

                dynamic json = JObject.Parse(check_out);
                string message = json.msg.message;
                var order_id = string.Empty;
                if (message == "ORDER CREATED.")
                {
                    //complete payment and return response to the client app
                    order_id = json.msg.data.id;
                    var pay_for_order = PayforOrder(token, order_id, "PaynowMobile", "ZWL", "0773303669").Value.ToString();
                    return Json(new
                    {
                        res = "ok",
                        msg = JObject.Parse(pay_for_order),
                        tracking_id = order_id,
                        payment_ref = billerCartRequest.payment_ref
                    });
                }
                else
                {

                    return Json(new
                    {
                        res = "err",
                        msg = "failed to check out"
                    });
                }


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex,
                });
            }
        }






    }
}
