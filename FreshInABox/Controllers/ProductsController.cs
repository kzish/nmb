﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FreshInABox.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FreshInABox.Controllers
{
    [Route("FreshInABox")]
    public class ProductsController : Controller
    {

        [HttpGet("GetBillerProducts")]
        public JsonResult RetrieveAllProducts()
        {
            try
            {
                //create webclient and fetch all products
                var client = new HttpClient();
                var response  = client.GetAsync("https://api.freshinabox.co.zw/products/?cached=false")
                    .Result.Content.ReadAsStringAsync().Result;
                dynamic json_response = JObject.Parse(response);
                //map the response onto the standard mBillerProduct
                var biller_products = new List<MBillerProduct>();
                foreach (var obj in JArray.Parse(json_response.data.ToString()))
                {
                    var product = new MBillerProduct();
                    //
                    product.ProductCode = obj.id;
                    product.Name = obj.name;
                    product.Category = obj.type;
                    product.Description = obj.description;
                    product.Price = obj.price;
                    product.ImageUrl = obj.image_url;
                    string available = obj.available;
                    available = available.ToLower();
                    product.Available = bool.Parse(available);
                    //
                    biller_products.Add(product);
                }



                return Json(new {
                    res="ok",
                    msg = biller_products

                });
            }catch(Exception ex)
            {


                Globals.log_data_to_file("FreshInABox.Controllers.ProductsController.RetrieveAllProducts", ex);
                return
                    Json(new
                    {
                        res = "err",
                        msg=ex.Message
                    });
            }
            
        }


        [HttpGet("GetBillerProductsByType")]
        public JsonResult RetrieveProductsByType(string type)
        {

            if(string.IsNullOrEmpty(type))
            {
                return Json(new {
                    res="err",
                    msg="product type param is missing."
                });
            }

            try
            {
                //create webclient and fetch all products by category
                var client = new HttpClient();
                var response = client.GetAsync("https://api.freshinabox.co.zw/products/type/" + type + "?cached=false")
                    .Result.Content.ReadAsStringAsync().Result;


                dynamic json_response = JObject.Parse(response);
                //map the response onto the standard mBillerProduct
                var biller_products = new List<MBillerProduct>();
                foreach (var obj in json_response.data.products)
                {
                    var product = new MBillerProduct();
                    //
                    product.Id = obj.id;
                    product.Name = obj.name;
                    product.Category = obj.type;
                    product.Description = obj.description;
                    product.Price = obj.price;
                    product.ImageUrl = obj.image_url;
                    string available = obj.available;
                    available = available.ToLower();
                    product.Available = bool.Parse(available);
                    //
                    biller_products.Add(product);
                }



                return Json(new
                {
                    res = "ok",
                    msg = biller_products

                });
            }
            catch (Exception ex)
            {


                Globals.log_data_to_file("FreshInABox.Controllers.ProductsController.RetrieveProductsByType", ex);
                return
                    Json(new
                    {
                        res = "err",
                        msg = ex.Message
                    });
            }
        }

    }
}
