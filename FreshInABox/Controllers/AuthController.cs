﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FreshInABox.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FreshInABox.Controllers
{
    /// <summary>
    /// handles fresh in a box authentication
    /// </summary>
    [Route("FreshInABox")]
    public class AuthController : Controller
    {

        /// <summary>
        /// authenticate the caller and return a token
        /// </summary>
        /// <returns></returns>
        [HttpPost("AnonyAuth")]
        public string AnonyAuth()
        {

            try
            {
                //get guid
                var guid = Guid.NewGuid().ToString();
                //create json object with hash
                var json = new JObject();
                json.Add("hash",guid);

                //create web client and post to fresh in a box auth api
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept","application/json");
                var response = client.PostAsJsonAsync("https://api.freshinabox.co.zw/auth/user/anonymous", json).Result.Content.ReadAsStringAsync().Result;
                dynamic json_data = JObject.Parse(response);


                //return Json(new
                //{
                //    res = "ok",
                //    msg = json_data.data.token
                //});
               return  json_data.data.token;
            }
            catch(Exception ex)
            {

                Globals.log_data_to_file("FreshInABox.Controllers.AuthController.AnonyAuth", ex);

                //return Json(new
                //{
                //    res = "err",
                //    msg = ex.Message
                //});
                return "err";
            }
        }
    }
}
