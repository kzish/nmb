﻿using System;
using System.Collections.Generic;

namespace FreshInABox.Models
{
    public partial class Providers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ZssproviderName { get; set; }
        public string ProviderDescription { get; set; }
        public int ProductId { get; set; }
    }
}
