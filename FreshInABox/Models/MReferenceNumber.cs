﻿using System;
using System.Collections.Generic;

namespace FreshInABox.Models
{
    public partial class MReferenceNumber
    {
        public int Reference { get; set; }
    }
}
